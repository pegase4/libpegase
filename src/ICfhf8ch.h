#ifndef ICFHF8CH_H
#define ICFHF8CH_H

#include <QObject>


#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <qtconcurrentrun.h>
#include <IEvent.h>
#include <sys/time.h>
#include <iostream>
#include <QtMath>
#include <QMutex>
#include <QQueue>
#include <ifsttar_hf8ch.h>
#include "IReadhfcan.h"
#include "IReadhfpps.h"
#include <QDir>
#include <QProcess>

#define NB_SECONDE_PPS_INFO 120
#define FREQ_RX_MAX 2000000
#define MAX_DATA_READ_KERNEL 250000

typedef struct _ConfTx ConfTx_t;
struct _ConfTx {
    int digitDelay = 0;
    int prf = 1000000;
    int prfFreq = 10;
    int prf_reel = 1000000;
    int nbAv = 100;
    int channel = 1;
    int acqDelay = 0;
    int syncPps = 1;
};

typedef struct _ConfRx ConfRx_t;
struct _ConfRx {
    int nbSample = 0;
    quint8 channel = 0b11111111;
    quint8 gain[8] =  { 0 } ;
    int freq = 0;
};

typedef struct _PpsInfoAll PpsInfoAll_t;
struct _PpsInfoAll {
    quint64 time = 0;
    quint32 period = 0;
    quint32 qerrBeginning = 0;
    quint32 qerrEnd = 0;

};

typedef struct _DataRx DataRx_t;
struct _DataRx {
   QVector<quint16> data;
};
typedef struct _SignalRx Signal_t;
struct _SignalRx {
   quint32 typeAc;
   quint32 activateChannelRx;
   quint32 activateChannelTx;
   quint32 freqencyRx;
   quint64 secondFirst;
   quint32 cptFirst;
   int nbChanneRx;
   int nbDataRx;
   int partOfTransfer;
   bool endRead;
   QVector<DataRx_t> dataRx;
   uint16_t *dataListbrut;
};

class ICfhf8ch : public QObject
{
    Q_OBJECT
public:
    enum state{NOT_OPENED,OPENED,STARTED,STOPED};

    explicit ICfhf8ch(QObject *parent = 0);
    ~ICfhf8ch();
    int init(bool databrut = false);
    int start();
    int stop();
signals:
     void dataAvaible(Signal_t data, IEvent *event);
public slots:

    int startTrigSoft();
    int startTrigExterne(bool rising, int channel);
    int startTrigDate(quint64 date);
    int setDigitDelay_dns(int delay);//temps d'attente entre le tir et le debut d'acquisition (pas = 10ns), exemple : 01=>10ns 02=>20ns …
    int setPrf_hz(int prf);//frequence de tir
    int setPrf_dns(int prf);

    int setNbAvg(int nbAv);//nombre de tirs pour une moyenne

    int setChannelTx(int channelTx);//voie active à l'emission


    int setNbRxSample(int nbSampleRx);//nombre d'echantillons par voie
    int getNbRxSampleBytes();//nombre d'octet à remonter par acquistion: nombre de voies(8)*nombre d'echantillons(nbr_rx_sample)*nombred'octet(2 par echantillons)
    int setChannelRx(quint8 channel);//channel d'acquisition activees
    //int setGainRx(int channel,Gain1_ad8253 gain1,Gain2_ltc6912 gain2 );//gain pour un channel acquisition
   // int setGainRx(Gain1_ad8253 gain1,Gain2_ltc6912 gain2 );//gain pour tous les channels d'acquisition
    int setGainRx(int channel,quint8 gain );//gain pour un channel acquisition
    int setGainRx(quint8 gain);//gain pour tous les channels d'acquisition

    int setfreqRx(int freqRx);//frequence d'échantillonnage exemple: 0=>2,5MHz, 1=> 0,625MHz, 2=> 0,3125MHz

    quint64 getDateFPGA();
    int saveConfig();
    void ReadPps(ppsInfos_t newPpsInfo, IEvent *event);
    int resetHardFpga();
    int resetSoftFpga();
    int setBT(bool on);
    int setHT(bool on);
    int setSignalTx(QList<quint16> signalTx, int channel);
    int setLedBlink(int num);//nombre de tirs pour une moyenne
    int setAcqDelay_dns(int delay);
    int setSyncPps (int nbTire);
    bool getVersionPps();
    int startTrigSeuilDate(quint8 channel,quint64 date);
    int startTrigSeuilSoft(quint8 channel);
    int setSeuil(int seuilPos,int seuilNeg);
    int setpreTrig(int preTrig);
private:
    int saveDelayPrfPps();
    int sendAndVerifyConf(int setIoctl, int getIoctl, QString name, int confToSend);
    int OpenPps();
    int StartPps();
    int modprobeDriver();
    int m_fdCf8ch;
    bool m_databrut;
    state m_state = NOT_OPENED;
    ConfTx_t m_confTx_t;
    ConfRx_t m_confRx_t;
    IReadhfcan *m_readCan;
/////////////PPS//////////
    int m_fdHfpps;
    IReadhfpps *m_readHfpps;
    QQueue<struct _PpsInfoAll> m_ppsInfosQueue;
    QMutex m_ppsInfosMutex;
private slots:
    void analyseRx(uint16_t *dataList, int count,int partOfTransfer,bool endRead, ConfhfCan_t confData, IEvent *event);
};


#endif // ICFHF8CH_H
