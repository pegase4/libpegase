#ifndef IPPS_H
#define IPPS_H

#include <QObject>
#include "IEvent.h"
#include <fcntl.h>
#include <QDebug>
#include <unistd.h>

class IPps : public QThread
{
    Q_OBJECT
public:
    IPps();
    int init();
    ~IPps();
signals:
    void ppsRising(IEvent *event);
    void ppsFalse(IEvent *event);
private:
    bool m_running;
    int _fd;
    void run ();
};

#endif // IPPS_H
