#include "IReadhfpps.h"


IReadhfpps::IReadhfpps(int fd)
{
    m_fd = fd;
    m_running = true;
}

IReadhfpps::~IReadhfpps()
{
    m_running = false;
}

void IReadhfpps::run()
{
    int ret;
    IEvent *newEvent;
    struct pollfd PollFd;
    ppsInfos_t newPpsInfo;
    PollFd.fd = m_fd;
    PollFd.events = POLLIN | POLLPRI;
    do {
        ret = poll (&PollFd, 1, 500);
        if (ret > 0) {
            ret = read (m_fd, &newPpsInfo, sizeof(newPpsInfo));
            newEvent = new IEvent();
            emit dataAvaible(newPpsInfo,newEvent);
            delete(newEvent);
        }
    }while(m_running);

}
