#ifndef ICFG8CHREADCAN_H
#define ICFG8CHREADCAN_H
#include "IEvent.h"
#include <ifsttar_g8ch.h>
#include <unistd.h>

class ICfg8chReadCan:  public QThread
{
    Q_OBJECT
public:
    ICfg8chReadCan(int fd,int count,int nbChannel);
    ~ICfg8chReadCan();
private :
    int  m_fd;
    bool m_running;
    int m_count;
    int m_nbChannel;
signals:
    void dataAvaible(uint32_t *data,int count, int nbChannel,quint64 seconde, quint32 compteur,IEvent *event);
    void Error_overwrite_dma(int nbOverwrite,IEvent *event);

private:
    void run ();
};

#endif // ICFG8CHREADCAN_H
