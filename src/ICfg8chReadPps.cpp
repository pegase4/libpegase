#include "ICfg8chReadPps.h"

ICfg8chReadPps::ICfg8chReadPps(int fd)
{
    m_fd = fd;
    m_running = true;
}

ICfg8chReadPps::~ICfg8chReadPps()
{
    m_running = false;
}

void ICfg8chReadPps::run()
{
    int ret;
    IEvent *newEvent;
    struct pollfd PollFd;
    ppsInfos newPpsInfo;
    PollFd.fd = m_fd;
    PollFd.events = POLLIN | POLLPRI;
    do {
        ret = poll (&PollFd, 1, 500);
        if (ret > 0) {
            ret = read (m_fd, &newPpsInfo, sizeof(newPpsInfo));
            newEvent = new IEvent();
            emit dataAvaible(newPpsInfo,newEvent);
            delete(newEvent);
        }
    }while(m_running);

}
