#
#   Qt modules
#
#QT += core
QT += network
QT -= gui
QT += concurrent
QT += core-private
#
#   Project configuration and compiler options
#
CONFIG += c++14
CONFIG += c++11
CONFIG += warn_on
CONFIG += console
#
#   Output config
#
TEMPLATE = lib
CONFIG += dll
VERSION = 1.0.0
TARGET = sdkp3

#
#   Install rules
#
headers.path = $$[QT_INSTALL_PREFIX]/include/sdkp3

headers.files = IEvent.h
headers.files += IReadhfpps.h
headers.files += IAnalogData.h 
headers.files += ICfg8ch.h 
headers.files += IGnumdata.h 
headers.files += IReadhfcan.h 
headers.files += ICfg8chReadCan.h 
headers.files += ICfg8chReadTrig.h 
headers.files += ICfg8chReadPps.h 
headers.files += ICfrail.h 
headers.files += ICfbeton.h 
headers.files += ICfhf8ch.h
headers.files += IPps.h 
headers.files += i_hf8ch_basic_fnc.h
headers.files += tdmswriter/tdmswriter.h
headers.files += IStdout.h
headers.files += IGpio.h


INSTALLS += headers

target.path = $$[QT_INSTALL_LIBS]
INSTALLS += target

#
#   Remove debug in release mode
#
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

CONFIG += create_pc create_prl no_install_prl

QMAKE_PKGCONFIG_NAME = libsdkp3
QMAKE_PKGCONFIG_DESCRIPTION = libsdkp3 for pegase
QMAKE_PKGCONFIG_PREFIX = $$INSTALLBASE
QMAKE_PKGCONFIG_LIBDIR = $$mylib.path
QMAKE_PKGCONFIG_INCDIR = $$headers.path
QMAKE_PKGCONFIG_VERSION = $$VERSION
QMAKE_PKGCONFIG_DESTDIR = pkgconfig

#
#   Sources
#
SOURCES += \
    IEvent.cpp\
    IReadhfpps.cpp \
    IReadhfcan.cpp \
    IAnalogData.cpp \
    ICfg8ch.cpp \
    IGnumdata.cpp \
    ICfg8chReadCan.cpp \
    ICfg8chReadTrig.cpp \
    ICfg8chReadPps.cpp \
    ICfrail.cpp \
    ICfbeton.cpp \
    IStdout.cpp \
    i_hf8ch_basic_fnc.cpp\
    IPps.cpp\
    tdmswriter/tdmswriter.cpp\
    ICfhf8ch.cpp \
    IGpio.cpp

HEADERS += \
    IEvent.h\
    IReadhfpps.h \
    IAnalogData.h \
    ICfg8ch.h \
    IGnumdata.h \
    IReadhfcan.h \
    ICfg8chReadCan.h \
    ICfg8chReadTrig.h \
    ICfg8chReadPps.h \
    ICfrail.h \
    ICfbeton.h \
    IStdout.h \
    IPps.h\
    i_hf8ch_basic_fnc.h\
    tdmswriter/tdmswriter.h\
    ICfhf8ch.h \
    IGpio.h

LIBS  += -lgpiod

DEFINES += SDKP3_LIBRARY
