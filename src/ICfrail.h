#ifndef ICFRAIL_H
#define ICFRAIL_H

#include "ICfhf8ch.h"

enum Gain1_ad8253 {
    G1_0=0b00000000,
    G1_20=0b00000001,
    G1_40=0b00000010,
    G1_60=0b00000011
};
enum Gain2_ltc6912 {
    G2_NotUse = 0b11000000,
    G2_less120 = 0b10000000,
    G2_0 = 0b00010000,
    G2_6 = 0b00100000,
    G2_14 = 0b00110000,
    G2_20 = 0b01000000,
    G2_26 = 0b01010000,
    G2_34 = 0b01100000,
    G2_40 = 0b01110000
};

class ICfRail : public ICfhf8ch
{
public:
    ICfRail();
    int setChannelTx(int channelTx);
    int setGainRx(int channel,Gain1_ad8253 gain1,Gain2_ltc6912 gain2 );//gain pour un channel acquisition
    int setGainRx(Gain1_ad8253 gain1,Gain2_ltc6912 gain2 );//gain pour tous les channels d'acquisition
    int setGainRx(int channel,quint8 gain );//gain pour un channel acquisition
    int setGainRx(quint8 gain);//gain pour tous les channels d'acquisition
    int setTensionHt(int volt);
private:
    bool isNewHT(int numTrig);
};

#endif // ICFRAIL_H
