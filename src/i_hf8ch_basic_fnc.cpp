#include "i_hf8ch_basic_fnc.h"
#include <QByteArray>
#include <ostream>
#include <unistd.h>
#include <fcntl.h>
#include <sys/poll.h>

I_hf8ch_basic_fnc::I_hf8ch_basic_fnc()
{

}

int I_hf8ch_basic_fnc::init()
{
    QByteArray driverRegistre("/dev/");
    driverRegistre.append("ifsttar_hf8ch-registre");
    _fd_registre = open(driverRegistre.toStdString().c_str(), O_RDWR);
    if(_fd_registre<0){
        perror("impossible d'ouvir /dev/ifsttar_hf8ch-registre\n");
        return -1;
    }
    QByteArray driverData("/dev/");
    driverData.append("ifsttar_hf8ch");
    _fd_data = open(driverData.toStdString().c_str(), O_RDWR);
    if(_fd_data<0){
        perror("impossible d'ouvir /dev/ifsttar_hf8ch\n");
        return -1;
    }
    return 0;
}

int I_hf8ch_basic_fnc::secure_write_registre(int registre, int buff)
{
    int ret;
    int tempbuff_write;
    int tempbuff_read;
    int nbTry = 0;
    bool countDone = false;
    tempbuff_write = buff;
    while(!countDone && nbTry < 5){
        ret = write_registre(registre,&tempbuff_write);
        if(ret < 0){
            perror("error secure_write\n");
            return ret;
        }
        ret = read_registre(registre,&tempbuff_read);
        if(ret < 0){
            perror("error secure_write\n");
            return ret;
        }
        if(tempbuff_read==tempbuff_write){
            countDone = true;
         //   std::cout << __FUNCTION__ << " : essay nb "<< nbTry <<std::endl;
        }else{
            nbTry++;
        }
    }
    if(!countDone){
        std::cerr << __FUNCTION__ << " : ERROR : ecriture sur FPGA impossible" << std::endl;
        return -1;
    }
    return ret;
}

int I_hf8ch_basic_fnc::read_registre(int registre,int* buff)
{
    return read_write(0,registre,buff);

}

int I_hf8ch_basic_fnc::write_registre(int registre, int* buff)
{
    return read_write(1,registre,buff);
}

int I_hf8ch_basic_fnc::read_data(int16_t *buff,int size, int timeout)
{
    int ret;
    int16_t * channelReceive;
    struct pollfd PollFd;
    PollFd.fd = _fd_data;
    PollFd.events = POLLIN | POLLPRI;
    int nbDataToRead,counterTimeout = 0,nbDataRestToRead = -1;
    int _maxDataRead = 250000;
    int nbRead = 0;
    do {
        ret = poll (&PollFd, 1, 500);
        if (ret > 0) {
            channelReceive = (int16_t *) malloc(size* sizeof(int16_t));
            nbDataRestToRead = size;
        //    std::cout << "read nbDataRestToRead = "<<size<<std::endl;
        //    std::cout << "read _maxDataRead = "<<_maxDataRead<<std::endl;
            nbRead = 0;
            while(nbDataRestToRead != 0){
                if(nbDataRestToRead<_maxDataRead){
                    nbDataToRead = nbDataRestToRead;
                }else{
                    nbDataToRead = _maxDataRead;
                }
                std::cout << "read nbDataToRead = "<<nbDataToRead<<std::endl;
                ret = read (_fd_data,(char *) channelReceive + nbRead*(_maxDataRead* sizeof(int16_t)), nbDataToRead* sizeof(int16_t));
                nbDataRestToRead -= nbDataToRead;
                nbRead++;
        //        std::cout << "nbRead = "<<nbRead <<" rest = "<<nbDataRestToRead<<std::endl;
            }
            memcpy(buff,channelReceive,size*sizeof(int16_t));
            free(channelReceive);
        }else{
            if(timeout >= 0 ){
                counterTimeout++;
                if(counterTimeout > (timeout * 2)){
                    return -1;
                }
            }
        }
    }while(nbDataRestToRead != 0);
    return 0;
}

int I_hf8ch_basic_fnc::write_signal(uint16_t *buff, int size)
{
    return write(_fd_data,buff,size);
}

int I_hf8ch_basic_fnc::set_bit_registre(int registre, int pos, bool value)
{
    int ret, valueRegister,mask;
    ret = read_registre(registre,&valueRegister);
    if(ret<0){
        std::cerr << __FUNCTION__ << " : ERROR read_register" << std::endl;
        return -1;
    }
    mask = 1 << pos;
  //  std::cout << __FUNCTION__ << " : registre = " << std::hex<<valueRegister<<std::dec << std::endl;
    // Appliquez le masque pour remplacer la valeur du bit
    if (value){
        valueRegister |= mask;
    }else{
        valueRegister &= ~mask;
    }
 //   std::cout << __FUNCTION__ << " : registre = " << std::hex<<valueRegister<<std::dec << std::endl;

    ret = write_registre(registre,&valueRegister);
    if(ret<0){
        std::cerr << __FUNCTION__ << " : ERROR read_register" << std::endl;
        return -2;
    }
    return ret;
}

int I_hf8ch_basic_fnc::get_bit_registre(int registre, int pos)
{
    int ret,valueRegister,mask;
    ret = read_registre(registre,&valueRegister);
    if(ret<0){
        std::cerr << __FUNCTION__ << " : ERROR read_register" << std::endl;
        return -1;
    }
    mask = 1 << pos;  // crée un masque avec un bit à 1 à la position 3
    return (valueRegister & mask) != 0;
}

int I_hf8ch_basic_fnc::read_write(bool mode,int registre,int *buff){
    int ret;
    int tempsize = 2 * sizeof(int);
    int tempbuff[2];
    tempbuff[0] = *buff;
    tempbuff[1] = registre;
    if(mode){
        ret = write(_fd_registre,tempbuff,tempsize);
    }else{
        ret = read(_fd_registre,tempbuff,tempsize);
        memcpy(buff,tempbuff,sizeof(int));
    }
    return ret;
}


