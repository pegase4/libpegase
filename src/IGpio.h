#ifndef IGPIO_H
#define IGPIO_H

#include <QObject>
#include <gpiod.h>
#include <QThread>
#include <QObject>

#include "IEvent.h"

#define	CONSUMER	"Consumer"

class IGpio : public QThread
{
    Q_OBJECT

    public:
        enum Gpiochip{PA=0,PB,PC,PD,PE,PF,PG,PH,PI,PZ};

        enum Event{RISING_EDGE = 1,FALLING_EDGE,BOTH_EDGES};
        Event convertirEnEnumEvent(int valeur) {
            return static_cast<Event>(valeur);
        }
        IGpio(Gpiochip gpiochip,int gpio_offset);
        IGpio(int gpio);

        int SetValue(bool value);
        int GetValue();

        void waitEvent(Event eventtype, int timeout = -1);

        struct data{
            int gpiochip;
            int gpio_offset;
            int timeout;
            int eventtype;
            IGpio* gpio;
        };
         data m_data;

        void run();
    private :
        int open();
        int close();
        struct gpiod_chip * m_chip;
        struct gpiod_line * m_line;

        // int event_cb(int event, unsigned int offset, const struct timespec *timestamp, void *unused);
    signals:
        void GpioRising(int,int,IGpio::Event,IEvent *);
        void GpioFalling(int,int,IGpio::Event,IEvent *);
        void GpioTimeout(int,int,IGpio::Event,IEvent *);
};

#endif // IGPIO_H