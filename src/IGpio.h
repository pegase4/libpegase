#ifndef IGPIO_H
#define IGPIO_H

#include <QObject>
#include <QThread>
#include <atomic>
#include <gpiod.h>
#include "IEvent.h"

#define	CONSUMER	"Consumer"

class IGpio : public QThread
{
    Q_OBJECT

    public:
        enum Gpiochip{PA=0,PB,PC,PD,PE,PF,PG,PH,PI,PZ};
        enum Direction{Input=0,Output,event};
        enum Event{RISING_EDGE = 1,FALLING_EDGE,BOTH_EDGES};


        IGpio(Gpiochip gpiochip,int gpio_offset);
        IGpio(int gpio);
        int open(Direction direction);
        int close();
        int SetValue(bool value);
        int GetValue();
        void waitEvent(Event eventtype, int timeout = -1);

        struct data {
            int gpiochip;
            int gpio_offset;
            int timeout;
            int eventtype;
            IGpio *gpio;
        };

    protected:
        void run() override;

    private:
        std::atomic<bool> m_running;
        int m_fd;
        int m_pipefd[2];
        struct gpiod_chip *m_chip;
        struct gpiod_line *m_line;
        void handleGpioInterrupt();
        data m_data;

    signals:
        void GpioRising(int,int,IGpio::Event,IEvent *);
        void GpioFalling(int,int,IGpio::Event,IEvent *);
        void GpioTimeout(int,int,IGpio::Event,IEvent *);
};

#endif // IGPIO_H
