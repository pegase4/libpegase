#include "ICfbeton.h"


ICfbeton::ICfbeton()
{

}

int ICfbeton::setChannelTx(int channelTx)
{
    int temp = 0x00000001;
    if(channelTx != 0){
        temp = temp << (channelTx-1)*2;
    }else{
        temp = 0;
    }
    return ICfhf8ch::setChannelTx(temp);
}

int ICfbeton::setGainRx(int channel, Gain1_ad8253 gain1, Gain2_ltc6912 gain2)
{
    int ret = 0;
    ret = setGainRx(channel,gain1 | gain2);
    return ret;
}

int ICfbeton::setGainRx(Gain1_ad8253 gain1, Gain2_ltc6912 gain2)
{
    int ret;
    for(int i =1; i < 9;i++){
        ret= setGainRx(i, gain1,gain2);
        if(ret < 0){
            break;
        }
    }
    return ret;
}

int ICfbeton::setGainRx(int channel, quint8 gain)
{
    return ICfhf8ch::setGainRx(channel,gain);
}

int ICfbeton::setGainRx(quint8 gain)
{
    int ret;
    for(int i =1; i < 9;i++){
        ret= setGainRx(i,gain);
        if(ret < 0){
            break;
        }
    }
    return ret;
}
