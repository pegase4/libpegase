#ifndef I_HF8CH_BASIC_FNC_H
#define I_HF8CH_BASIC_FNC_H
//#include <fcntl.h>
//#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <iostream>

class I_hf8ch_basic_fnc
{
public:
    I_hf8ch_basic_fnc();
    int init();
    int secure_write_registre(int registre,int buff);
    int read_registre(int registre,int* buff);
    int write_registre(int registre,int *buff);

    int readData(int16_t *buff,int size, int timeout = -1);
    int writeSignal(uint16_t *buff,int size);

    int setBitRegistre(int registre,int pos,bool value);
    int getbitRegistre(int registre,int pos);
private:
    int _fd_data;
    int _fd_registre;
    int read_write(bool mode,int registre,int *buff);
};

extern "C"
{
    I_hf8ch_basic_fnc*create_hf8ch_basic_fnc(){
        return new I_hf8ch_basic_fnc();
    }
    int init(I_hf8ch_basic_fnc *my_hf8ch_basic_fnc){
        return my_hf8ch_basic_fnc->init();
    }
    int secure_write_registre(I_hf8ch_basic_fnc *my_hf8ch_basic_fnc,int registre,int buff){
        return my_hf8ch_basic_fnc->secure_write_registre(registre,buff);
    }

    int read_registre(I_hf8ch_basic_fnc *my_hf8ch_basic_fnc,int registre,int* buff){
        return my_hf8ch_basic_fnc->read_registre(registre,buff);
    }

    int write_registre(I_hf8ch_basic_fnc *my_hf8ch_basic_fnc,int registre,int *buff){
        return my_hf8ch_basic_fnc->write_registre(registre,buff);
    }

    int readData(I_hf8ch_basic_fnc *my_hf8ch_basic_fnc,int16_t *buff,int size,int timeout = -1){
        return my_hf8ch_basic_fnc->readData(buff,size,timeout);
    }

    int witeSignal(I_hf8ch_basic_fnc *my_hf8ch_basic_fnc,uint16_t *buff,int size){
        return my_hf8ch_basic_fnc->writeSignal(buff,size);
    }

    int setBitRegistre(I_hf8ch_basic_fnc *my_hf8ch_basic_fnc,int registre,int pos,bool value){
        return my_hf8ch_basic_fnc->setBitRegistre(registre,pos,value);
    }

    int getbitRegistre(I_hf8ch_basic_fnc *my_hf8ch_basic_fnc,int registre,int pos){
       return my_hf8ch_basic_fnc->getbitRegistre(registre,pos);
    }
}
#endif // I_HF8CH_BASIC_FNC_H
