#ifndef IANALOGDATA_H
#define IANALOGDATA_H

#include <QtGlobal>
#include <QList>
#include <QVector>
#include <math.h>       /* pow */


class IAnalogData
{
public:
    IAnalogData();
    IAnalogData(int nbChannels);
    quint64 m_s;
    quint32 m_ns;
    QVector<int> m_data;
    bool m_Default;

    QVector<int> dataToUv() const;
};

#endif // IANALOGDATA_H
