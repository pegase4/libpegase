#include "ICfg8chReadTrig.h"

ICfg8chReadTrig::ICfg8chReadTrig(int fd)
{
    m_fd = fd;
    m_running = true;
}

ICfg8chReadTrig::~ICfg8chReadTrig()
{
    m_running = false;
}

void ICfg8chReadTrig::run()
{
    int nbdata,ret,statusTrigOffTemp;
    IEvent *newEvent;
    void* pBufferTrig;
    ReadTrigChanel_t* dataMax ;

    struct pollfd PollFd;
    PollFd.fd = m_fd;
    PollFd.events = POLLIN | POLLPRI;
    pBufferTrig = malloc(sizeof(ReadTrigChanel_t) + sizeof(ReadTrigChanel48_t) * 2047);
    dataMax = (ReadTrigChanel_t*) malloc(sizeof(ReadTrigChanel_t) * 2048);
    do {
        ret = poll (&PollFd, 1, 500);
        if (ret > 0) {
            nbdata = read (m_fd, pBufferTrig, sizeof(ReadTrigChanel_t) + sizeof(ReadTrigChanel48_t) * 2047);
            if(nbdata >=0){
                newEvent = new IEvent();
                ret = ioctl(m_fd, CF8CH_GET_STATUS_TRIG_OFF, &statusTrigOffTemp);
                if (ret < 0) {
                    perror(NULL);
                    free(pBufferTrig);
                }
                if ((statusTrigOffTemp & 0xff00) != (m_statusTrigOff & 0xff00)) {
                    m_statusTrigOff = statusTrigOffTemp;
                    emit Error_trig_off(m_statusTrigOff,newEvent);
                }

                memcpy(dataMax, pBufferTrig, sizeof(ReadTrigChanel_t));
                uint64_t cpt = dataMax[0].ts;
                for(int i=1; i<nbdata; i++ ){
                    ReadTrigChanel48_t* dataTemp = reinterpret_cast<ReadTrigChanel48_t*>(static_cast<char*>(pBufferTrig) + sizeof(ReadTrigChanel_t) + (i-1)*sizeof(ReadTrigChanel48_t));
                    dataMax[i].trig = static_cast<uint16_t>(dataTemp->trig);
                    cpt = cpt + dataTemp->dtS;
                    dataMax[i].ts = cpt;
                    dataMax[i].nanoS = static_cast<uint32_t>(dataTemp->nanoS);
                }
                emit dataAvaible(dataMax,nbdata,newEvent);
                delete(newEvent);
               //
            }
        }
    }while(m_running);
    free(dataMax);
    free(pBufferTrig);

}
