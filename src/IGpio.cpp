#include "IGpio.h"
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <QDebug>
#include <linux/gpio.h>
#include <QCoreApplication>

#define	CONSUMER	"Consumer"


IGpio::IGpio(Gpiochip gpiochip,int gpio_offset)
{
    m_data.gpiochip=gpiochip;
    m_data.gpio_offset=gpio_offset;
    qRegisterMetaType<IGpio::Event>("IGpio::Event");
}

IGpio::IGpio(int gpio)
{
    m_data.gpiochip = gpio/16;
    m_data.gpio_offset = gpio%16;
    qRegisterMetaType<IGpio::Event>("IGpio::Event");
}

int IGpio::open(){
    QString schipname;
    schipname= QString("gpiochip%1").arg(m_data.gpiochip);
    m_chip = gpiod_chip_open_by_name(schipname.toLocal8Bit().data());
    if (!m_chip) {
        perror("Open chip failed\n");
        return -1;
    }
    m_line = gpiod_chip_get_line(m_chip,m_data.gpio_offset);
    if (!m_line) {
        perror("Get line failed\n");
            return -2;

    }
    return 0;
}

int IGpio::close(){
    gpiod_line_release(m_line);
    gpiod_chip_close(m_chip);
    return 0;
}

int IGpio::SetValue(bool value)
{
    int ret;
    ret = this->open();
    if (ret < 0) {
        perror("Open gpio failed\n");
        return ret;
    }
    ret = gpiod_line_request_output(m_line, CONSUMER, 0);
    if (ret < 0) {
        perror("Request line as output failed\n");
        return ret;
    }

    ret = gpiod_line_set_value(m_line, value);
    if (ret < 0) {
        perror("set value failed\n");
        return ret;
    }
    this->close();
    return 0;
}

int IGpio::GetValue()
{
    int ret;
    ret = this->open();
    if (ret < 0) {
        perror("Open gpio failed\n");
        return ret;
    }
    ret = gpiod_line_request_input(m_line, CONSUMER);
    if (ret < 0) {
        perror("Request line as output failed\n");
        return ret;
    }

    ret = gpiod_line_get_value(m_line);
    if (ret < 0) {
        perror("get value failed\n");
        return ret;
    }
    this->close();
    return 0;
}

int event_cb(int event, unsigned int offset, const struct timespec *timestamp, void *unused)
{
    // Cast du void* en MaStructure*
    IGpio::data* pointeurStructure = static_cast<IGpio::data*>(unused);
    (void)(timestamp);
     IEvent *newEvent;
    newEvent = new IEvent();
    IGpio* gpio= pointeurStructure->gpio;
    if(event == GPIOD_CTXLESS_EVENT_CB_RISING_EDGE){
        emit gpio->GpioRising(pointeurStructure->gpiochip,offset,gpio->convertirEnEnumEvent(event),newEvent);
    }else if(event == GPIOD_CTXLESS_EVENT_CB_FALLING_EDGE){
        emit gpio->GpioFalling(pointeurStructure->gpiochip,offset,gpio->convertirEnEnumEvent(event),newEvent);
    }else if(event ==GPIOD_CTXLESS_EVENT_CB_TIMEOUT){
        emit gpio->GpioTimeout(pointeurStructure->gpiochip,offset,gpio->convertirEnEnumEvent(event),newEvent);
    }
    return GPIOD_CTXLESS_EVENT_CB_RET_OK;
}

void IGpio::waitEvent(Event eventtype, int timeout)
{
    m_data.eventtype=eventtype;
    m_data.timeout=timeout;
    m_data.gpio=this;
}

void IGpio::run()
{
    QString schipname;

    const char* c = "gpio-event";
    schipname= QString("gpiochip%1").arg(m_data.gpiochip);

   void* pointeurVoid = &m_data;
    if(m_data.timeout > 0){
        struct timespec ts = { m_data.timeout, 0 };
        gpiod_ctxless_event_monitor(schipname.toLocal8Bit().data(), m_data.eventtype,m_data.gpio_offset, 0, c, &ts, NULL, &event_cb,pointeurVoid);
    }else{
        gpiod_ctxless_event_monitor(schipname.toLocal8Bit().data(), m_data.eventtype,m_data.gpio_offset, 0, c, NULL, NULL, &event_cb,pointeurVoid);
    }

}
