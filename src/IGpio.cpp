#include "IGpio.h"
#include <QDebug>
#include <fcntl.h>
#include <unistd.h>
#include <linux/gpio.h>
#include <poll.h>

IGpio::IGpio(Gpiochip gpiochip, int gpio_offset)
    : m_running(false), m_fd(-1)
{
    qDebug() << "✅ IGpio construit avec gpiochip =" << gpiochip << " offset =" << gpio_offset;
    m_data.gpiochip = gpiochip;
    m_data.gpio_offset = gpio_offset;
    qRegisterMetaType<IGpio::Event>("IGpio::Event");
}

IGpio::IGpio(int gpio)
    : m_running(false), m_fd(-1)
{
    qDebug() << "✅ IGpio construit avec gpio =" << gpio;
    m_data.gpiochip = gpio / 16;
    m_data.gpio_offset = gpio % 16;
    qRegisterMetaType<IGpio::Event>("IGpio::Event");
}

int IGpio::open(Direction direction)
{
    QString schipname = QString("gpiochip%1").arg(m_data.gpiochip);
    m_chip = gpiod_chip_open_by_name(schipname.toLocal8Bit().data());
    if (!m_chip) {
        perror("❌ ERREUR: Échec de l'ouverture du gpiochip !");
        return -1;
    }

    m_line = gpiod_chip_get_line(m_chip, m_data.gpio_offset);
    if (!m_line) {
        perror("❌ ERREUR: Échec de l'obtention de la ligne GPIO !");
        gpiod_chip_close(m_chip);
        return -2;
    }

    int ret;
    if (direction == Output) {
        ret = gpiod_line_request_output(m_line, CONSUMER, 0);
    } else if (direction == Input) {
        ret = gpiod_line_request_input(m_line, CONSUMER);
    } else if (direction == event) {
        ret = gpiod_line_request_both_edges_events(m_line, CONSUMER);
    } else {
        perror("❌ ERREUR: Direction invalide !");
        return -3;
    }

    if (ret < 0) {
        perror("❌ ERREUR: Impossible de configurer la ligne GPIO !");
        gpiod_chip_close(m_chip);
        return -3;
    }

    return 0;
}

int IGpio::SetValue(bool value)
{
    int ret = gpiod_line_set_value(m_line, value);

    if (ret < 0) {
        perror("set value failed\n");
        return ret;
    }
    return 0;
}

int IGpio::GetValue()
{
    int ret = gpiod_line_get_value(m_line);

    if (ret < 0) {
        perror("get value failed\n");
        return ret;
    }
    return ret;
}

void IGpio::waitEvent(Event eventtype, int timeout)
{
    qDebug() << "✅ Entrée dans waitEvent() avec eventtype =" << eventtype << " timeout =" << timeout;

    if (!m_line) {
        qDebug() << "❌ ERREUR: `m_line` est NULL ! Impossible de configurer les événements.";
        return;
    }

    m_data.eventtype = eventtype;
    m_data.timeout = timeout;
    m_data.gpio = this;

    m_fd = gpiod_line_event_get_fd(m_line);
    if (m_fd < 0) {
        perror("❌ ERREUR: Impossible d'obtenir le file descriptor pour le GPIO !");
        return;
    }

    qDebug() << "✅ File descriptor GPIO obtenu: " << m_fd;

    start();  // 🔥 Démarrer le thread qui écoute les interruptions
}

void IGpio::run()
{
    qDebug() << "✅ Démarrage du thread de surveillance GPIO...";


    if (pipe(m_pipefd) < 0) {
        perror("❌ ERREUR: Impossible de créer le pipe !");
    }

    // ✅ Met le pipe en mode "non-bloquant" pour éviter tout blocage accidentel
    fcntl(m_pipefd[0], F_SETFL, O_NONBLOCK);
    fcntl(m_pipefd[1], F_SETFL, O_NONBLOCK);

    struct pollfd pfd[2];
    pfd[0].fd = m_fd;
    pfd[0].events = POLLIN;
    pfd[1].fd = m_pipefd[0];  // ✅ Ajout du pipe
    pfd[1].events = POLLIN;

    m_running = true;

    while (m_running)
    {
        int ret = poll(pfd, 2, -1);  // ✅ Attendre interruption GPIO ou arrêt via pipe

        if (ret > 0)
        {
            if (pfd[0].revents & POLLIN) {
                handleGpioInterrupt();
            }
            if (pfd[1].revents & POLLIN) {  // ✅ Signal d'arrêt reçu
                qDebug() << "🛑 Signal d'arrêt reçu, sortie du thread GPIO...";
                break;
            }
        }
        else if (ret < 0)
        {
            perror("❌ ERREUR: `poll()` a échoué !");
        }
    }

    qDebug() << "🛑 Thread GPIO arrêté.";
}

void IGpio::handleGpioInterrupt()
{
    qDebug() << "📢 `handleGpioInterrupt()` appelé !";

    if (!m_line) {
        qDebug() << "❌ ERREUR: `m_line` est NULL, arrêt du handler.";
        return;
    }

    struct gpiod_line_event event;
    int ret ;

    // ✅ Lecture de l'événement GPIO
    ret = gpiod_line_event_read(m_line, &event);
    if (ret < 0) {
        perror("❌ ERREUR: `gpiod_line_event_read()` a échoué !");
        return;
    }

    qDebug() << "✅ Interruption détectée sur GPIO" << m_data.gpiochip << " offset " << m_data.gpio_offset << "évènement de type :" << event.event_type;

    IEvent *newEvent = new IEvent();
    IGpio* gpio= this;
    if(event.event_type == GPIOD_LINE_EVENT_RISING_EDGE)
        emit gpio->GpioRising(m_data.gpiochip, m_data.gpio_offset, static_cast<IGpio::Event>(event.event_type), newEvent);
    else if(event.event_type == GPIOD_LINE_EVENT_FALLING_EDGE)
        emit gpio->GpioFalling(m_data.gpiochip, m_data.gpio_offset, static_cast<IGpio::Event>(event.event_type), newEvent);
}


int IGpio::close()
{
    qDebug() << "🛑 Fermeture du GPIO et arrêt du thread...";

    if(m_running)
    {
        m_running = false;

        if (m_pipefd[1] >= 0) {
            write(m_pipefd[1], "x", 1);
        }

        if (m_fd >= 0) {
            ::close(m_fd);
            m_fd = -1;
        }

        if (isRunning()) {
            wait();
        }

        if (m_pipefd[0] >= 0) {
            ::close(m_pipefd[0]);
            m_pipefd[0] = -1;
        }
        if (m_pipefd[1] >= 0) {
            ::close(m_pipefd[1]);
            m_pipefd[1] = -1;
        }
    }

    gpiod_line_release(m_line);
    gpiod_chip_close(m_chip);

    qDebug() << "✅ GPIO fermé.";
    return 0;
}
