#ifndef IGNUMDATA_H
#define IGNUMDATA_H

#include <QtGlobal>
#include <QList>
#include <ifsttar_g8ch.h>

class IGnumData
{
public:
    IGnumData();
    quint64 m_s;
    quint32 m_ns;
    quint16 m_data;
    quint32 m_nbPasEvent;
    ppsInfos m_pps;
    bool m_Default;

    QList<bool> dataToQList();
};

#endif // IGNUMDATA_H
