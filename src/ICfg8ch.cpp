#include "ICfg8ch.h"

typedef QList<int> list;

ICfg8ch::ICfg8ch()
{
    qRegisterMetaType<QVector<IAnalogData>>("QVector<IAnalogData>");
    qRegisterMetaType<QVector<IGnumData>>("QVector<IGnumData>");
    qRegisterMetaType<ppsInfos>("ppsInfos");

    m_running = true;
}

ICfg8ch::~ICfg8ch()
{
    delete(readPpsThread);
    m_running = false;
}


/**
 *	\fn		int ICf8ch::Init()
 *
 *	\brief	Open /dev/PPS8ch and start thread to pool the fd
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::Init()
{
//    if(!modprobeDriver()){
    if(modprobeDriver() < 0){
        return -1;
    }
    if(OpenPps() < 0){
        return -1;
    }
    if(StartPps() < 0){
        return -1;
    }
    return 0;
}

/**
 *	\fn		int ICf8ch::modprobeDriver()
 *
 *	\brief	Launch ifsttar-g8ch driver : Analog and Numerical driver
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::modprobeDriver()
{
    int ret;
    
    QString commandLine = "modprobe";
    QStringList arguments;
    arguments << "ifsttar-g8ch";
    QProcess *myProcess = new QProcess();
    myProcess->start(commandLine,arguments);

    myProcess->waitForFinished(2000);
    ret = myProcess->exitCode();
    if (ret < 0){
        //perror("modprobe ifsttar-g8ch error\n");
        qDebug() << ICfg8ch_NAME << __FUNCTION__ << "modprobe ifsttar-g8ch error" << Qt::endl;
        return ret;
    }
    
    sleep(2);           // Sleep important dans le cas où le driver ifsttar-g8ch ne s'installe pas car erreur de modprobe

    QStringList listDriver;
    QString pathDir = "/sys/module/";
    QDir driverDir(pathDir);
    if(!driverDir.exists()){
        qDebug() << ICfg8ch_NAME << __FUNCTION__ <<" : Can open /sys/module/ dir" << Qt::endl;
        return -1;
    }

    listDriver = driverDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    QString driver = "ifsttar_g8ch";
    if(!listDriver.contains(driver)){
        qDebug() << ICfg8ch_NAME << __FUNCTION__ <<" : the driver is not here"<< Qt::endl;
        qDebug() << listDriver << " not contain "<< driver <<Qt::endl;
        return -1;
    }

    return 0;
}


/**
 *	\fn		int ICf8ch::SetCanConfig(ICf8ch::confCan newConfCan)
 *
 *	\brief	set Can config and check the value of it
 *
 *  \param [confCan] newConfCan :	config of can
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::SetCanConfig(confCan newConfCan)
{
    int ret = 0;
    if(m_stateCan == CAN_NOT_OPENED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : can not open, need to openCan before"<<Qt::endl;
        ret = -1;
    }else if(m_stateCan == CAN_STARTED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : can started, need to stopCan before"<<Qt::endl;
        ret = -1;
    }else{
        if(newConfCan.count != 0){
            if((newConfCan.frequency/newConfCan.count) > CAN_FREQ_MAX_IT){
                IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Error Fe/count ("<<newConfCan.frequency<<"/"<<newConfCan.count <<") is out of range (maximum 20)" <<Qt::endl;
                ret = -1;
            }
        }
        if(newConfCan.frequency < CAN_FREQ_MIN || newConfCan.frequency > CAN_FREQ_MAX){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : fe is out of range ["<<newConfCan.frequency<<":"<< CAN_FREQ_MAX << "]" <<Qt::endl;
            ret = -1;
        }
        if(newConfCan.count < 1 || newConfCan.count > CAN_COUNT_MAX){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : count ("<< newConfCan.count<<") is out of range [1:"<< CAN_COUNT_MAX << "]" <<Qt::endl;
            ret = -1;
        }
        if(!newConfCan.channel){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : channel = 0"<<Qt::endl;
            ret = -1;
        }
        if(newConfCan.mode != CAN_MODE_HIGH_SPEED &&
           newConfCan.mode != CAN_MODE_HIGH_RESOLUTION &&
           newConfCan.mode != CAN_MODE_LOW_POWER &&
           newConfCan.mode != CAN_MODE_LOW_SPEED){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : mode is not correctly configured"<<Qt::endl;
            ret = -1;
        }
        if(!ret){
            m_confCan = newConfCan;
            std::bitset<8> nbChannel (m_confCan.channel);
            m_nbCanChannel = nbChannel.count();
            m_stateCan = CAN_CONFIGURATED;
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : done"<<Qt::endl;
        }
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::SetTrigConfig(confTrig newConfTrig)
 *
 *	\brief	set trig config and check the value of it
 *
 *  \param [confTrig] newConfTrig :	config of trig
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::SetTrigConfig(confTrig newConfTrig)
{
    int ret = 0;
    if(m_stateTrig == TRIG_NOT_OPENED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : trig not open, need to openTrig before"<<Qt::endl;

        ret = -1;
    }else if(m_stateTrig == TRIG_STARTED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : trig started, need to stopTrig before"<<Qt::endl;
        ret = -1;
    }else{
        if(!newConfTrig.channel){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : channel = 0"<<Qt::endl;
            ret = -1;
        }
        if(newConfTrig.timeout < 1 || newConfTrig.timeout > TRIG_TIMEOUT_MAX){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : timeout is out of range [1:"<< TRIG_TIMEOUT_MAX << "]" <<Qt::endl;
            ret = -1;
        }
        if(newConfTrig.nbTrigSecurity < 5 || newConfTrig.nbTrigSecurity > TRIG_NB_TRIG_SECURITY){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : nbTrigSecurity is out of range [1:"<< TRIG_NB_TRIG_SECURITY << "]" <<Qt::endl;
            ret = -1;
        }
        if(!ret){
            m_confTrig = newConfTrig;
            m_stateTrig = TRIG_CONFIGURATED;
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : done"<<Qt::endl;
        }
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::OpenCan()
 *
 *	\brief	open /dev/can8ch
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::OpenCan(){
    int ret = 0;
    if(m_statePps != PPS_STARTED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : need to int Init() before"<<Qt::endl;
        return -1;
    }
    if(m_stateCan == CAN_NOT_OPENED){
        //ouverture du driver /can
        m_canFd = open("/dev/cfg8chCan", O_RDWR);
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : m_canFd = "<< QString::number(m_canFd) << Qt::endl;
        if (m_canFd == -1)
        {
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : open /dev/cfg8chCan"<< Qt::endl;
            ret = -1;
        }else{
            m_stateCan = CAN_OPENED;
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : done"<<Qt::endl;
        }
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ret = "<< QString::number(ret) << Qt::endl;
    }else{
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : Can already opened" << Qt::endl;
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::OpenTrig()
 *
 *	\brief	open /dev/trigN
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::OpenTrig(){
    int ret = 0;
    if(m_statePps != PPS_STARTED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : need to int Init() before"<<Qt::endl;
        return -1;
    }
    if(m_stateTrig == TRIG_NOT_OPENED){
        //ouverture du driver /cfg8chTrig
        m_trigFd = open("/dev/cfg8chTrig", O_RDWR);
        if (m_trigFd == -1)
        {
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : open /dev/cfg8chTrig"<<Qt::endl;
            ret = -1;
        }else{
            m_stateTrig = TRIG_OPENED;
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : done"<<Qt::endl;
        }
    }else{
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : Trig already opened" << Qt::endl;
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::OpenPps()
 *
 *	\brief	open /dev/PPS8ch
 *
 *	\return A negative number if an error occured, 0 otherwise
 *
*/
int ICfg8ch::OpenPps(){
    int ret = 0;
    if(m_statePps == PPS_NOT_OPENED){
        //ouverture du driver /cfg8chgPPS
        m_ppsFd = open("/dev/cfg8chgPPS", O_RDWR);

        if (m_ppsFd == -1){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : open /dev/cfg8chgPPS"<< Qt::endl;
            ret = -1;
        }else{
            m_statePps = PPS_OPENED;
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : done"<< Qt::endl;
        }
    }else{
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << "PPS already opened" << Qt::endl;
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::CloseCan()
 *
 *	\brief	close /dev/can8ch
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::CloseCan()
{
    int ret = 0;
    if(m_stateCan == CAN_NOT_OPENED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : Can already closed"<< Qt::endl;
        ret = -1;
    }else if (m_stateCan == CAN_STARTED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : Can started, need to stop before"<< Qt::endl;
        ret = -1;
    }else{
        if(m_canFd != -1){
            close(m_canFd);
            m_stateCan = CAN_NOT_OPENED;
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Can close"<< Qt::endl;
            if(ClosePps() == -1){
                IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : fail to close pps"<< Qt::endl;
            }
        }
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::CloseTrig()
 *
 *	\brief	close /dev/trigN
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::CloseTrig()
{
    int ret = 0;
    if(m_stateTrig == TRIG_NOT_OPENED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : Trig already closed"<< Qt::endl;
        ret = -1;
    }else if (m_stateTrig == TRIG_STARTED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : Trig started, need to stop before"<< Qt::endl;
        ret = -1;
    }else{
        if(m_trigFd != -1){
            close(m_trigFd);
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Trig close"<< Qt::endl;
            m_stateTrig = TRIG_NOT_OPENED;
            if(ClosePps() == -1){
                IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : fail to close pps"<< Qt::endl;
            }
        }
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::ClosePps()
 *
 *	\brief	close /dev/PPS8ch
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::ClosePps(){
    int ret = 0;
    if(m_stateTrig != TRIG_NOT_OPENED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : Can't close PPS, Trig is opened" << Qt::endl;
        ret = -1;
    }else if(m_stateCan != CAN_NOT_OPENED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : Can't close PPS, Can is opened" << Qt::endl;
        ret = -1;
    }else if(m_statePps == PPS_NOT_OPENED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : PPS is already close" << Qt::endl;
        ret = -1;
    }else if(m_statePps == PPS_STARTED){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : PPS is started, need to close before" << Qt::endl;
        ret = -1;
    }else{
        if(m_ppsFd != -1){
            close(m_ppsFd);
            m_statePps = PPS_NOT_OPENED;
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Pps close"<< Qt::endl;

        }
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::StartPps()
 *
 *	\brief	nothing in this fonction, just change statePPS
 *
 *	\return A negative number if an error occured, 0 otherwise
 */
int ICfg8ch::StartPps()
{
    int ret = 0;
    if(m_statePps == PPS_OPENED || m_statePps == PPS_STOPED){

        m_statePps = PPS_STARTED;
        readPpsThread = new ICfg8chReadPps(m_ppsFd);
        connect(readPpsThread, SIGNAL(dataAvaible(ppsInfos, IEvent*)), this, SLOT(ReadPps(ppsInfos,IEvent*)), Qt::QueuedConnection);
        readPpsThread->start();
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : done"<< Qt::endl;
    }else if(m_statePps == PPS_STARTED){

        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : PPS already started"<< Qt::endl;
        ret = -1;
    }else if(m_statePps == PPS_NOT_OPENED){

        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : PPS not opened, need to openPps before"<< Qt::endl;
        ret = -1;
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::ReadPps()
 *
 *	\brief	read ppsInfo and save it in a queue
 *
 */
void ICfg8ch::ReadPps(ppsInfos newPpsInfo, IEvent *event)
{
    (void)(event);
    m_ppsInfosMutex.lock();
    m_ppsInfosQueue.enqueue(newPpsInfo);
    m_ppsInfosQueue[m_ppsInfosQueue.size()-1].qerr_end = newPpsInfo.qerr_beginning;
    m_ppsInfosQueue[m_ppsInfosQueue.size()-1].period = newPpsInfo.period;
    if(m_ppsInfosQueue.size() > NB_SECONDE_PPS_INFO + m_confTrig.timeout){
        m_ppsInfosQueue.dequeue();
    }
    m_ppsInfosMutex.unlock();
}

/**
 *	\fn		ICf8ch::calcDateData(quint64 seconde, quint32 cpt)
 *
 * \param [seconde] : seconde epoch
 *
 * \param [cpt] : counter of event in FPGA
 *
 *	\brief	read ppsInfo and save it in a queue
 *
 * \return date in seconde epoch and nanoSec
 *
 */
ICfg8ch::dateData ICfg8ch::calcDateData(quint64 seconde, quint32 cpt)
{
    dateData dateDataReturn;
    quint64 calcTempSeconde;
    double calcPasPeriod;
    m_ppsInfosMutex.lock();
    foreach (ppsInfos myPpsInfos, m_ppsInfosQueue) {
        if(myPpsInfos.time == seconde){
            dateDataReturn.valeurReel =  true;
            calcTempSeconde =1000000000000; //une seconde en ps
            calcTempSeconde += (-1 *myPpsInfos.qerr_end) + myPpsInfos.qerr_beginning;

            calcPasPeriod = cpt;
            calcPasPeriod /= myPpsInfos.period;

            calcTempSeconde = (-1 * myPpsInfos.qerr_beginning) + (calcPasPeriod * calcTempSeconde);
            calcTempSeconde /= 1000; //en ns

            dateDataReturn.nanoSeconde = (quint32) calcTempSeconde % 1000000000;
            dateDataReturn.seconde = myPpsInfos.time + (calcTempSeconde / 1000000000);
            dateDataReturn.pps = myPpsInfos;
        }
    }
    if(dateDataReturn.seconde == 0){
        dateDataReturn.valeurReel =  false;
        calcTempSeconde =1000000000000; //une seconde en ps

        calcPasPeriod = cpt;
        if(!m_ppsInfosQueue.isEmpty()){
            calcPasPeriod /= m_ppsInfosQueue.first().period;
        }else{
            calcPasPeriod /= PLL_THEORIQUE;
        }

        calcTempSeconde = calcPasPeriod * calcTempSeconde;
        calcTempSeconde /= 1000; //en ns

        dateDataReturn.nanoSeconde = (quint32)calcTempSeconde % 1000000000;
        dateDataReturn.seconde = seconde + (calcTempSeconde / 1000000000);
    }

    m_ppsInfosMutex.unlock();
    return dateDataReturn;
}

QQueue<ppsInfos> ICfg8ch::ppsInfosQueue() const
{
    return m_ppsInfosQueue;
}


/**
 *	\fn		int ICf8ch::StartCan()
 *
 *	\brief	set all parameter in the FPGA and start CAN
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::StartCan()
{
    int ret = 0;
    if(m_stateCan == CAN_STARTED){
        ret = -1;
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : CanData is already started"<< Qt::endl;
    }else if(m_stateCan == CAN_OPENED){
        ret = -1;
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : Can is not configured, need to setCanConfig before"<< Qt::endl;
    }else if(m_stateCan == CAN_NOT_OPENED){
        ret = -1;
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : Can is not opened, need to openCan and setCanConfig before"<< Qt::endl;
    }else{

        if(SetFeCanToFPGA()){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : SetFeCanToFPGA"<< Qt::endl;
            return -1;
        }else{
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : set Can Fe = "<< m_confCan.frequency << " OK" << Qt::endl;
        }
        if(SetModeCanToFPGA()){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : SetModeCanToFPGA"<< Qt::endl;
            return -1;
        }else{
            IStdout ()<< ICfg8ch_NAME << __FUNCTION__ <<" : set Can Mode = "<< m_confCan.mode << " OK" << Qt::endl;
        }
        if(SetCountCanToFPGA()){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : SetCountCanToFPGA"<< Qt::endl;
            return -1;
        }else{
            IStdout ()<< ICfg8ch_NAME << __FUNCTION__ <<" : set Can Count = "<< m_confCan.count << " OK" << Qt::endl;
        }

        if(SetChannelCanToFPGA()){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : SetChannelCanToFPGA"<< Qt::endl;
            return -1;
        }else{
            IStdout ()<< ICfg8ch_NAME << __FUNCTION__ <<" : set Can Channel = "<< m_confCan.channel << " OK" << Qt::endl;
        }
        //Activation du Can à travers le FPGA
        ret = ioctl(m_canFd, CF8CH_ENABLE_CAN_ACQUISITION);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }
        IStdout ()<< ICfg8ch_NAME << __FUNCTION__ <<" : acquisition Can is started" << Qt::endl;
        readCanThread = new ICfg8chReadCan(m_canFd,m_confCan.count,m_nbCanChannel);
        connect(readCanThread, SIGNAL(dataAvaible(uint32_t*,int, int,quint64, quint32,IEvent*)), this, SLOT(ReadCan(uint32_t*,int,int,quint64,quint32,IEvent*)), Qt::QueuedConnection);
        connect(readCanThread, SIGNAL(Error_overwrite_dma(int,IEvent*)), this, SLOT(Error_overwrite_dma_slot(int,IEvent*)), Qt::QueuedConnection);
        readCanThread->start();

        m_stateCan = CAN_STARTED;
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::StartTrig()
 *
 *	\brief	set all parameter in the FPGA and start TRIG
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::StartTrig(){
    int ret = 0;
    if(m_stateTrig == TRIG_STARTED){
        ret = -1;
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : TrigData is already started"<< Qt::endl;
    }else if(m_stateTrig == TRIG_OPENED){
        ret = -1;
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : Trig is not configured, need to setTrigConfig before"<< Qt::endl;
    }else if(m_stateTrig == TRIG_NOT_OPENED){
        ret = -1;
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : Trig is not opened, need to openCan and setTrigConfig before"<< Qt::endl;
    }else{
        if(SetTimeOutTrigToFPGA()){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : SetTimeOutTrigToFPGA"<< Qt::endl;
            return -1;
        }else{
            IStdout ()<< ICfg8ch_NAME << __FUNCTION__ <<" : set Trig timeout = "<< m_confTrig.timeout << " OK" << Qt::endl;
        }

        if(SetChannelTrigToFPGA()){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : SetChannelTrigToFPGA"<< Qt::endl;
            return -1;
        }else{
            IStdout ()<< ICfg8ch_NAME << __FUNCTION__ <<" : set Trig Channel = "<< m_confTrig.channel << " OK" << Qt::endl;
        }

        if(SetNbTrigOffToFPGA()){
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : SetNbTrigOffToFPGA"<< Qt::endl;
            return -1;
        }else{
            IStdout ()<< ICfg8ch_NAME << __FUNCTION__ <<" : set Trig TrigOff = "<< m_confTrig.nbTrigSecurity << " OK" << Qt::endl;
        }

        ret = ioctl(m_trigFd, CF8CH_ENABLE_TRIG_ACQUISITION);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }

        IStdout ()<< ICfg8ch_NAME << __FUNCTION__ <<" : acquisition Trig is started" << Qt::endl;
        m_stateTrig = TRIG_STARTED;
        readTrigThread = new ICfg8chReadTrig(m_trigFd);
        connect(readTrigThread, SIGNAL(dataAvaible(ReadTrigChanel_t*,int, IEvent*)), this, SLOT(ReadTrig(ReadTrigChanel_t*,int,IEvent*)), Qt::QueuedConnection);
        connect(readTrigThread, SIGNAL(Error_trig_off(int, IEvent*)), this, SLOT(Error_trig_off_slot(int,IEvent*)), Qt::QueuedConnection);
        readTrigThread->start();
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::StopCan()
 *
 *	\brief	disable CAN acquisition
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::StopCan()
{
    int ret = 0;
    ret = ioctl(m_canFd, CF8CH_DISABLE_CAN_ACQUISITION);
    if (ret < 0) {
        perror(NULL);
        return ret;
    }
    delete(readCanThread);
    return ret;
}

/**
 *	\fn		int ICf8ch::StopCan()
 *
 *	\brief	disable TRIG acquisition
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::StopTrig()
{
    int channel_desactived = 0,ret = 0;
    ret = ioctl(m_trigFd, CF8CH_SET_CHANELS_TRIG, &channel_desactived);
    if (ret < 0) {
        perror(NULL);
        return ret;
    }
    ret = ioctl(m_canFd, CF8CH_DISABLE_TRIG_ACQUISITION);
    if (ret < 0) {
        perror(NULL);
        return ret;
    }
    delete(readTrigThread);
    return ret;
}

/**
 *	\fn		QString ICf8ch::showConfig()
 *
 *	\brief	create a QString with all parameter
 *
 * \return QString with all parameter
 *
 */
QString ICfg8ch::showConfig()
{
QString config;
return config;
}

/**
 *	\fn		quint8 ICf8ch::getStatusTrigOff()
 *
 *	\brief	return mask of channel trig that is disable by FPGA
 *
 * \return mask of channel disable
 *
 */
quint8 ICfg8ch::getStatusTrigOff()
{
    if(m_stateTrig != TRIG_NOT_OPENED){
        int statusTrigOffTemp = 0, ret;
        ret = ioctl(m_trigFd, CF8CH_GET_STATUS_TRIG_OFF, &statusTrigOffTemp);
        if (ret < 0) {
            return -1;
        }
        return ((statusTrigOffTemp & 0xff00)>>8);
    }else{
        return -1;
    }
}

/**
 *	\fn		int ICf8ch::resetStatusTrigOff()
 *
 *	\brief	reactivate channel trig that is disable by FPGA
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::resetStatusTrigOff()
{
    if(m_stateTrig != TRIG_NOT_OPENED){
        int ret = ioctl(m_trigFd, CF8CH_RESET_TRIG_OFF);
        if (ret < 0) {
            return -1;
        }
        return ret;
    }else{
        return -1;
    }
}

/**
 *	\fn		int ICf8ch::GetRealFE(float &fFe)
 *
 *	\brief	get the real frequency of FPGA's CAN
 *
 * \param [float] pointer of real frequency
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::GetRealFE(float &fFe)
{
    int ret = 0;
//    quint32 u32, base;
    quint32 real_CAN_Fe;

/*  if (m_stateCan != CAN_STARTED) {
        fFe = m_confCan.frequency;
        return -1;
    }

    ret = ioctl(m_canFd, CF8CH_GET_CAN_FE_DIVISOR, &u32);
    if (ret < 0) {
        perror(NULL);
        return ret;
    }
    ret = ioctl(m_canFd, CF8CH_GET_PLL_CAN_CLOCK, &base);
    if (ret < 0) {
        perror(NULL);
        return -1;
    }

    fFe = ( base / (512.0 * (u32 + 2))) ; //link with the FPGA
    */

    ret = ioctl(m_canFd, CF8CH_GET_CAN_FE, &real_CAN_Fe); // JGI - A corriger pour obtenir "capture_periodCan" du FPGA => CF8CH_GET_FE à modifier
    if (ret < 0) {
        perror(NULL);
        return ret;
    }

    fFe = (float) real_CAN_Fe;

    return ret;
}

/**
 *	\fn		float ICf8ch::GetFpgaVersion()
 *
 *	\brief	get the version of FPGA
 *
 * \return version of FPGA
 *
 */
float ICfg8ch::GetFpgaVersion()
{
    quint32 u32;
    if(m_statePps != PPS_STARTED){
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ERROR : need to int Init() before"<< Qt::endl;
        return -1;
    }
    float version;
    ioctl(m_ppsFd, CF8CH_GET_FPGA_VERSION, &u32);
    version = (u32 & 0x00FF);
    version /= 100;
    version += u32 >> 8;
    return version;
}

/**
 *	\fn		int ICf8ch::SetFeCanToFPGA()
 *
 *	\brief	set the CAN frequency to the FPGA and check it
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::SetFeCanToFPGA()
{
    int ret = 0;
//    quint32 pplCan, divisorCan, divisorCanReturn;
    bool feDone = false;
    int nbTry = 1;

    quint32 fprocess_fpga, fprocess_fpga_theoric, fprocess_fpga_real, fcan_clk;
	quint32 fclk_tp2, fclk_tp2_feedback;
	quint16 mult_FPGA_TP2, nb_cycle_TP2, ka, kb;
	quint16 a, a_feedback;
	quint16 b, b_feedback;
	quint16 nb_cycle_can_clk_A_hi, nb_cycle_can_clk_A_hi_feedback;
	quint16 nb_cycle_can_clk_A_low, nb_cycle_can_clk_A_low_feedback;
	quint16 nb_cycle_can_clk_B_hi, nb_cycle_can_clk_B_hi_feedback;
	quint16 nb_cycle_can_clk_B_low, nb_cycle_can_clk_B_low_feedback;
    float fRealFE;


 /*   ret = ioctl(m_canFd,CF8CH_GET_PLL_CAN_CLOCK,&pplCan);
    if(ret < 0){
        return ret;
    }
    divisorCan = ((pplCan/512.0)/m_confCan.frequency);
    if(divisorCan > 2){
        divisorCan -= 2;
    }else{
        divisorCan = 0;
    }
    while(!feDone && nbTry < 5){
        ret = ioctl(m_canFd, CF8CH_SET_CAN_FE_DIVISOR, &divisorCan);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }

        ret = ioctl(m_canFd, CF8CH_GET_CAN_FE_DIVISOR, &divisorCanReturn);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }
        if(divisorCanReturn == divisorCan){
            feDone = true;
        }else{
            IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : essay nb "<< nbTry << " divisor = " << divisorCan << " != "<< divisorCanReturn << Qt::endl;
        }
        nbTry++;
    }
    if(!feDone){
        IStdout() << ICfg8ch_NAME << __FUNCTION__ << " : ERROR : ecriture sur FPGA impossible" << Qt::endl;
        return -1;
    }
    return ret;*/

	if (m_confCan.mode == CAN_MODE_HIGH_SPEED)
	{
        // Fclk set from FPGA to CAN ADS1278 for conversion Analogic to Numeric HIGH_SPEED mode (clk_div = '1')
		mult_FPGA_TP2 = MULT_CAN_HIGH_SPEED;
		fclk_tp2 = m_confCan.frequency * MULT_FE_CAN_HIGH_SPEED / mult_FPGA_TP2;
		fcan_clk = m_confCan.frequency * MULT_FE_CAN_HIGH_SPEED;
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Mode d'acquisition du CAN : MODE_HIGH_SPEED"<< Qt::endl;
	}
	else if (m_confCan.mode == CAN_MODE_HIGH_RESOLUTION)
	{
		// Fclk set from FPGA to CAN ADS1278 for conversion Analogic to Numeric in HIGH_RESOLUTION mode (clk_div = '1')
		mult_FPGA_TP2 = MULT_CAN_HIGH_RESOLUTION;
		fclk_tp2 = m_confCan.frequency * MULT_FE_CAN_HIGH_RESOLUTION / mult_FPGA_TP2;
		fcan_clk = m_confCan.frequency * MULT_FE_CAN_HIGH_RESOLUTION;
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Mode d'acquisition du CAN : MODE_HIGH_RESOLUTION"<< Qt::endl;
	}
	else if (m_confCan.mode == CAN_MODE_LOW_POWER)
	{
        // Fclk set from FPGA to CAN ADS1278 for conversion Analogic to Numeric LOW_POWER mode (clk_div = '1')
		mult_FPGA_TP2 = MULT_CAN_LOW_POWER;
		fclk_tp2 = m_confCan.frequency * MULT_FE_CAN_LOW_POWER_1 / mult_FPGA_TP2;
		fcan_clk = m_confCan.frequency * MULT_FE_CAN_LOW_POWER_1;
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Mode d'acquisition du CAN : MODE_LOW_POWER"<< Qt::endl;
	}
	else
	{
		// Fclk set from FPGA to CAN ADS1278 for conversion Analogic to Numeric LOW_SPEED mode (clk_div = '1')
		mult_FPGA_TP2 = MULT_CAN_LOW_SPEED;
		fclk_tp2 = m_confCan.frequency * MULT_FE_CAN_LOW_SPEED_1 / mult_FPGA_TP2;
		fcan_clk = m_confCan.frequency * MULT_FE_CAN_LOW_SPEED_1;
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Mode d'acquisition du CAN : MODE_LOW_SPEED"<< Qt::endl;
	}

	// Paramétrage de la fréquence de la clk fournit par le Timepulse2 du NEO-M8T nécessaire au CAN pour échantillonner à la fréquence d'échantillonnage souhaitée
    IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Configuration du Timepulse2 du NEO-M8T a "<< fclk_tp2 <<"Hz"<< Qt::endl;
	
	// Définition de fréquence de la clk fournit au CAN pour échantillonner à la fréquence d'échantillonnage souhaitée
    IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Configuration du signal can_clk a "<< fcan_clk <<"Hz"<< Qt::endl;


    // Récupération de la fréquence du process FPGA théorique
    ret = ioctl(m_canFd, CF8CH_GET_PLL_CAN_CLOCK, &fprocess_fpga_theoric);
    if (ret < 0)
    {
        //perror(NULL);
        perror("getFrequencyProcessFPGA error\n");
        return ret;
    }

    // Récupération de la fréquence du process FPGA reélle
    ret = ioctl(m_canFd, CF8CH_GET_CAN_FREQUENCY_PROCESS, &fprocess_fpga_real);
    if (ret < 0)
    {
        //perror(NULL);
        perror("getFrequencyProcessFPGA error\n");
        return ret;
    }

    fprocess_fpga = fprocess_fpga_theoric;
    IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Pour information, la valeur de la frequence process du FPGA utilisee dans les calculs des parametres d'acquisition est la valeur theorique et non reelle"<< Qt::endl;

//    fprocess_fpga = fprocess_fpga_real;


    // Calcul des paramètres nécessaire au FPGA pour générer l'horloge transmise au CAN 'can_clk" à partir du signal TP2
    if ( (fprocess_fpga % fclk_tp2) == 0 )
    {
        nb_cycle_TP2 = fprocess_fpga / fclk_tp2;
    }
    else
    {
        nb_cycle_TP2 = fprocess_fpga / fclk_tp2 + 1;
    }


    ka = fprocess_fpga / fcan_clk;	
    if ( (fprocess_fpga % fcan_clk) == 0 )
    {
        kb = ka;
        b = nb_cycle_TP2 / (2 * ka);
        a = b;
    }
    else
    {
        kb = ka + 1;
        b = (nb_cycle_TP2 - mult_FPGA_TP2 * ka) / (kb - ka);
        a = mult_FPGA_TP2 - b;
    }

    
    nb_cycle_can_clk_A_hi = ka * DUTY_CYCLE_CAN_CLK / 100;
    if (ka * (100 - DUTY_CYCLE_CAN_CLK) % 100 == 0)
    {
        nb_cycle_can_clk_A_low = ka * (100 - DUTY_CYCLE_CAN_CLK) / 100;
    }
    else
    {
        nb_cycle_can_clk_A_low = ka * (100 - DUTY_CYCLE_CAN_CLK) / 100 + 1;
    }


    nb_cycle_can_clk_B_hi = kb * DUTY_CYCLE_CAN_CLK / 100;
    if (kb * (100 - DUTY_CYCLE_CAN_CLK) % 100 == 0)
    {
        nb_cycle_can_clk_B_low = kb * (100 - DUTY_CYCLE_CAN_CLK) / 100;
    }
    else
    {
        nb_cycle_can_clk_B_low = kb * (100 - DUTY_CYCLE_CAN_CLK) / 100 + 1;
    }

    // Vérifications des paramètres pour générer l'horloge transmise au CAN 'can_clk" à partir du signal TP2
    IStdout()<<""<< Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Voici les parametres de configuration de l'horloge du CAN :"<< Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Duty_cycle_can_clk = "<< DUTY_CYCLE_CAN_CLK <<"%"<< Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Frequence echantillonnage du CAN ADS1278 = "<< m_confCan.frequency <<"Hz"<< Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Frequence horloge GPS TP2 = "<< fclk_tp2 <<"Hz"<< Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Frequence horloge process CAN FPGA = "<< fprocess_fpga <<"Hz"<< Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Frequence horloge process CAN FPGA theorique = "<< fprocess_fpga_theoric <<"Hz"<< Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Frequence horloge process CAN FPGA reelle = "<< fprocess_fpga_real <<"Hz"<< Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : nb_cycle_TP2 = "<< nb_cycle_TP2 << Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : mult_FPGA_TP2 = "<< mult_FPGA_TP2 << Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : KA = "<< ka << Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : KB = "<< kb << Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : A = "<< a << Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : B = "<< b << Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : nb_cycle_can_clk_A_hi = "<< nb_cycle_can_clk_A_hi << Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : nb_cycle_can_clk_A_low = "<< nb_cycle_can_clk_A_low << Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : nb_cycle_can_clk_B_hi = "<< nb_cycle_can_clk_B_hi << Qt::endl;
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : nb_cycle_can_clk_B_low = "<< nb_cycle_can_clk_B_low << Qt::endl;
    IStdout() <<""<< Qt::endl;
    
    // Transfert des paramètres pour la configuration de l'horloge pour le CAN au FPGA :
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Transfert des parametres pour l'horloge du CAN ADS1278 (can_clk) au FPGA"<< Qt::endl;


    // Can clk parameter "A"
    ret = ioctl(m_canFd, CF8CH_SET_CAN_CLK_PARAM_A, &a);
    if (ret < 0)
    {
        perror("set can parameter \"A\" error\n");
        return ret;
    }
    ret = ioctl(m_canFd, CF8CH_GET_CAN_CLK_PARAM_A, &a_feedback);
    if (ret < 0)
    {
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : get can parameter \"A\" is not well executed or not executed, error code : "<< ret << Qt::endl;
        perror("get can parameter \"A\" error\n");
        return ret;
    }
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : set A = "<< a <<", get A = "<< a_feedback << Qt::endl;


    // Can clk parameter "B"
    ret = ioctl(m_canFd, CF8CH_SET_CAN_CLK_PARAM_B, &b);
    if (ret < 0)
    {
        perror("set can parameter \"B\" error\n");
        return ret;
    }
    ret = ioctl(m_canFd, CF8CH_GET_CAN_CLK_PARAM_B, &b_feedback);
    if (ret < 0)
    {
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : get can parameter \"B\" is not well executed or not executed, error code : "<< ret << Qt::endl;
        perror("get can parameter \"B\" error\n");
        return ret;
    }
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : set B = "<< b <<", get B = "<< b_feedback << Qt::endl;


    // Can clk parameter "nb_cycle_can_clk_A_hi"
    ret = ioctl(m_canFd, CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_A_HI, &nb_cycle_can_clk_A_hi);
    if (ret < 0)
    {
        perror("set can parameter \"nb_cycle_can_clk_A_hi\" error\n");
        return ret;
    }
    ret = ioctl(m_canFd, CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_A_HI, &nb_cycle_can_clk_A_hi_feedback);
    if (ret < 0)
    {
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : get can parameter \"nb_cycle_can_clk_A_hi\" is not well executed or not executed, error code : "<< ret << Qt::endl;
        perror("get can parameter \"nb_cycle_can_clk_A_hi\"  error\n");
        return ret;
    }
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : set nb_cycle_can_clk_A_hi = "<< nb_cycle_can_clk_A_hi <<", get nb_cycle_can_clk_A_hi = "<< nb_cycle_can_clk_A_hi_feedback << Qt::endl;


    // Can clk parameter "nb_cycle_can_clk_A_low"
    ret = ioctl(m_canFd, CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_A_LOW, &nb_cycle_can_clk_A_low);
    if (ret < 0)
    {
        perror("set can parameter \"nb_cycle_can_clk_A_low\" error\n");
        return ret;
    }
    ret = ioctl(m_canFd, CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_A_LOW, &nb_cycle_can_clk_A_low_feedback);
    if (ret < 0)
    {
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : get can parameter \"nb_cycle_can_clk_A_low\" is not well executed or not executed, error code : "<< ret << Qt::endl;
        perror("get can parameter \"nb_cycle_can_clk_A_low\"  error\n");
        return ret;
    }
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : set nb_cycle_can_clk_A_low = "<< nb_cycle_can_clk_A_low <<", get nb_cycle_can_clk_A_low = "<< nb_cycle_can_clk_A_low_feedback << Qt::endl;


    // Can clk parameter "nb_cycle_can_clk_B_hi"
    ret = ioctl(m_canFd, CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_B_HI, &nb_cycle_can_clk_B_hi);
    if (ret < 0)
    {
        perror("set can parameter \"nb_cycle_can_clk_B_hi\" error\n");
        return ret;
    }
    ret = ioctl(m_canFd, CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_B_HI, &nb_cycle_can_clk_B_hi_feedback);
    if (ret < 0)
    {
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : get can parameter \"nb_cycle_can_clk_B_hi\" is not well executed or not executed, error code : "<< ret << Qt::endl;
        perror("get can parameter \"nb_cycle_can_clk_B_hi\"  error\n");
        return ret;
    }
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : set nb_cycle_can_clk_B_hi = "<< nb_cycle_can_clk_B_hi <<", get nb_cycle_can_clk_B_hi = "<< nb_cycle_can_clk_B_hi_feedback << Qt::endl;


    // Can clk parameter "nb_cycle_can_clk_B_low"
    ret = ioctl(m_canFd, CF8CH_SET_CAN_CLK_PARAM_NB_CYCLE_B_LOW, &nb_cycle_can_clk_B_low);
    if (ret < 0)
    {
        perror("set can parameter \"nb_cycle_can_clk_B_low\" error\n");
        return ret;
    }
    ret = ioctl(m_canFd, CF8CH_GET_CAN_CLK_PARAM_NB_CYCLE_B_LOW, &nb_cycle_can_clk_B_low_feedback);
    if (ret < 0)
    {
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : get can parameter \"nb_cycle_can_clk_B_low\" is not well executed or not executed, error code : "<< ret << Qt::endl;
        perror("get can parameter \"nb_cycle_can_clk_B_low\"  error\n");
        return ret;
    }
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : set nb_cycle_can_clk_B_low = "<< nb_cycle_can_clk_B_low <<", get nb_cycle_can_clk_B_low = "<< nb_cycle_can_clk_B_low_feedback << Qt::endl;


    if (a != a_feedback ||
        b != b_feedback ||
        nb_cycle_can_clk_A_hi != nb_cycle_can_clk_A_hi_feedback ||
        nb_cycle_can_clk_A_low != nb_cycle_can_clk_A_low_feedback ||
        nb_cycle_can_clk_B_hi != nb_cycle_can_clk_B_hi_feedback ||
        nb_cycle_can_clk_B_low != nb_cycle_can_clk_B_low_feedback)
    {
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : set or get can parameter can_clk is not well executed"<< Qt::endl;
        return -1;
    }
    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : set and get can parameter can_clk is well executed"<< Qt::endl;

    

	while(!feDone && nbTry < NB_TRY_MAX_FCAN_CLK+1)
	{
//		string cmd = "pegase-confpps.py " + to_string(TIMEPULSE2) + " -f " + to_string(fclk_tp2) + " -d " + to_string(DELAYTIMEPULSE2);
//	    IStdout()<< ICfg8ch_NAME << __FUNCTION__ << cmd << Qt::endl;
//      IStdout()<< ICfg8ch_NAME << __FUNCTION__ << QString::fromStdString(cmd) << Qt::endl;

//        ret = system(cmd.c_str());
//        if (ret != 0)
//        {
//            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<"Erreur execution script python : pegase-confpps.py"<< Qt::endl;
//        }

        QString commandLine = "pegase-confpps";
        QProcess *myProcess = new QProcess();
        QStringList arguments;
        arguments << QString::number(TIMEPULSE2) << "-f" << QString::number(fclk_tp2) << "-d" <<QString::number(DELAYTIMEPULSE2);
        myProcess->start(commandLine,arguments);
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<  " : " << commandLine << Qt::endl;
        myProcess->waitForFinished(1000);

        ret = myProcess->exitCode();
        if (ret < 0)
		{
			//perror(NULL);
			perror("setTimePulse2 error\n");
			return ret;
		}


		// Transmission de la fréquence de TP2 au FPGA
		ret = ioctl(m_canFd, CF8CH_SET_FREQUENCE_TP2, &fclk_tp2);
		if (ret < 0)
		{
			//perror(NULL);
			perror("setFrequencyTP2 error\n");
			return ret;
		}

		ret = ioctl(m_canFd, CF8CH_GET_FREQUENCE_TP2, &fclk_tp2_feedback);
		if (ret < 0)
		{
			//perror(NULL);
			perror("getFrequencyTP2 error\n");
			return ret;
		}

        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : fclk_tp2 = " << fclk_tp2 << "Hz, fclk_tp2_feedback = "<< fclk_tp2_feedback <<"Hz"<< Qt::endl;

		if (fclk_tp2 != fclk_tp2_feedback)
		{
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : setFrequencyTP2 or getFrequencyTP2 is not well executed or not executed"<< Qt::endl;
			return -1;
		}

        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : setFrequencyTP2 and getFrequencyTP2 is well executed"<< Qt::endl;

        sleep(5);

        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : m_confCan.frequency = "<< m_confCan.frequency <<"Hz"<< Qt::endl;
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : fcan_clk = "<< fcan_clk <<"Hz"<< Qt::endl;

        ret = GetRealFE(fRealFE);
        if (ret != 0)
        {
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Mesure de la frequence d'echantillonnage reelle NOK !, ret = "<< ret << Qt::endl;
        }

//        IStdout()<< setprecision(3) << fixed;
        IStdout()<< qSetRealNumberPrecision(3) << Qt::fixed;

        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : Essai nb "<< nbTry <<", m_confCan.frequency = "<< m_confCan.frequency <<"Hz & fRealFE = "<< fRealFE <<"Hz"<< Qt::endl;

        if (fRealFE >= m_confCan.frequency*(1-TOL_ERROR_TIMEPULSE2_GPS_NON_VALID/100) && fRealFE <= m_confCan.frequency*(1+TOL_ERROR_TIMEPULSE2_GPS_NON_VALID/100))
        {
			feDone = true;

            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : fRealFE valide : "<< m_confCan.frequency*(1-TOL_ERROR_TIMEPULSE2_GPS_NON_VALID/100) <<"Hz <= fRealFE <= "<< m_confCan.frequency*(1+TOL_ERROR_TIMEPULSE2_GPS_NON_VALID/100) <<"Hz"<< Qt::endl;
		}
        else
        {
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : fRealFE non valide : fRealFE < "<< m_confCan.frequency*(1-TOL_ERROR_TIMEPULSE2_GPS_NON_VALID/100) <<"Hz"<<" ou fRealFE > "<< m_confCan.frequency*(1+TOL_ERROR_TIMEPULSE2_GPS_NON_VALID/100) <<"Hz"<< Qt::endl;
		}
		nbTry++;
	}
	if(!feDone)
	{
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : error SetFeToFPGA : ecriture sur FPGA impossible ou m_confCan.frequency != fRealFE"<< Qt::endl;
		return -1;
	}
    
    return ret;
}

/**
 *	\fn		int ICf8ch::SetModeCanToFPGA()
 *
 *	\brief	set the CAN mode to the FPGA and check it
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::SetModeCanToFPGA()
{
	int ret = 0;
	uint16_t canMode_feedback;
	
	ret = ioctl(m_canFd, CF8CH_SET_CAN_MODE, &m_confCan.mode);
    if (ret < 0)
	{
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : setModeCanToFPGA is not well executed, error code : "<< ret << Qt::endl;
		//perror(NULL);
		perror("SetModeCanToFPGA error\n");
    	return ret;
	}

	ret = ioctl(m_canFd, CF8CH_GET_CAN_MODE, &canMode_feedback);
    if (ret < 0)
	{
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : getModeCanToFPGA is not well executed or not executed, error code : "<< ret << Qt::endl;
    	//perror(NULL);
		perror("getModeCanToFPGA error\n");
    	return ret;
	}

	IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : m_confCan.mode = ";
	switch (m_confCan.mode)
	{
		case CAN_MODE_HIGH_SPEED :
		IStdout()<<"CAN_MODE_HIGH_SPEED";
		break;

		case CAN_MODE_HIGH_RESOLUTION :
		IStdout()<<"CAN_MODE_HIGH_RESOLUTION";
		break;

		case CAN_MODE_LOW_POWER :
		IStdout()<<"CAN_MODE_LOW_POWER";
		break;

		default : 		// case m_confCan.mode == CAN_MODE_LOW_SPEED
		IStdout()<<"CAN_MODE_LOW_SPEED";
		break;
	}

	IStdout()<<", canMode_feedback = ";
	switch (canMode_feedback)
	{
		case CAN_MODE_HIGH_SPEED :
        IStdout()<<"CAN_MODE_HIGH_SPEED"<< Qt::endl;
		break;

		case CAN_MODE_HIGH_RESOLUTION :
        IStdout()<<"CAN_MODE_HIGH_RESOLUTION"<< Qt::endl;
		break;

		case CAN_MODE_LOW_POWER :
        IStdout()<<"CAN_MODE_LOW_POWER"<< Qt::endl;
		break;

		default : 		// case m_confCan.mode == CAN_MODE_LOW_SPEED
        IStdout()<<"CAN_MODE_LOW_SPEED"<< Qt::endl;
		break;
	}
	
	if (m_confCan.mode != canMode_feedback)
	{
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : setModeCanToFPGA or getModeCanToFPGA is not well executed or not executed"<< Qt::endl;
		return -1;
	}

    IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : setModeCanToFPGA and getModeCanToFPGA is well executed"<< Qt::endl;
	
	return ret;
}

/**
 *	\fn		int ICf8ch::SetCountCanToFPGA()
 *
 *	\brief	set the CAN count to the FPGA and check it
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::SetCountCanToFPGA()
{
    int ret = 0;
    quint32 countReturn;
    bool countDone = false;
    int nbTry = 0;
    while(!countDone && nbTry < 5){
        ret = ioctl(m_canFd, CF8CH_SET_NB_DATA_DMA, &m_confCan.count);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }

        ret = ioctl(m_canFd, CF8CH_GET_NB_DATA_DMA, &countReturn);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }
        if(countReturn == m_confCan.count){
            countDone = true;
        }else{
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : essay nb "<< nbTry << " count = " << countReturn << " != "<< m_confCan.count << Qt::endl;
        }
        nbTry++;
    }
    if(!countDone){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : ERROR : ecriture sur FPGA impossible" << Qt::endl;
        return -1;
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::SetChannelCanToFPGA()
 *
 *	\brief	set the CAN channel to the FPGA and check it
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::SetChannelCanToFPGA()
{
    int ret = 0;
    quint16 channelCanReturn = 0;

    bool channelDone = false;
    int nbTry = 0;
    while(!channelDone && nbTry < 5){
        ret = ioctl(m_canFd, CF8CH_SET_CAN_CHANNELS, &m_confCan.channel);
        if (ret < 0) {
            perror(NULL);
            return -5;
        }
        ret = ioctl(m_canFd, CF8CH_GET_CAN_CHANNELS, &channelCanReturn);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }
        channelCanReturn = channelCanReturn >> 8;
        if(channelCanReturn == m_confCan.channel){
            channelDone = true;
        }else{
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : essay nb "<< nbTry << " channelCan = " << channelCanReturn << " != "<< m_confCan.channel  << Qt::endl;
        }
        nbTry++;
    }
    if(!channelDone){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : ERROR : ecriture sur FPGA impossible" << Qt::endl;
        return -1;
    }
    return ret;
}

/**
 *	\fn		void ICf8ch::ReadCan()
 *
 *	\brief	read data CAN on FPGA, date them and emit signal Data_Can
 *
 */
void ICfg8ch::ReadCan(uint32_t *dataList,int count, int nbChannel,quint64 seconde, quint32 compteur,IEvent *event)
{
    (void)(count);
    (void)(nbChannel);
    static Date_S_NS_t dateLastData;
    double realPeriodAcquisition;
    quint64 tempEntreData;
    double tempEntreDataPrecision;
    quint64 timeDataNanoSeconde;
    static quint64 timeDataNanoSecondeFirst = 0, timeDataNanoSecondelast = 0;
    dateData datedataCalc;
    QVector<IAnalogData> listData;

    timeDataNanoSecondeFirst = timeDataNanoSecondelast;
        if(m_confCan.withGPS){

            datedataCalc = calcDateData(seconde,compteur);
            dateLastData.ts = datedataCalc.seconde;
            dateLastData.nanoS = datedataCalc.nanoSeconde;
        }else{
            static struct timeval timeNow;
            gettimeofday(&timeNow, NULL);
            dateLastData.ts = timeNow.tv_sec;
            dateLastData.nanoS = timeNow.tv_usec*1000;
        }
        float cptPrecision = 0;
        float freqInst = m_confCan.frequency;
        GetRealFE(freqInst);
        int delta_us = 39*1000000/freqInst;//Delay du can
        timeDataNanoSecondelast =dateLastData.ts * 1000000000 + dateLastData.nanoS - (delta_us * 100);

        if(timeDataNanoSecondeFirst != 0){
            realPeriodAcquisition = timeDataNanoSecondelast - timeDataNanoSecondeFirst;
            tempEntreData = realPeriodAcquisition /(m_confCan.count);
            tempEntreDataPrecision = fmod(realPeriodAcquisition /(m_confCan.count), 1);
            timeDataNanoSeconde = timeDataNanoSecondeFirst;
            for(quint32 data = 0; data < m_confCan.count; data++){
                cptPrecision += tempEntreDataPrecision;
                if (cptPrecision > 1) {
                    ++timeDataNanoSeconde;
                    --cptPrecision;
                }
                timeDataNanoSeconde += tempEntreData;
                IAnalogData newData;
                newData.m_Default = datedataCalc.valeurReel;
                newData.m_s = timeDataNanoSeconde / 1000000000;
                newData.m_ns = timeDataNanoSeconde % 1000000000;
                for(int channel = 0; channel < m_nbCanChannel; channel ++){
                    newData.m_data.push_back((int)dataList[data * m_nbCanChannel + channel]);
                }
                listData.push_back(newData);
            }
            emit Data_Can(listData,event);
       }
}

/**
 *	\fn		int ICf8ch::SetTimeOutTrigToFPGA()
 *
 *	\brief	set the TRIG timeout to the FPGA and check it
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::SetTimeOutTrigToFPGA()
{
    int ret = 0;
    quint16 timeoutTrigReturn;
    bool timeoutTriDone = false;
    int nbTry = 0;
    while(!timeoutTriDone && nbTry < 5){
        ret = ioctl(m_trigFd, CF8CH_SET_TIMEOUT_TRIG, &m_confTrig.timeout);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }
        ret = ioctl(m_trigFd, CF8CH_GET_TIMEOUT_TRIG, &timeoutTrigReturn);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }
        if(timeoutTrigReturn == m_confTrig.timeout){
            timeoutTriDone = true;
        }else{
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : essay nb "<< nbTry << " timeoutTrig = " << timeoutTrigReturn << " != "<< m_confTrig.timeout  <<Qt::endl;
        }
        nbTry++;
    }
    if(!timeoutTriDone){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : ERROR  : ecriture sur FPGA impossible" << Qt::endl;
        return -1;
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::SetChannelTrigToFPGA()
 *
 *	\brief	set the TRIG chanel to the FPGA and check it
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::SetChannelTrigToFPGA()
{
    int ret = 0;
    quint16 channelTrigReturn;
    bool channelTrigDone = false;
    int nbTry = 0;
    while(!channelTrigDone && nbTry < 5){
        ret = ioctl(m_trigFd, CF8CH_SET_CHANELS_TRIG, &m_confTrig.channel);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }

        ret = ioctl(m_trigFd, CF8CH_GET_CHANELS_TRIG, &channelTrigReturn);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }
        channelTrigReturn = channelTrigReturn >> 8;
        if(channelTrigReturn == m_confTrig.channel){
            channelTrigDone = true;
        }else{
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : essay nb "<< nbTry << " channelTrig = " << channelTrigReturn << " != "<< m_confTrig.channel <<Qt::endl;
        }
        nbTry++;
    }
    if(!channelTrigDone){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << " : ERROR : ecriture sur FPGA impossible" << Qt::endl;
        return -1;
    }
    return ret;
}

/**
 *	\fn		int ICf8ch::SetNbTrigOffToFPGA()
 *
 *	\brief	set the nb trig to disable channel to the FPGA and check it
 *
 * \return A negative number if an error occured, 0 otherwise
 *
 */
int ICfg8ch::SetNbTrigOffToFPGA()
{
    int ret = 0;
    quint16 trigMaxReturn;
    bool trigMaxDone = false;
    int nbTry = 0;
    while(!trigMaxDone && nbTry < 5){
        ret = ioctl(m_trigFd, CF8CH_SET_NB_TRIG_OFF, &m_confTrig.nbTrigSecurity);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }

        ret = ioctl(m_trigFd, CF8CH_GET_NB_TRIG_OFF, &trigMaxReturn);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }
        if(trigMaxReturn == m_confTrig.nbTrigSecurity){
            trigMaxDone = true;
        }else{
            IStdout()<< ICfg8ch_NAME << __FUNCTION__ <<" : essay nb "<< nbTry << " trigMax = " << trigMaxReturn << " != "<< m_confTrig.nbTrigSecurity  <<Qt::endl;
        }
        nbTry++;
    }
    if(!trigMaxDone){
        IStdout()<< ICfg8ch_NAME << __FUNCTION__ << "ERROR : ecriture sur FPGA impossible" << Qt::endl;
        return -1;
    }
    return 0;
    return ret;
}

/**
 *	\fn		void ICf8ch::ReadTrig()
 *
 *	\brief	read data TRIG on FPGA, date them and emit signal Data_Trig or
 *
 */
void ICfg8ch::ReadTrig(ReadTrigChanel_t *data, int nbdata, IEvent *event)
{
    QVector<IGnumData> listData;
    dateData datedataCalc;
    for (int i = 0; i < nbdata; i++) {
        IGnumData INumDataTemp;
        datedataCalc = calcDateData(data[i].ts,data[i].nanoS);
        INumDataTemp.m_Default = datedataCalc.valeurReel;
        INumDataTemp.m_nbPasEvent = data[i].nanoS;
        INumDataTemp.m_ns = datedataCalc.nanoSeconde;
        INumDataTemp.m_s = datedataCalc.seconde;
        INumDataTemp.m_pps = datedataCalc.pps;
        INumDataTemp.m_data = data[i].trig;
        listData.push_back(INumDataTemp);
    }
    emit Data_Trig(listData,event);
}

void ICfg8ch::Error_trig_off_slot(int statusTrigOff, IEvent *event)
{
    emit Error_trig_off(statusTrigOff,event);
}

void ICfg8ch::Error_overwrite_dma_slot(int nbOverwrite, IEvent *event)
{
    emit Error_overwrite(nbOverwrite,event);
}


