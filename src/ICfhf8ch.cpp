#include "ICfhf8ch.h"

ICfhf8ch::ICfhf8ch(QObject *parent) : QObject(parent)
{
    qRegisterMetaType<Signal_t>("Signal_t");
    qRegisterMetaType<Signal_t>("ppsInfos_t");
}

ICfhf8ch::~ICfhf8ch()
{
    delete(m_readCan);
}

int ICfhf8ch::init(bool databrut){

    m_databrut=databrut;
    if(!modprobeDriver()){
        return -1;
    }

    QByteArray driverName("/dev/");
    driverName.append(DRV_NAME_HF8CH);
    m_fdCf8ch = open(driverName.toStdString().c_str(), O_RDWR);
    m_state = OPENED;

    if(OpenPps() < 0){
        return -1;
    }
    if(StartPps() < 0){
        return -1;
    }
    return m_fdCf8ch;
}

int ICfhf8ch::modprobeDriver(){
    QString commandLine = "modprobe";
    QStringList arguments;
    arguments << "ifsttar-hf8ch";
    QProcess *myProcess = new QProcess();
    myProcess->start(commandLine,arguments);
    sleep(2);
    QStringList listDriver;
    QString pathDir = "/sys/module/";
    QDir driverDir(pathDir);
    if(!driverDir.exists()){
        qDebug() << "Can open /sys/module/ dir" << Qt::endl;
        return false;
    }
    listDriver = driverDir.entryList(QDir::Dirs | QDir::NoDotAndDotDot);
    QString driver = "ifsttar_hf8ch";
    if(!listDriver.contains(driver)){
        qDebug() <<"the driver is not here" <<Qt::endl;
        qDebug() << listDriver << " not contain "<<  driver<<Qt::endl;
        return false;
    }
    return true;
}

int ICfhf8ch::OpenPps(){
    int ret = 0;
    QByteArray driverName("/dev/");
    driverName.append(DRV_NAME_PPS);
    m_fdHfpps = open(driverName.toStdString().c_str(), O_RDWR);
    if (m_fdHfpps == -1)
    {
        std::cout << __FUNCTION__ << " : ERROR : open /dev/"<< DRV_NAME_PPS << std::endl;
        ret = -1;
    }else{
        std::cout << __FUNCTION__ << " : done" << std::endl;

    }

    return ret;
}

int ICfhf8ch::StartPps()
{
    int ret = 0;
    m_readHfpps = new IReadhfpps(m_fdHfpps);
    connect(m_readHfpps, SIGNAL(dataAvaible(ppsInfos_t, IEvent*)), this, SLOT(ReadPps(ppsInfos_t,IEvent*)), Qt::DirectConnection);
    m_readHfpps->start();
    std::cout << __FUNCTION__ << " : done" << std::endl;
    return ret;
}

int ICfhf8ch::start()
{
    int ret = 0;
    if(m_state == OPENED){

        m_state = STARTED;
        m_readCan = new IReadhfcan(m_fdCf8ch,MAX_DATA_READ_KERNEL);
        connect(m_readCan, SIGNAL(dataAvaible(uint16_t*,int,int,bool,ConfhfCan_t,IEvent*)), this, SLOT(analyseRx(uint16_t*,int,int,bool,ConfhfCan_t,IEvent*)),Qt::DirectConnection);
        m_readCan->start();
    }else if(m_state == STOPED){
        m_state = STARTED;
        connect(m_readCan, SIGNAL(dataAvaible(uint16_t*,int,int,bool,ConfhfCan_t,IEvent*)), this, SLOT(analyseRx(uint16_t*,int,int,bool,ConfhfCan_t,IEvent*)),Qt::DirectConnection);
    }else if(m_state == STARTED){
        ret = -1;
    }else if(m_state == NOT_OPENED){
        ret = -2;
    }
    return ret;
}
int ICfhf8ch::stop()
{
    int ret = 0;
    if(m_state == STARTED){
        disconnect(m_readCan, SIGNAL(dataAvaible(uint16_t*,int,int,bool,ConfhfCan_t,IEvent*)), this, SLOT(analyseRx(uint16_t*,int,int,bool,ConfhfCan_t,IEvent*)));
        m_state = STOPED;
    }else{
        ret = -2;
    }
    return ret;
}

int ICfhf8ch::startTrigSoft(){
    int ret = 0;
    ret = sendAndVerifyConf(SET_CHANNEL_ACTIVATION_SEUIL,GET_CHANNEL_ACTIVATION_SEUIL,"channel seuil",0);
    if(ret<0){
        return ret;
    }
    ret = ioctl(m_fdCf8ch,TRIG_SOFT,NULL);
    return ret;
}

int ICfhf8ch::startTrigExterne(bool rising, int channel)
{
    int ret = 0;
    (void)(rising);
    (void)(channel);
    ret = sendAndVerifyConf(SET_CHANNEL_ACTIVATION_SEUIL,GET_CHANNEL_ACTIVATION_SEUIL,"channel seuil",0);
    if(ret<0){
        return ret;
    }
    ret = ioctl(m_fdCf8ch,TRIG_EXTERNE,NULL);
    return ret;
}

int ICfhf8ch::resetSoftFpga()
{
    int ret = 0;
    // ret = ioctl(m_fdCf8ch,RESET_FPGA,NULL);
    return ret;
}

int ICfhf8ch::startTrigDate(quint64 date)
{
    int ret = 0;
    ret = sendAndVerifyConf(SET_CHANNEL_ACTIVATION_SEUIL,GET_CHANNEL_ACTIVATION_SEUIL,"channel seuil",0);
    if(ret<0){
        return ret;
    }
    ret = ioctl(m_fdCf8ch,SET_DATE_DECLANCHEMENT,&date);
    ret = ioctl(m_fdCf8ch,TRIG_DATE,NULL);
    return ret;
}

int ICfhf8ch::setDigitDelay_dns(int delay)
{
    m_confTx_t.digitDelay = delay;
    return sendAndVerifyConf(SET_DIGIT_DELAY,GET_DIGIT_DELAY,"delay",delay);
}

int ICfhf8ch::setPrf_hz(int prf)
{
    if(prf ==0){
        return -1;
    }
    m_confTx_t.prfFreq = prf;
    return setPrf_dns(100000000/prf);
}

int ICfhf8ch::setPrf_dns(int prf)
{
    if(prf ==0){
        return -1;
    }
    m_confTx_t.prf = prf;
    m_confTx_t.prf_reel = prf;
    m_confTx_t.prfFreq = 100000000/prf;
    if(getVersionPps()){
        return saveDelayPrfPps();
    }
    return sendAndVerifyConf(SET_PRF,GET_PRF,"prf",m_confTx_t.prf_reel);
}
int ICfhf8ch::setNbAvg(int nbAv)
{
    m_confTx_t.nbAv = nbAv;
    return sendAndVerifyConf(SET_NBR_OF_AVG,GET_NBR_OF_AVG,"nbAv",nbAv);

}

int ICfhf8ch::setChannelTx(int channelTx)
{
    m_confTx_t.channel = channelTx;
    return sendAndVerifyConf(SET_CHANNEL_TX,GET_CHANNEL_TX,"channelTx",channelTx);
}

int ICfhf8ch::setNbRxSample(int nbSampleRx)
{
    m_confRx_t.nbSample = nbSampleRx;
    return sendAndVerifyConf(SET_NB_SAMPLE_RX,GET_NB_SAMPLE_RX,"nbSampleRx",nbSampleRx);
}

int ICfhf8ch::getNbRxSampleBytes()
{
    return 0;
}

int ICfhf8ch::setChannelRx(quint8 channel)
{
    m_confRx_t.channel = channel;
    return sendAndVerifyConf(SET_CHANNEL_RX,GET_CHANNEL_RX,"channelRx",channel);
}

int ICfhf8ch::setGainRx(int channel, quint8 gain)
{
    int ret = 0;
    switch (channel) {
    case 1 :
        m_confRx_t.gain[0] = gain;
        return sendAndVerifyConf(SET_GAIN_0,GET_GAIN_0,"gain 0",gain);
        break;
    case 2 :
        m_confRx_t.gain[1] = gain;
        return sendAndVerifyConf(SET_GAIN_1,GET_GAIN_1,"gain 1",gain);
        break;
    case 3 :
        m_confRx_t.gain[2] = gain;
        return sendAndVerifyConf(SET_GAIN_2,GET_GAIN_2,"gain 2",gain);
        break;
    case 4 :
        m_confRx_t.gain[3] = gain;
        return sendAndVerifyConf(SET_GAIN_3,GET_GAIN_3,"gain 3",gain);
        break;
    case 5 :
        m_confRx_t.gain[4] = gain;
        return sendAndVerifyConf(SET_GAIN_4,GET_GAIN_4,"gain 4",gain);
        break;
    case 6 :
        m_confRx_t.gain[5] = gain;
        return sendAndVerifyConf(SET_GAIN_5,GET_GAIN_5,"gain 5",gain);
        break;
    case 7 :
        m_confRx_t.gain[6] = gain;
        return sendAndVerifyConf(SET_GAIN_6,GET_GAIN_6,"gain 6",gain);
        break;
    case 8 :
        m_confRx_t.gain[7] = gain;
        return sendAndVerifyConf(SET_GAIN_7,GET_GAIN_7,"gain 7",gain);
        break;
    default:
        ret = -1;
        break;
    }
    return ret;
}

int ICfhf8ch::setGainRx(quint8 gain)
{
    int ret;
    for(int i =1; i < 9;i++){
        ret= setGainRx(i,gain);
        if(ret < 0){
            break;
        }
    }
    return ret;
}


int ICfhf8ch::setfreqRx(int freqRx)
{
    int ret;
    int temp;
    if(freqRx==0){
        return -1;
    }

    m_confRx_t.freq = FREQ_RX_MAX/freqRx;
    if(m_confRx_t.freq<1){
        m_confRx_t.freq = 1;
    }
    m_confRx_t.freq -=1;
    temp = m_confRx_t.freq;
    m_confRx_t.freq = FREQ_RX_MAX/(temp+1);
    ret = sendAndVerifyConf(SET_FREQ_RX,GET_FREQ_RX,"freqRx",temp);
    if (ret<0){
        return ret;
    }
    return m_confRx_t.freq;

}


quint64 ICfhf8ch::getDateFPGA()
{
    quint64 dateTemp;
    ioctl(m_fdCf8ch,GET_DATE_FPGA,&dateTemp);
    return dateTemp;
}

int ICfhf8ch::saveConfig()
{
    int ret;
    ret = ioctl(m_fdCf8ch,SAVE_CONFIG,NULL);
    return ret;
}

void ICfhf8ch::ReadPps(ppsInfos_t newPpsInfo, IEvent *event)
{

    (void)(event);
    m_ppsInfosMutex.lock();
    PpsInfoAll_t temp;
    temp.qerrBeginning = newPpsInfo.qerr;
    temp.time = newPpsInfo.time;
    m_ppsInfosQueue.enqueue(temp);
    m_ppsInfosQueue[m_ppsInfosQueue.size()-1].qerrEnd = newPpsInfo.qerr;
    m_ppsInfosQueue[m_ppsInfosQueue.size()-1].period = newPpsInfo.period;
    if(m_ppsInfosQueue.size() > NB_SECONDE_PPS_INFO){
        m_ppsInfosQueue.dequeue();
    }
    m_ppsInfosMutex.unlock();
    //printf("nouvelle period = %d\n",newPpsInfo.period);
    if(!getVersionPps()){

        if(newPpsInfo.period> 50000000 && newPpsInfo.period< 150000000){
            //PRF
            m_confTx_t.prf_reel = newPpsInfo.period / m_confTx_t.prf;
            //TODOD       ioctl(m_fdCf8ch,SET_PRF,&m_confTx_t.prf_reel);
            //CALC VALEUR COMPEUR
            quint64 secondeFs = 1000000000000000;
            quint64 fsByCtp = secondeFs / newPpsInfo.period;
            //DIGIT DELAY
            quint64 nbCtpDigitDelay = m_confTx_t.digitDelay;
            nbCtpDigitDelay = nbCtpDigitDelay * 10000000;//1000000 nanoS en fs
            nbCtpDigitDelay =  nbCtpDigitDelay / fsByCtp;
            int newDigitDelay = nbCtpDigitDelay;
            ioctl(m_fdCf8ch,SET_DIGIT_DELAY,&newDigitDelay);

            //AQC DELAIS
            quint64 nbCtpAcqDelay = m_confTx_t.acqDelay;
            nbCtpAcqDelay = nbCtpAcqDelay * 10000000;//1000000 nanoS en fs
            nbCtpAcqDelay =  nbCtpAcqDelay / fsByCtp;
            int newAcqDelay = nbCtpAcqDelay;
            ioctl(m_fdCf8ch,SET_DELAIS_ACQ,&newAcqDelay);
            // printf("prf = %d digitDelay =%d acqDely = %d\n",m_confTx_t.prf_reel,newDigitDelay,newAcqDelay);
        }
    }


}

int ICfhf8ch::resetHardFpga()
{
    int num = 1;
    num = write(m_fdCf8ch,&num,sizeof(num));
    usleep(500);
    return write(m_fdCf8ch,&num,sizeof(num));
}

int ICfhf8ch::setBT(bool on)
{
    if(on){
        return ioctl(m_fdCf8ch,ENABLE_BT,NULL);
    }else{
        return ioctl(m_fdCf8ch,DISABLE_BT,NULL);
    }
}

int ICfhf8ch::setHT(bool on)
{
    if(on){
        return ioctl(m_fdCf8ch,ENABLE_HT,NULL);
    }else{
        return ioctl(m_fdCf8ch,DISABLE_HT,NULL);
    }
}


int ICfhf8ch::setSignalTx(QList<quint16> signalTx, int channel)
{
    (void)(channel);
    quint16 *signal;
    int ret = 0;
    if(!signalTx.empty()){
        signal = (quint16 *)malloc(sizeof(quint16)*(signalTx.size()));
        for(int i = 0; i < signalTx.size();i++){
            signal[i] = signalTx.at(i);
        }
        ret = write(m_fdCf8ch,signal,signalTx.size());
    }
    return ret;
}

int ICfhf8ch::setLedBlink(int num)
{
    return sendAndVerifyConf(SET_LED_BLINK,GET_LED_BLINK,"blinkLed",num);
}

int ICfhf8ch::setAcqDelay_dns(int delay)
{
    m_confTx_t.acqDelay = delay;
    std::cout << "m_confTx_t.acqDelay = "<<m_confTx_t.acqDelay<<" delay = "<<delay<<std::endl;
    if(getVersionPps()){
        return saveDelayPrfPps();
    }
    return sendAndVerifyConf(SET_DELAIS_ACQ,GET_DELAIS_ACQ,"delay acq",delay); //pas de 10 ns
}

// 0 si pas de synchro
int ICfhf8ch::setSyncPps(int nbTire)
{
    m_confTx_t.syncPps = nbTire;
    if(getVersionPps()){
        int temp = 1;
        return sendAndVerifyConf(SET_SYNC_PPS,GET_SYNC_PPS,"syncpps",temp);
    }
    return sendAndVerifyConf(SET_SYNC_PPS,GET_SYNC_PPS,"syncpps",nbTire);
}

bool ICfhf8ch::getVersionPps()
{
    int versionPPS;
    ioctl(m_fdCf8ch,GET_VERSION_PPS,&versionPPS);
    if(versionPPS == 1){
        return true;
    }else{
        return false;
    }
}

int ICfhf8ch::startTrigSeuilDate(quint8 channel, quint64 date)
{
    int temp,ret;
    temp = channel;
    temp = (temp << 8) +1;
    ret = sendAndVerifyConf(SET_CHANNEL_ACTIVATION_SEUIL,GET_CHANNEL_ACTIVATION_SEUIL,"channel seuil",temp);
    if(ret<0){
        return ret;
    }
    ret = ioctl(m_fdCf8ch,SET_DATE_DECLANCHEMENT,&date);
    ret = ioctl(m_fdCf8ch,TRIG_DATE,NULL);
    return ret;
}

int ICfhf8ch::startTrigSeuilSoft(quint8 channel)
{
    int temp,ret;
    temp = channel;
    temp = (temp << 8) +1;
    ret = sendAndVerifyConf(SET_CHANNEL_ACTIVATION_SEUIL,GET_CHANNEL_ACTIVATION_SEUIL,"channel seuil",temp);
    if(ret<0){
        return ret;
    }
    ret = ioctl(m_fdCf8ch,TRIG_SOFT,NULL);

    return ret;
}

int ICfhf8ch::setSeuil(int seuilPos, int seuilNeg)
{
    int ret;
    ret = sendAndVerifyConf(SET_SEUIL_POS,GET_SEUIL_POS,"seuil pos",seuilPos);
    if(ret<0){
        return ret;
    }
    return sendAndVerifyConf(SET_SEUIL_NEG,GET_SEUIL_NEG,"seuil neg",seuilNeg);
}

int ICfhf8ch::setpreTrig(int preTrig)
{
    return sendAndVerifyConf(SET_PRE_TRIG,GET_PRE_TRIG,"nb data preTrig",preTrig);
}

int ICfhf8ch::saveDelayPrfPps()
{
    QProcess setPPSConf;
    QString program = "/usr/bin/pegase-confpps";
    QStringList arguments;
    int delayPps, delayFpga;
    std::cout << "m_confTx_t.acqDelay = "<<m_confTx_t.acqDelay<<" m_confTx_t.prf = "<<m_confTx_t.prf<<std::endl;
    delayFpga = (m_confTx_t.acqDelay / m_confTx_t.prf);
    if(m_confTx_t.acqDelay != 0){
        delayFpga++;
    }
    delayPps = -(m_confTx_t.acqDelay % m_confTx_t.prf);
    //programmation pps 2
    arguments << "2";
    arguments << "--freq" << QString::number(m_confTx_t.prfFreq);
    arguments << "--delay" << QString::number(delayPps*10);
    std::cout << "delayPps = "<<delayPps<<" delaisfpga = "<<delayFpga<<std::endl;
    setPPSConf.start(program,arguments);
    setPPSConf.waitForFinished();
    //programmation pps 1 delay si delay multiple prf

    arguments.clear();
    if(m_confTx_t.acqDelay != 0 && delayPps > -100){
        arguments << "1";
        arguments << "--delay" << QString::number(500);
    }else{
        arguments << "1";
    }
    setPPSConf.start(program,arguments);
    setPPSConf.waitForFinished();
    return sendAndVerifyConf(SET_DELAIS_ACQ,GET_DELAIS_ACQ,"delay acq",delayFpga);//nombre de pps2
}

void ICfhf8ch::analyseRx(uint16_t *dataList, int count,int partOfTransfer,bool endRead, ConfhfCan_t confData, IEvent *event)
{
    Signal_t signal;
    int nbChannel = 0;
    quint32 channelRxTemp = confData.activatedChanelR;

    for(int i = 0; i < 8; i++){
        if((channelRxTemp & 0x0001 )== 0x0001){
            nbChannel++;
        }
        channelRxTemp = channelRxTemp >> 1;

    }
    std::cout <<"nbChannel "<<nbChannel<<std::endl;
    signal.activateChannelRx =  confData.activatedChanelR;
    signal.activateChannelTx =  confData.activatedChanelE;
    signal.secondFirst = confData.timefirst;
    signal.cptFirst = confData.cptfirst;
    signal.partOfTransfer=partOfTransfer;
    signal.endRead = endRead;
    signal.typeAc = confData.typeAcq;
    signal.freqencyRx = 2500000 / qPow(2,confData.frequency);
    signal.nbDataRx = count;
    signal.nbChanneRx = nbChannel;
    if(m_databrut){
        signal.dataListbrut = (uint16_t *) malloc(count * sizeof(uint16_t));
        memcpy(signal.dataListbrut,dataList,count * sizeof(uint16_t));

    }else{
        for(int data = 0; data < count/nbChannel; data++){
            DataRx_t dataRx;
            for(int channel = 0; channel < nbChannel;channel++){
                dataRx.data.push_front(dataList[data*nbChannel+channel]);
                //  std::cout<< dataRx.data.at(channel)<<";";
            }
            signal.dataRx.push_back(dataRx);
            // std::cout<<std::endl;
            if(data%100000 == 0){
                std::cout<< "data = " <<data<<std::endl;
            }
        }
    }
    emit dataAvaible(signal,event);
    if(m_databrut){
        free(signal.dataListbrut);

    }
}

int ICfhf8ch::sendAndVerifyConf(int setIoctl, int getIoctl, QString name, int confToSend){
    int ret = 0;
    int confReturn;
    bool countDone = false;
    int nbTry = 0;
    while(!countDone && nbTry < 5){
        ret = ioctl(m_fdCf8ch,setIoctl,&confToSend);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }

        ret = ioctl(m_fdCf8ch, getIoctl, &confReturn);
        if (ret < 0) {
            perror(NULL);
            return ret;
        }
        if(confReturn == confToSend){
            countDone = true;
        }else{
            std::cout << __FUNCTION__ << " : essay nb "<< nbTry << " "<<name.toStdString()<<" = " << confReturn << " != "<< confToSend <<std::endl;
        }
        nbTry++;
    }
    if(!countDone){
        std::cout << __FUNCTION__ << " : ERROR : ecriture sur FPGA impossible" << std::endl;
        return -1;
    }
    return ret;
}
