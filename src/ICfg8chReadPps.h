#ifndef ICHG8CHREADPPS_H
#define ICHG8CHREADPPS_H

#include "IEvent.h"
#include <ifsttar_g8ch.h>
#include <unistd.h>

class ICfg8chReadPps : public QThread
{
    Q_OBJECT
public:
    ICfg8chReadPps(int fd);
    ~ICfg8chReadPps();
private :
    int m_fd;
    bool m_running;
signals:
    void dataAvaible(ppsInfos newPpsInfo, IEvent *event);
private:
    void run ();
};

#endif // ICHG8CHREADPPS_H
