/* tdmswriter.h
 *
 * Module to write .tdms files
 *
 * nicolas.bedouin@cetim.fr
 * 2023-06-07
 * 1.0.1 LE
 */
#ifndef TDMS_H_20230607
#define TDMS_H_20230607
// #pragma once
#include <map>
#include <vector>
#include <fstream>
#include <bitset>
#include <ctime>

#define kTocMetaData        1 << 1
#define kTocNewObjList      1 << 2
#define kTocRawData         1 << 3
#define kTocInterleavedData 1 << 5
#define kTocBigEndian       1 << 6
#define kTocDAQmxRawData    1 << 7

namespace TDMS
{
    const std::vector<uint8_t> TEMPORARY_OFFSET {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
    const std::vector<uint8_t> NO_RAWDATA {0xFF, 0xFF, 0xFF, 0xFF};
    const std::vector<uint8_t> TDSM {0x54, 0x44, 0x53, 0x6D};
    const std::vector<uint8_t> TOC_PADDING {0x00, 0x00, 0x00};

    enum mode_tir : short { SOFT=0, SEUIL, TRIG_IN, DATE };
    std::map<mode_tir,std::string> mode_string = {
        { SOFT, "Software" },
        { SEUIL, "Sur seuil" },
        { TRIG_IN, "Trigger in" },
        { DATE, "Sur date" }
    };

    enum TdsType : int32_t {
        I8 = 0x1,
        I16 = 0x2,
        I32 = 0x3,
        I64 = 0x4,
        U8 = 0x5,
        U16 = 0x6,
        U32 = 0x7,
        U64 = 0x8,
        SingleFloat = 0x9,
        DoubleFloat = 0xa,
        ExtendedFloat = 0xb,
        SingleFloatWithUnit = 0x19,
        DoubleFloatWithUnit = 0x1a,
        ExtendedFloatWithUnit = 0x1b,
        String = 0x20,
        Boolean = 0x21,
        TimeStamp = 0x44
    };

    typedef struct {
        TdsType type_id;
        std::vector<uint8_t> rawbytes;
    } tdms_encoded;
    
    typedef std::map<std::string,tdms_encoded> tdms_properties;
    
    typedef struct {
        std::string path;
        tdms_properties properties;
    } tdms_object;

    bool big_endian_system() {
        static unsigned long x(1);
        static bool result(reinterpret_cast<uint8_t*>(&x)[0] == 0);
        return result;
    }
    
    int bit_count(int x) { return std::bitset<8>(x).count(); }
    std::vector<int> decode_bitmask(int bitmask, int start);
    template <typename T> std::vector<uint8_t> to_bytes(T& value);
    std::string base_name(const std::string& path);

    class TdmsWriter {
        public:
            TdmsWriter(bool interleaved_buffer=true) : _interleaved_buffer{interleaved_buffer} { }
            // ~TdmsWriter() {}

            void createfile(const std::string& path, const std::string& test_name, timespec start_datetime, mode_tir mode, int averages);
            void write_output(const std::string& signal_file, int samplerate, int channelTx);
            void write_waveforms(const char* buffer, size_t buffer_size, int samplerate, int channel_mask, int gain1, int gain2, int gain3, int gain4, int gain5, int gain6, int gain7, int gain8);
            void append_waveforms(const char* buffer, size_t buffer_size);
            void close();
            
            bool is_ready() { return stream.is_open(); }

            std::string get_path() { return _path; }
            std::string get_test_name() { return _test_name; }
            std::string get_start_datetime();

        private:
            const std::string OUTPUT_GROUP = "Output";
            const std::string WAVEFORMS_GROUP = "Waveforms";
            const uint32_t VERSION = 4713; // TDMS version 2.0
            const uint32_t HEADER_SIZE = 20;
            // const uint32_t STR_HEADER_SIZE = 28;
            const uint32_t ARRAY_DIMENSION = 1;
            
            bool _interleaved_buffer;

            std::string _path = "";
            std::string _test_name = "";
            timespec _start_datetime = {0, 0};

            int32_t _number_of_channels = 0;
            int32_t _number_of_samples = 0;
            uint64_t _next_segment_offset_value_position = 0;
            std::vector<std::string> _objects_list;

            std::ofstream stream;

            // tdms_encoded encode(int8_t);
            // tdms_encoded encode(int16_t);
            tdms_encoded encode(int32_t);
            // tdms_encoded encode(int64_t);
            // tdms_encoded encode(uint8_t);
            tdms_encoded encode(uint16_t);
            // tdms_encoded encode(uint32_t);
            // tdms_encoded encode(uint64_t);
            // tdms_encoded encode(float);
            tdms_encoded encode(double);
            tdms_encoded encode(const std::string&);
            // tdms_encoded encode(bool);
            tdms_encoded encode(timespec);

            bool is_new_objects_list(const std::vector<tdms_object>& objects);
            std::vector<tdms_object> tdms_objects_without_properties();
            template<typename T> void stream_write(const T& value);
            void write_one_property(const std::string& name, const tdms_encoded& value);
            void write_properties(const tdms_properties& properties);
            void write_segment(const char* buffer, size_t buffer_size, const std::vector<tdms_object>& objects);
    };
}

#endif 
