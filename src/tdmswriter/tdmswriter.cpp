/* Spécifications du format de fichier TDMS :
 * https://www.ni.com/fr-fr/support/documentation/supplemental/07/tdms-file-format-internal-structure.html
 * https://www.ni.com/fr-fr/support/documentation/supplemental/12/writing-data-management-ready-tdms-files.html
 */
#include <iostream>
#include <string>
#include <cmath>
#include <chrono>
#include "tdmswriter.h"

using namespace std;
using namespace TDMS;

vector<int> TDMS::decode_bitmask(int bitmask, int start=0) {
    vector<int> result;
    for (int i = start; bitmask > 0; ++i, bitmask >>= 1) {
        if (bitmask & 1) { result.push_back(i); }
    }
    return result;
}

string TDMS::base_name(const string& path) {
    return path.substr(path.find_last_of("/\\") + 1);
}

template <typename T>
vector<uint8_t> TDMS::to_bytes(T& value) {
    uint8_t* value_bytes = reinterpret_cast<uint8_t*>(&value);
    return vector<uint8_t>(value_bytes, value_bytes + sizeof(value));
}

tdms_encoded TdmsWriter::encode(uint16_t value) {
    return tdms_encoded { TdsType::U16, to_bytes(value) };
}

tdms_encoded TdmsWriter::encode(int32_t value) {
    return tdms_encoded { TdsType::I32, to_bytes(value) };
}

tdms_encoded TdmsWriter::encode(double value) {
    return tdms_encoded { TdsType::DoubleFloat, to_bytes(value) };
}

tdms_encoded TdmsWriter::encode(const string& value) {
    uint32_t bytes_size = value.size();
    vector<uint8_t> rawbytes = to_bytes(bytes_size);
    rawbytes.insert(rawbytes.end(), value.begin(), value.end());
    tdms_encoded out = { TdsType::String, rawbytes };
    return out;
}

tdms_encoded TdmsWriter::encode(timespec dt) {
    int64_t secs = (int64_t)dt.tv_sec + 2082844800;
    uint64_t frac_secs = 18446744073 * (uint64_t)dt.tv_nsec + ((uint64_t)dt.tv_nsec * 1385843) / 1953125;
    vector<uint8_t> rawbytes = to_bytes(frac_secs);
    vector<uint8_t> rawbytes_more = to_bytes(secs);
    rawbytes.insert(rawbytes.end(), rawbytes_more.begin(), rawbytes_more.end());
    tdms_encoded out = { TdsType::TimeStamp, rawbytes };
    return out;
}

string TdmsWriter::get_start_datetime() {
    char str_buffer[100];
    strftime(str_buffer, sizeof(str_buffer), "%Y-%m-%d %H:%M:%S", localtime(&_start_datetime.tv_sec));
    string out = str_buffer;
    out.append(" et " + to_string(_start_datetime.tv_nsec) + " ns (heure locale)");
    return out;
}

void TdmsWriter::createfile(const string& path, const string& test_name, timespec start_datetime, mode_tir mode, int averages) {
    _path = path;
    _test_name = test_name;
    _start_datetime = start_datetime;

    // Création d’un nouveau fichier (écrase si existe déjà)
    if (stream.is_open()) {
        std::cout << "createfile() : a file is already opened, .close() first\n";
        return;
    }
    stream.open(_path, ios::binary);
    if (!(stream.good())) {
        std::cout << "createfile() : can’t create " + _path + "\n";
        return;
    }
    string basename = base_name(_path);
    // Écriture du segment racine
    vector<tdms_object> objects = {
        {"/", // root path inside the tdms file
            {
                {"name", encode(basename)},
                {"title", encode(_test_name)},
                {"author", encode("Pégase")},
                // {"description", encode("")},
                {"datetime", encode(_start_datetime)},
                {"mode", encode(mode_string[mode])},
                {"averages", encode((uint16_t)averages)}
            }
        }
    };
    write_segment(nullptr, 0, objects);
}

void TdmsWriter::close() {
    // TODO: update wf_samples with the total number of samples at the end ?
    _number_of_channels = 0;
    _number_of_samples = 0;
    _next_segment_offset_value_position = 0;
    _objects_list.clear();
    stream.close();
}

bool TdmsWriter::is_new_objects_list(const vector<tdms_object>& objects) {
    if (_objects_list.size() != objects.size()) { return true; }
    size_t i = 0;
    for (const auto &item : objects) {
        if (item.path != _objects_list[i]) { return true; }
        i++;
    }
    return false;
}

vector<tdms_object> TdmsWriter::tdms_objects_without_properties() {
    vector<tdms_object> result;
    for (const auto& key : _objects_list) {
        result.push_back({key, {}});
    }
    return result;
}

template<typename T>
void TdmsWriter::stream_write(const T& value)
{
    auto bytes = reinterpret_cast<const char*>(&value);
    copy(bytes, bytes + sizeof(T), ostreambuf_iterator<char>(stream));
}

void TdmsWriter::write_one_property(const string& name, const tdms_encoded& value) {
    vector<uint8_t> rawbytes = value.rawbytes;
    vector<uint8_t> name_bytes = encode(name).rawbytes;
    stream.write(reinterpret_cast<const char*>(name_bytes.data()), name_bytes.size());
    stream_write(value.type_id);
    stream.write(reinterpret_cast<const char*>(rawbytes.data()), value.rawbytes.size());
}

void TdmsWriter::write_properties(const tdms_properties& properties) {
    // TODO: vector de struct à la place de map ?
    uint32_t properties_count = properties.size();
    stream_write(properties_count);
    for (const auto &item : properties) {
        string name = item.first;
        tdms_encoded value = item.second;
        write_one_property(name, value);
    }    
}

void TdmsWriter::write_segment(const char* buffer, size_t buffer_size, const vector<tdms_object>& objects) {
    const int32_t type_id = TdsType::I16; // cette version de tdmwriter ne gère que I16 pour les buffers de données

    if ((buffer_size==0) && (objects.size()==0)) { return; }

    uint8_t toc_mask = 0;
    if (buffer_size>0) {
        toc_mask |= kTocRawData;
        if (_number_of_channels>1) {
            toc_mask |= kTocInterleavedData;
        }
    }
    if (objects.size()>0) {
        toc_mask |= kTocMetaData;
        if (is_new_objects_list(objects)) {
            toc_mask |= kTocNewObjList;
            _objects_list.clear();
            for (auto const& item : objects) {
                _objects_list.push_back(item.path);
            }
        }
    }

    // Lead-in
    stream.write(reinterpret_cast<const char*>(TDSM.data()), TDSM.size());
    stream.write(reinterpret_cast<const char*>(&toc_mask), sizeof(toc_mask));
    stream.write(reinterpret_cast<const char*>(TOC_PADDING.data()), TOC_PADDING.size());
    stream_write((uint32_t)VERSION);
    _next_segment_offset_value_position = stream.tellp();
    // valeur d’offset vers le segment suivant (ré-écrite à la fermeture du segment) :
    stream.write(reinterpret_cast<const char*>(TEMPORARY_OFFSET.data()), TEMPORARY_OFFSET.size());
    uint64_t rawdata_offset_value_position = stream.tellp();
    stream.write(reinterpret_cast<const char*>(TEMPORARY_OFFSET.data()), TEMPORARY_OFFSET.size());
    
    // Écriture des MetaData
    if (toc_mask & kTocMetaData) {
        uint32_t objects_count = objects.size();
        stream_write((uint32_t)objects_count);
        for (const auto &item : objects) {
            vector<uint8_t> path_rawbytes = encode(item.path).rawbytes;
            stream.write(reinterpret_cast<const char*>(path_rawbytes.data()), path_rawbytes.size());
            if (toc_mask & kTocRawData) {
                uint64_t total_number_of_samples = _number_of_samples;
                stream_write((uint32_t)HEADER_SIZE);
                stream_write((int32_t)type_id);
                stream_write((uint32_t)ARRAY_DIMENSION);
                stream_write((uint64_t)total_number_of_samples);
            } else { // Si absence de données dans le segment:
                stream.write(reinterpret_cast<const char*>(NO_RAWDATA.data()), NO_RAWDATA.size());
            }
            write_properties(item.properties);
        }
    }

    // Mise à jour de l’offset de début des rawdata
    uint64_t rawdata_start_position = stream.tellp();
    stream.seekp(rawdata_offset_value_position, ios_base::beg);
    uint64_t offset = rawdata_start_position - (rawdata_offset_value_position + 8);
    stream_write((uint64_t)offset);
    stream.seekp(rawdata_start_position, ios_base::beg); // retour à la fin des MetaData

    // Écriture des rawdata
    if (toc_mask & kTocRawData) { stream.write(buffer, buffer_size); }

    // Mise à jour offset de fin de segment
    uint64_t end_of_segment_position = stream.tellp();
    stream.seekp(_next_segment_offset_value_position, ios_base::beg);
    offset = end_of_segment_position - (_next_segment_offset_value_position + 16);
    stream_write((uint64_t)offset);
    stream.seekp(end_of_segment_position, ios_base::beg); // retour à la fin du segment
}

void TdmsWriter::write_output(
        const string& signal_file,
        int samplerate,
        int channelTx
    ) {
    if (!(samplerate>0)) {
        std::cout << "write_output() : `samplerate` must be > 0.";
        return;
    }
    
    ifstream input_file(signal_file, ios::binary);
    if (!(input_file.good())) {
        std::cout << "write_output() : can’t open " + signal_file + "\n";
        return;
    }

    // Lecture de input_file avec conversion BE vers LE
    vector<int16_t> file_data;
    int16_t value;
    while (input_file.read((char*) &value, sizeof(value))) {
        file_data.push_back(((value >> 8) & 0x00FF) | ((value << 8) & 0xFF00));
    }
    input_file.close();
    const char* buffer = reinterpret_cast<const char*>(file_data.data());
    size_t buffer_size = file_data.size()*sizeof(int16_t);

    _number_of_channels = 1;
    _number_of_samples = buffer_size/2;

    double time_increment = 1.0/(double)(samplerate);

    // ========== Écriture du dossier Output =========
    string path = "/'" + OUTPUT_GROUP + "'";
    string basename = base_name(signal_file);
    vector<tdms_object> objects = {
        {path,
            {
                {"output_signal_file", encode(basename)},
                {"output_channel", encode((uint16_t)channelTx)}
            }
        }
    };
    write_segment(nullptr, 0, objects);
    objects.clear();

    // ========== Écriture du segment output =========
    path = "/'" + OUTPUT_GROUP + "'/'Channel " + to_string(channelTx) + "'";
    objects = {
        {path,
            {
                { "unit_string", encode("#") }, // String representation of the channel unit (required)
                { "wf_start_time", encode(_start_datetime) }, // Start DateTime value of the time axis (optional)
                { "wf_start_offset", encode((double)0.0) }, // Start offset value of the x-axis (required) toujours 0 pour les signaux temporels
                { "wf_increment", encode((double)time_increment) }, // Increment value of the x-axis (required)
                { "wf_samples", encode((int32_t)_number_of_samples) }, // Number of values of the x-axis (Required)
                { "wf_xname", encode("Time") }, // Name of the x-axis quantity (required)
                { "wf_xunit_string", encode("s") } // Unit of the x-axis quantity (required)
            }
        }
    };
    write_segment(buffer, buffer_size, objects);
}

void TdmsWriter::write_waveforms(
        const char* buffer,
        size_t buffer_size,
        int samplerate,
        int channel_mask,
        int gain1,
        int gain2,
        int gain3,
        int gain4,
        int gain5,
        int gain6,
        int gain7,
        int gain8
    ) {
    if (!(samplerate>0)) {
        throw invalid_argument("Argument `samplerate` must be > 0.");
    }
    if (!(channel_mask>0)) {
        throw invalid_argument("Argument `channel_mask` must be > 0.");
    }
    
    if (buffer_size<2) { return; }

    _number_of_channels = bit_count(channel_mask);
    _number_of_samples = (buffer_size/2) / _number_of_channels;
    int r = (buffer_size/2) % _number_of_channels;
    if (r != 0) {
        throw invalid_argument("Size of `buffer` is inconsistent.");
    }

    double time_increment = 1.0/(double)(samplerate);

    vector<int> gains {gain1, gain2, gain3, gain4, gain5, gain6, gain7, gain8};
    vector<int> objects_gains;
    for (int i = 0; i < 8; i++) {
        if ((channel_mask >> i) & 1) {
            objects_gains.push_back(gains[i]);
        }
    }

    // // ========== Écriture de properties dans le dossier Waveforms =========
    //TODO à utiliser si besoin de propriétés associées au groupe WAVEFORMS_GROUP
    // vector<tdms_object> objects = {
    //     { "/'" + WAVEFORMS_GROUP + "'",
    //         {
    //             { "SN", encode.String("DG 6246") } // exemple
    //         }
    //     }
    // };
    // write_segment(nullptr, 0, objects);
    // objects.clear();
    
    // ========== Écriture des voies du segment waveforms =========
    vector<int> channels = decode_bitmask(channel_mask, 1);
    vector<tdms_object> objects;
    for (std::vector<int>::size_type i = 0; i < channels.size(); i++) {
        string path = "/'" + WAVEFORMS_GROUP + "'/'Channel " + to_string(channels[i]) + "'";
        tdms_object object {path,
            {
                { "unit_string", encode("#") }, // String representation of the channel unit (required)
                { "wf_start_time", encode(_start_datetime) }, // Start DateTime of the whole TDMS file (required)
                { "wf_start_offset", encode((double)0.0) }, // Start offset value of the x-axis (required) toujours 0 pour les signaux temporels
                { "wf_increment", encode((double)time_increment) }, // Increment value of the x-axis (required)
                { "wf_samples", encode((int32_t)_number_of_samples) }, // Number of values of the x-axis (Required)
                { "wf_xname", encode("Time") }, // Name of the x-axis quantity (required)
                { "wf_xunit_string", encode("s") }, // Unit of the x-axis quantity (required)
                { "amplifier_gain", encode((int32_t)objects_gains[i]) }
            }
        };
        objects.push_back(object);
    }
    write_segment(buffer, buffer_size, objects);
}

// Attention : avant d’utiliser cette fonction,
// - il faut avoir exécuté une fois `write_waveforms()`
// - les données de `buffer` doivent être similaires à celles utilisées pour `write_waveforms()` (seul `number_of_samples` peut être différent).
void TdmsWriter::append_waveforms(const char* buffer, size_t buffer_size) {
    int32_t new_number_of_samples = (buffer_size/2) / _number_of_channels;
    int r = (buffer_size/2) % _number_of_channels;
    if (r != 0) {
        throw invalid_argument("Size of `buffer` is inconsistent.");
    }

    if (new_number_of_samples==_number_of_samples) {
        // On peut juste écrire directement `buffer` et mettre à jour `end_of_segment_position`
        stream.write(buffer, buffer_size);

        // Mise à jour offset de fin de segment
        uint64_t end_of_segment_position = stream.tellp();
        stream.seekp(_next_segment_offset_value_position, ios_base::beg);
        uint64_t offset = end_of_segment_position - (_next_segment_offset_value_position + 16);
        stream_write((uint64_t)offset);
        stream.seekp(end_of_segment_position, ios_base::beg); // retour à la fin du segment
    } else {
        // Nouvelle taille -> nouveau segment (mais les `properties` ne sont pas réécrites)
        _number_of_samples = new_number_of_samples;
        write_segment(buffer, buffer_size, tdms_objects_without_properties());
    }
}







// Exemple d’utilisation :

int main()
{
    if (big_endian_system()) {
        throw runtime_error("Big endian system : not supported.");
    }

    // BUFFER SIMULÉ : ===============================
    const double PI = 3.14159265358979323846;

    const double amplitude = 30000.0; // < 32767.0
    constexpr int samplerate = 2000000; // Hz
    constexpr double duration = 2.3; // s
    constexpr int samples = (int)(duration * (double)samplerate);

    vector<int16_t> data(2*samples);
    const char* buffer = reinterpret_cast<const char*>(data.data());
    size_t buffer_size = data.size() * sizeof(int16_t);

    // Génération d’un buffer de données (2 voies) entrelacées en little endian
    for (int i = 0; i < samples; i++) {
        data[2*i] = (int16_t)round(amplitude * sin(2.0 * PI * (double)i / (double)samplerate * 50000.0));
        data[2*i+1] = (int16_t)round(0.5 * amplitude * sin(2.0 * PI * (double)i / (double)samplerate * 70000.0));
    }
    // ===============================================


    // Mesure du temps d’écriture du fichier
    auto start = chrono::high_resolution_clock::now();

    string path = "/lcpc/sd_stockage/test_le.tdms";
    string test_name = "Mon essai";
    timespec start_datetime = {1685451714, 987654321}; // seconds (epoch 01/01/1970), nanoseconds

    mode_tir mode = SOFT;
    int channelTx = 3;
    int averages = 0;
    // ... TODO: autres propriétés à sauvegarder

    int channel_mask = 0b10000001;
    bool interleaved_buffer = true; // les voies du FPGA sont entrelacées

    TdmsWriter writer {
        interleaved_buffer // true si les données des voies acquises sont entrelacées
    };

    // Crée le fichier .tdms et écrit les premières metadonnées
    // path : chemin + nom de fichier .tdms
    // test_name : nom de l’essai (écrit en propriété dans le .tdms)
    // start_datetime : date du premier échantillon (précision à la nano-seconde)
    // mode : mode de déclenchement du tir ou de l’enregistrement (écrit en propriété dans le .tdms)
    // averages : nombre de moyennes effectuées (écrit en propriété dans le .tdms)
    writer.createfile(
        path,
        test_name,
        start_datetime,
        mode,
        averages
    );
    
    if (!writer.is_ready()) {
        std::cout << "Échec de création du fichier " << writer.get_path() << std::endl;
        return 0;
    }

    // - Optionnel - Sauvegarde du signal de tir
    // signal_file : chemin vers le fichier .bin de tir
    // samplerate : fréquence d'échantillonnage du tir (10 MHz ?)
    // channelTx : numéro de voie utilisée pour le tir
    string signal_file = "signal_768_10_50000.bin";
    writer.write_output(
        signal_file,
        10000000,
        channelTx
    );

    // Écriture du premier buffer des données acquises
    // buffer : char*
    // buffer_size : taille du buffer
    // samplerate : fréquence d’échantillonnage de l’acquisition
    // channel_mask : bitmask des voies activées
    // gain1 ... gain8 : gain des voies (écrites en propriété dans le .tdms)
    writer.write_waveforms(
        buffer,
        buffer_size,
        samplerate,
        channel_mask,
        11, 21, 31, 41, 51, 61, 71, 81
    );

    // Écriture des buffers suivants
    writer.append_waveforms(buffer, buffer_size);
    // ...

    // Écriture du dernier buffer (qui peut être de taille différente)
    writer.append_waveforms(buffer, buffer_size/4);
    
    // Fermeture du fichier (TODO peut servir à écrire des propriétés supplémentaires à la fin du fichier si besoin)
    writer.close();

    // End the timer
    auto end = chrono::high_resolution_clock::now();

    // Durée de l’enregistrement du buffer vers le .tdms
    chrono::duration<double> elapsed = end - start;
    double duration_in_seconds = elapsed.count();

    std::cout << "Fichier .tdms sauvegardé ici : " << writer.get_path() << "   (" << writer.get_test_name() << ")\n";
    std::cout << "Date du premier échantillon : " << writer.get_start_datetime() << std::endl;
    std::cout << "Durée d’exécution : " << duration_in_seconds << " seconds\n";

    return 0;
};
