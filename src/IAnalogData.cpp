	#include "IAnalogData.h"

IAnalogData::IAnalogData()
{
    m_s = 0;
    m_ns = 0;
    m_Default = false;
    m_data.reserve(8);
}

IAnalogData::IAnalogData(int nbChannels)
{
    m_s = 0;
    m_ns = 0;
    m_Default = false;
    m_data.reserve(nbChannels);
}

QVector<int> IAnalogData::dataToUv() const
{
    QVector<int> dataInUv;
    for (int i = 0; i < m_data.count(); i++)
	{
		
	//~ }
	 //~ (int data, m_data) {
        //~ //todo utiliser un quint64
       long long temp;
       temp = m_data[i]*(2.5 / pow(2, 23)) * 1000000;
       dataInUv.push_back((int)temp);
    }
    return dataInUv;
}
