#ifndef ICFBETON_H
#define ICFBETON_H

#include "ICfhf8ch.h"


enum Gain1_ad8253 {
    G1_0=0b00000000,
    G1_20=0b00010000,
    G1_40=0b00100000,
    G1_60=0b00110000
};
enum Gain2_ltc6912 {
    G2_NotUse = 0b00001100,
    G2_less120 = 0b00001000,
    G2_0 = 0b00000001,
    G2_6 = 0b00000010,
    G2_14 = 0b00000011,
    G2_20 = 0b00000100,
    G2_26 = 0b00000101,
    G2_34 = 0b00000110,
    G2_40 = 0b00000111
};


class ICfbeton : public ICfhf8ch
{
public:
    ICfbeton();
    int setChannelTx(int channelTx);
    int setGainRx(int channel,Gain1_ad8253 gain1,Gain2_ltc6912 gain2 );//gain pour un channel acquisition
    int setGainRx(Gain1_ad8253 gain1,Gain2_ltc6912 gain2 );//gain pour tous les channels d'acquisition
    int setGainRx(int channel,quint8 gain );//gain pour un channel acquisition
    int setGainRx(quint8 gain);//gain pour tous les channels d'acquisition
};

#endif // ICFBETON_H
