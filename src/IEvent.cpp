#include <QMetaEnum>
#include <QString>
#include "IEvent.h"
#include "error.h"
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <sys/stat.h>
#include <fcntl.h>
#include <QThread>
#include <unistd.h>
#include <QFileInfo>
#include <iostream>



/**
 *	\fn		IEvent :: IEvent ()
 *	\brief	Constructor of the IEvent class
 *
 */
IEvent::IEvent()
{
    // Variables initialization
    int  iRet       = 0;
    m_oCurrentGeoDatationData = {};

    // Open the link with the synchronization driver and check if the open happened well
    m_fd = open("/dev/pegase-sync-drv", O_RDWR);
    if (m_fd < 0)
        return;

    // Get the GeoDatation informations form the driver
    iRet = ioctl(m_fd, SYNC_IOCTL_GET_OFFICIAL_GD_DATA, &m_oCurrentGeoDatationData);
    // If an error occure during the get of the GeoDatation informations
    if (iRet < 0) {
        // Get the GeoDatation informations from the system time (using time.h)
        GetSystemTime();
        //cout << CEVENT_PRINT_NAME << "WARNING : An error occurred during the communication with the synchronization driver, the system hour is currently used (the microsecond is incorrect)" << endl << endl;
    }

}

/**
 *	\fn		IEvent :: IEvent (GeoDatationData * GDData)
 *	\brief 	Constructor of the CEvent class using a GeoDatationData structure
 *
 *	\param[in]	GDData :	pointer to the GeoDatationData structure to use to create the CEvent object
 */
IEvent :: IEvent (GpsData * GDData)
{
    m_fd = -1; // For the destructor to skip the close

    memcpy (&m_oCurrentGeoDatationData, GDData, sizeof(GpsData));
}

/**
 *	\fn		IEvent :: ~IEvent (int pin)
 *	\brief	Deconstructor of the IEvent class
 *
 */
IEvent::~IEvent()
{
    if(m_fd>0)
        close(m_fd);
}

/**
 *	\fn		void IEvent :: RefreshTime()
 *	\brief 	Refresh the time of CEvent object using current time from sync driver or system time
 */
void IEvent :: RefreshTime(){
    int iRet = 0;

    // If the sync driver is open
    if( m_fd >= 0 ){
        // Get the GeoDatation informations form the driver
        iRet = ioctl(m_fd, SYNC_IOCTL_GET_OFFICIAL_GD_DATA, &m_oCurrentGeoDatationData);
        // If an error occure during the get of the GeoDatation informations
        if (iRet < 0) {
            // Get the GeoDatation informations from the system time (using time.h)
            GetSystemTime();
        }
    }
}



/**
 *	\fn		int CEvent :: GetSystemTime()
 *	\brief 	Get the system time using the time.h library
 *
 * 	\return	A negative number if an error occured, 0 otherwise
 */
int IEvent :: GetSystemTime()
{
    // Gets the current time
    time_t tDate = time (NULL);
    struct tm *tm_now = gmtime( &tDate );

    m_oCurrentGeoDatationData.uiSeconde = tm_now->tm_sec;
    m_oCurrentGeoDatationData.uiMinute = tm_now->tm_min;
    m_oCurrentGeoDatationData.uiHour = tm_now->tm_hour;
    m_oCurrentGeoDatationData.uiDay = tm_now->tm_mday;
    m_oCurrentGeoDatationData.uiMonth = tm_now->tm_mon + 1;
    m_oCurrentGeoDatationData.uiYear = tm_now->tm_year + 1900;
    m_oCurrentGeoDatationData.uiTimerStep = 0;

    // IFSTTAR Location
    m_oCurrentGeoDatationData.dLatitude = 4709.37501;
    m_oCurrentGeoDatationData.dLongitude = 138.29299;

    // Init quality information (which are currently unavailable)
    m_oCurrentGeoDatationData.uiSatelliteUsed = 0;

    return 0;
}

/*
 *	Accesor for the decoded informations.
 *	Year, month, day, hour, minute, second, number of satellite, quality, ...
 */

/**
 *	\fn 	unsigned int CEvent :: GetYear ()
 *	\brief 	Send back the Year of the event
 *
 *	\return	The year of the event
 */
unsigned int IEvent :: GetYear ()
{
    return  m_oCurrentGeoDatationData.uiYear;
}




/**
 *	\fn 	unsigned int CEvent :: GetMonth ()
 *	\brief 	Send back the Month of the event
 *
 *	\return The Month of the event
 */
unsigned int IEvent :: GetMonth ()
{
    return  m_oCurrentGeoDatationData.uiMonth;
}


/**
 *	\fn		unsigned int CEvent :: GetDay ()
 *	\brief	Send back the Day of the event
 *
 *	\return	The Day of the event
 */
unsigned int IEvent :: GetDay ()
{
    return m_oCurrentGeoDatationData.uiDay;
}


/**
 *	\fn		unsigned int CEvent :: GetHour ()
 *	\brief	Send back the Hour of the event (UTC)
 *
 *	\return The Hour of the event (UTC)
 */
unsigned int IEvent :: GetHour ()
{
    return m_oCurrentGeoDatationData.uiHour;
}


/**
 *	\fn		unsigned int CEvent :: GetMinute ()
 *	\brief	Send back the Minute of the event
 *
 *	\return The Minute of the event
 */
unsigned int IEvent :: GetMinute ()
{
    return m_oCurrentGeoDatationData.uiMinute;
}


/**
 *	\fn		unsigned int CEvent :: GetSecond ()
 *	\brief	Send back the Second of the event
 *
 *	\return	The second of the event
 */
unsigned int IEvent :: GetSecond ()
{
    return m_oCurrentGeoDatationData.uiSeconde;
}


/**
 *	\fn		unsigned int CEvent :: GetMicroSecond ()
 *	\brief	Send back the µSecond of the event
 *
 *	\return	The µSecond of the event
 */
unsigned int IEvent :: GetMicroSecond ()
{
    double d= (double)((double)m_oCurrentGeoDatationData.uiTimerStep / m_oCurrentGeoDatationData.uiPeriodaverage);

    unsigned int uiMicroSecond = (unsigned int)(d*1000000);
    return uiMicroSecond;
}

/**
 *	\fn		time_t CEvent :: GetTimeInSecondsEpoch()
 *	\brief	Send back the time in Second since 1970
 *
 *	\return The time in seconds of the event since 1970
 */
time_t IEvent :: GetTimeInSecondsEpoch()
{
    struct tm t;  // Initalize to all 0's

    t.tm_year = m_oCurrentGeoDatationData.uiYear - 1900;
    t.tm_mon = m_oCurrentGeoDatationData.uiMonth - 1;
    t.tm_mday = m_oCurrentGeoDatationData.uiDay;
    t.tm_hour = m_oCurrentGeoDatationData.uiHour;
    t.tm_min = m_oCurrentGeoDatationData.uiMinute;
    t.tm_sec = m_oCurrentGeoDatationData.uiSeconde;

    return mktime(&t);
}



/**
 *	\fn		unsigned int CEvent :: GetTimeInSeconds()
 *	\brief	Send back the time in Second of the event
 *
 *  Time in second is : 3600 X Hour + 60 X Minute + Second
 *  This method is used to have the time of the event in seconds (UTC)
 *
 *	\return The time in seconds of the event
 */
unsigned int IEvent :: GetTimeInSeconds()
{
    return m_oCurrentGeoDatationData.uiHour*3600+m_oCurrentGeoDatationData.uiMinute*60+m_oCurrentGeoDatationData.uiSeconde;
}


/**
 *	\fn		double CEvent :: GetLatitude ()
 *	\brief 	Send back the raw latitude data of the event sent by the GPS
 *
 *	\return The raw latitude data of the event
 */
double IEvent :: GetLatitude ()
{
    return m_oCurrentGeoDatationData.dLatitude;
}

/**
 *	\fn		double CEvent :: GetPreciseLatitude ()
 *	\brief	Send back the latitude data of the event using the format : DD.HHXXXX
 *
 *	With :
 *			- DD : degrees
 *			- HH : hundredths of degrees (0-99)
 *			- XXXX : rest of the decimale part of the degrees (0-9999)
 *
 *	This is the format most currently used
 *
 *	\return The latitude data of the event using the format : DD.HHXXXX
 */
double IEvent :: GetPreciseLatitude ()
{
    double 	DecLatitude = 0.0;
    double	ResteLatitude = 0.0;
    int 	EntierLatitude = 0;

    EntierLatitude = (int)floor(m_oCurrentGeoDatationData.dLatitude / 100.0 );
    ResteLatitude =  (m_oCurrentGeoDatationData.dLatitude - (EntierLatitude * 100));
    DecLatitude = (double) ((double) EntierLatitude + (ResteLatitude / 60.0));
    return DecLatitude;
}

/**
 *	\fn		unsigned int CEvent :: GetLatitudeDeg ()
 *	\brief	Send back the degrees of the latitude of the event using the format : DD
 *
 *	With :
 *			- DD : degrees
 *
 *	\return	The degrees of the latitude of the event using the format : DD
 */
unsigned int IEvent :: GetLatitudeDeg ()
{
    return (int) floor(m_oCurrentGeoDatationData.dLatitude / 100.0);
}

/**
 *	\fn		unsigned int CEvent :: GetLatitudeMin ()
 *	\brief	Send back the minutes of the latitude of the event using the format : MM
 *
 *	With :
 *			- MM : minutes (between 0 and 59)
 *
 *	\return	The minutes of the latitude of the event using the format : MM
 */
unsigned int IEvent :: GetLatitudeMin ()
{
    return (int) floor(((m_oCurrentGeoDatationData.dLatitude/100) - GetLatitudeDeg ())*100);
}

/**
 *	\fn		unsigned int CEvent :: GetLatitudeSec ()
 *	\brief	Send back the seconds of the latitude of the event using the format : SS
 *
 *	With :
 *			- SS : seconds (between 0 and 59)
 *
 *	\return	The seconds of the latitude of the event using the format : SS
 */
unsigned int IEvent :: GetLatitudeSec ()
{
    double dDecMinutes = 0, dSecondes = 0;

    dDecMinutes = (m_oCurrentGeoDatationData.dLatitude - floor(m_oCurrentGeoDatationData.dLatitude))*100;
    dSecondes = dDecMinutes/100*60;

    return floor(dSecondes);
}

/**
 *	\fn		unsigned int CEvent :: GetLatitudeHSec ()
 *	\brief	Send back the hundredths of a seconds of the latitude of the event using the format : HH
 *
 *	With :
 *			- HH : hundredths of a seconds (between 0 and 99)
 *
 *	\return	The hundredths of a seconds of the latitude of the event using the format : HH
 */
unsigned int IEvent :: GetLatitudeHSec ()
{
    double dDecMinutes = 0, dSecondes = 0;

    dDecMinutes = (m_oCurrentGeoDatationData.dLatitude - floor(m_oCurrentGeoDatationData.dLatitude))*100;
    dSecondes = dDecMinutes/100*60;

    return (dSecondes - floor(dSecondes))*100;
}

/**
 *	\fn		double CEvent :: GetLongitude ()
 *	\brief	Send back the raw longitude data of the event sent by the GPS
 *
 *	\return	The raw longitude data of the event sent by the GPS
 */
double IEvent :: GetLongitude ()
{
    return m_oCurrentGeoDatationData.dLongitude;
}

/**
 *	\fn		double CEvent :: GetPreciseLongitude ()
 *	\brief	Send back the longitude data of the event using the format : DD.HHXXXX
 *
 *	With :
 *			- DD : degrees
 *			- HH : hundredths of degrees (0-99)
 *			- XXXX : rest of the decimale part of the degrees (0-9999)
 *
 *	This is the format most currently used
 *
 *	\return The longitude data of the event using the format : DD.HHXXXX
 */
double IEvent :: GetPreciseLongitude ()
{
    double 	DecLongitude = 0.0;
    double	ResteLongitude = 0.0;
    int 	EntierLongitude = 0;

    EntierLongitude = (int)floor(m_oCurrentGeoDatationData.dLongitude / 100.0 );
    ResteLongitude =  (m_oCurrentGeoDatationData.dLongitude - (EntierLongitude * 100));
    DecLongitude = (double) ((double) EntierLongitude + (ResteLongitude / 60.0));
    return DecLongitude;
}

/**
 *	\fn		unsigned int CEvent :: GetLongitudeDeg ()
 *	\brief	Send back the degrees of the longitude of the event using the format : DD
 *
 * 	With :
 *			- DD : degrees
 *
 *	\return	The degrees of the longitude of the event using the format : DD
 */
unsigned int IEvent :: GetLongitudeDeg ()
{
    return (int) floor(m_oCurrentGeoDatationData.dLongitude / 100.0);
}


/**
 *	\fn		unsigned int CEvent :: GetLongitudeMin ()
 *	\brief	Send back the minutes of the longitude of the event using the format : MM
 *
 *	With :
 *			- MM : minutes (between 0 and 59)
 *
 *	\return	The minutes of the longitude of the event using the format : MM
 */
unsigned int IEvent :: GetLongitudeMin ()
{
    return (int) floor(((m_oCurrentGeoDatationData.dLongitude/100) - GetLongitudeDeg ())*100);
}

/**
 *	\fn		unsigned int CEvent :: GetLongitudeSec ()
 *	\brief	Send back the seconds of the longitude of the event using the format : SS
 *
 *	With :
 *			- SS : seconds (between 0 and 59)
 *
 *	\return	The seconds of the longitude of the event using the format : SS
 */
unsigned int IEvent :: GetLongitudeSec ()
{
    double dDecMinutes = 0, dSecondes = 0;

    dDecMinutes = (m_oCurrentGeoDatationData.dLongitude - floor(m_oCurrentGeoDatationData.dLongitude))*100;
    dSecondes = dDecMinutes/100*60;

    return floor(dSecondes);
}

/**
 *	\fn		unsigned int CEvent :: GetLongitudeHSec ()
 *	\brief  Send back the hundredths of a seconds of the longitude of the event using the format : HH
 *
 *	With :
 *			- HH : hundredths of a seconds (between 0 and 99)
 *
 *	\return	The hundredths of a seconds of the longitude of the event using the format : HH
 */
unsigned int IEvent :: GetLongitudeHSec ()
{
    double dDecMinutes = 0, dSecondes = 0;

    dDecMinutes = (m_oCurrentGeoDatationData.dLongitude - floor(m_oCurrentGeoDatationData.dLongitude))*100;
    dSecondes = dDecMinutes/100*60;

    return (dSecondes - floor(dSecondes))*100;
}

/**
 *	\fn		unsigned int CEvent :: GetNbSat()
 *	\brief	Send back the number of satellites used to obtain the other informations about the event
 *
 *	\return The number of satellites used to obtain the other informations about the event
 */
unsigned int IEvent :: GetNbSat()
{
    return m_oCurrentGeoDatationData.uiSatelliteUsed;
}

/**
 *	\fn		GpsData CEvent::getGpsData(void)
 *	\brief	Get current GpsData
 *
 *	\return GpsData
 */
GpsData IEvent::getGpsData(void)
{
    return m_oCurrentGeoDatationData;
}

