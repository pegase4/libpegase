#ifndef IREADHFCAN_H
#define IREADHFCAN_H
#include "IEvent.h"
#include <ifsttar_hf8ch.h>
#include <unistd.h>

#define MAX_DATA_EMIT  20160000  //20160000 bytes

class IReadhfcan : public QThread
{
    Q_OBJECT
public:
    IReadhfcan(int fd,int maxDataRead);
    ~IReadhfcan();
private :
    int  m_fd;
    bool m_running;
    //int m_count;
    //int m_nbChannel;
    int _maxDataRead;
signals:
    void dataAvaible(uint16_t *data,int count,int partOfTransfer,bool endRead, ConfhfCan_t confData, IEvent *event);
private:
    void run ();
};

#endif // IREADHFCAN_H
