#ifndef INUMDATA_H
#define INUMDATA_H

#include <QtGlobal>
#include <QList>
#include <ifsttar_8ch.h>

class INumData
{
public:
    INumData();
    quint64 m_s;
    quint32 m_ns;
    quint16 m_data;
    quint32 m_nbPasEvent;
    ppsInfos m_pps;
    bool m_Default;

    QList<bool> dataToQList();
};

#endif // INUMDATA_H
