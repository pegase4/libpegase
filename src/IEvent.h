/**
 *	\file	IEvent.h
 *
 *	\brief 	This header file describes the IEvent class and all its methods and attributes.
 *
 *	\author LEMARCHAND Laurent
 *
 *	\version v1.0
 *
 *	\date 25th of January 2017
 *
 */
#ifndef IEVENT_H
#define IEVENT_H

#include <sys/ioctl.h>
#include <QString>
#include <QObject>
#include <QThread>
#include <poll.h>
#include <ifsttar_sync.h>
#include <time.h>
#include <math.h>

/*
*	\class IEvent
*
*	\brief 	The IGpio class allows to open, read, and write on a GPIO using its entry in '/dev'
*
*	It also allows to register a functions that will be called when a special event occurs on the GPIO.
*
*	\author LEMARCHAND Laurent
*
*	\version v1.0
*
*	\date 14th of February 2018
*
*/
class IEvent : public QObject

{
    Q_OBJECT

public:
    IEvent();
    IEvent(GpsData * GDData);
    ~IEvent();

    void RefreshTime();
    int GetSystemTime();

    unsigned int GetYear();				/**< \brief Send back the Year of the event */
    unsigned int GetMonth();			/**< \brief Send back the Month of the event */
    unsigned int GetDay();				/**< \brief Send back the Day of the event */
    unsigned int GetHour();				/**< \brief Send back the Hour of the event (UTC) */
    unsigned int GetMinute();			/**< \brief Send back the Minute of the event */
    unsigned int GetSecond();			/**< \brief Send back the Second of the event */
    unsigned int GetMicroSecond();		/**< \brief Send back the µSecond of the event */
    unsigned int GetTimeInSeconds();	/**< \brief Send back the time in Second of the event */
    time_t GetTimeInSecondsEpoch();     /**< \brief Send back the time in Second since 1970 */
    double GetLatitude();				/**< \brief Send back the raw latitude data of the event sent by the GPS */
    double GetPreciseLatitude();		/**< \brief Send back the latitude data of the event using the format : DD.HHXXXX */
    unsigned int GetLatitudeDeg();		/**< \brief Send back the degrees of the latitude of the event using the format : DD */
    unsigned int GetLatitudeMin();		/**< \brief Send back the minutes of the latitude of the event using the format : MM */
    unsigned int GetLatitudeSec(); 		/**< \brief Send back the seconds of the latitude of the event using the format : SS */
    unsigned int GetLatitudeHSec();		/**< \brief Send back the hundredths of a seconds of the latitude of the event using the format : HH */

    double GetLongitude();				/**< \brief Send back the raw longitude data of the event sent by the GPS */
    double GetPreciseLongitude();		/**< \brief Send back the longitude data of the event using the format : DD.HHXXXX */
    unsigned int GetLongitudeDeg();		/**< \brief Send back the degrees of the longitude of the event using the format : DD */
    unsigned int GetLongitudeMin();		/**< \brief Send back the minutes of the longitude of the event using the format : MM */
    unsigned int GetLongitudeSec(); 	/**< \brief Send back the seconds of the longitude of the event using the format : SS */
    unsigned int GetLongitudeHSec();	/**< \brief Send back the hundredths of a seconds of the longitude of the event using the format : HH */

    unsigned int GetNbSat();			/**< \brief Send back the number of satellites used to obtain the other inforamtions about the event */

    GpsData getGpsData(void);			/**< \brief Send back the raw GpsData object */

   private:
    int m_fd;
 GpsData m_oCurrentGeoDatationData;	/**< \brief Date (GeoDatationData). */

};

#endif
