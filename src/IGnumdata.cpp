#include "IGnumdata.h"

IGnumData::IGnumData()
{
    m_s = 0;
    m_ns = 0;
    m_Default = false;
}

QList<bool> IGnumData::dataToQList()
{
    QList<bool> dataReturn;
    quint16 dataTemp = m_data;
    for(int i = 0; i<8; i++){
        dataReturn.push_back(dataTemp & 0x01);
        dataTemp = dataTemp >> 1;
    }
    return dataReturn;
}
