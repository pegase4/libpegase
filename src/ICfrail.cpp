#include "ICfrail.h"

ICfRail::ICfRail()
{

}

int ICfRail::setChannelTx(int channelTx)
{
    int temp = 0x00000003;
    if(channelTx != 0){
        temp = temp << (channelTx-1)*2;
    }else{
        temp = 0;
    }
    return ICfhf8ch::setChannelTx(temp);
}

int ICfRail::setGainRx(int channel, Gain1_ad8253 gain1, Gain2_ltc6912 gain2)
{
    int ret = 0;
    ret = setGainRx(channel,gain1 | gain2);
    return ret;
}

int ICfRail::setGainRx(Gain1_ad8253 gain1, Gain2_ltc6912 gain2)
{
    int ret;
    for(int i =1; i < 9;i++){
        ret= setGainRx(i, gain1,gain2);
        if(ret < 0){
            break;
        }
    }
    return ret;
}

int ICfRail::setGainRx(int channel, quint8 gain)
{
    return ICfhf8ch::setGainRx(channel,gain);
}

int ICfRail::setGainRx(quint8 gain)
{
    int ret;
    for(int i =1; i < 9;i++){
        ret= setGainRx(i,gain);
        if(ret < 0){
            break;
        }
    }
    return ret;
}

int ICfRail::setTensionHt(int volt)
{
    int nbPasDac = 0;
    bool newHT;
    QProcess scriptPy;
    //HT a 0
    newHT = isNewHT(volt);
    if(newHT){
        scriptPy.start("pegaseNxp-hf", QStringList() << "-t"<<QString::number(0));
        scriptPy.waitForFinished();
    }
    //BT EN
    setBT(true);
    //HT EN
    setHT(true);
    //HT a volt
    if(newHT){
        nbPasDac = volt*600/20.1;
        scriptPy.start("pegaseNxp-hf", QStringList() << "-t"<<QString::number(nbPasDac));
        scriptPy.waitForFinished();
    }
    return true;
}

bool ICfRail::isNewHT(int voltHT)
{
    QString fileName = "/tmp/lastHT.conf";
    QFile fichier(fileName);
    fichier.open(QIODevice::ReadOnly | QIODevice::Text);
    QTextStream flux(&fichier);

    QString tout = flux.readAll();
    if(voltHT != tout.toInt()){
        std::cout <<"----------change HT---------"<<std::endl;
        fichier.close();
        QString path = "/tmp/lastHT.conf";
        QFile file(path);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return true;

        QTextStream out(&file);
        out<<voltHT;
        return true;
    }else{
        std::cout <<"----------pas de changement de HT---------"<<std::endl;
        return false;
    }


}
