#ifndef ICFG8CHREADTRIG_H
#define ICFG8CHREADTRIG_H
#include "IEvent.h"
#include <ifsttar_g8ch.h>
#include <unistd.h>

class ICfg8chReadTrig:  public QThread
{
    Q_OBJECT
public:
    ICfg8chReadTrig(int fd);
    ~ICfg8chReadTrig();
private :
    int  m_fd;
    bool m_running;
    int  m_statusTrigOff = 0xffff;
signals:
    void dataAvaible(ReadTrigChanel_t* data,int nbdata, IEvent *event);
    void Error_trig_off(int statusTrigOff, IEvent *event);

private:
    void run ();
};

#endif // ICFG8CHREADTRIG_H
