#include "IReadhfcan.h"


#include <iostream>

Q_DECLARE_METATYPE(ConfhfCan_t);

using namespace std;

IReadhfcan::IReadhfcan(int fd, int maxDataRead)
{
    m_fd = fd;
    m_running = true;
    _maxDataRead = maxDataRead*8;
    qRegisterMetaType<_ConfhfCan>("ConfhfCan_t>");
    cout << "read _maxDataRead = "<<_maxDataRead<<endl;

}

IReadhfcan::~IReadhfcan()
{
    m_running = false;
}

void IReadhfcan::run()
{
    int ret,nbRead;
    struct pollfd PollFd;
    PollFd.fd = m_fd;
    PollFd.events = POLLIN | POLLPRI;
    IEvent *newEvent;
    uint16_t * channelReceive;
    int nbDataAll,nbDataToRead,nbDataRestToRead;
    ConfhfCan_t confData;
    int DataToEmit,nbDataRestToTransfer;
    int partOfTransfer;
    do {
        ret = poll (&PollFd, 1, 500);
        if (ret > 0) {
            newEvent = new IEvent();
            partOfTransfer = 0;
            ioctl(m_fd,GET_NB_DATA_READ,&nbDataRestToTransfer);

            while(nbDataRestToTransfer != 0){
                if(nbDataRestToTransfer > MAX_DATA_EMIT){
                    nbDataAll = MAX_DATA_EMIT;
                }else{
                    nbDataAll = nbDataRestToTransfer;
                }

                cout << "nbDataRestToTransfer = "<< nbDataRestToTransfer<<" and nbDataAll = " << nbDataAll<< endl;
                channelReceive = (uint16_t *) malloc(nbDataAll * sizeof(uint16_t));
                nbDataRestToRead = nbDataAll;
                //cout << "read nbDataAll = "<<nbDataAll<<endl;
                //cout << "read _maxDataRead = "<<_maxDataRead<<endl;
                nbRead = 0;
                while(nbDataRestToRead != 0){
                    if(nbDataRestToRead<_maxDataRead){
                        nbDataToRead = nbDataRestToRead;
                    }else{
                        nbDataToRead = _maxDataRead;
                    }
                 //   cout << "read nbDataToRead = "<<nbDataToRead<<endl;

                    ret = read (m_fd,(char *) channelReceive + nbRead*(_maxDataRead* sizeof(uint16_t)), nbDataToRead* sizeof(uint16_t));
                    nbDataRestToRead -= nbDataToRead;
                    nbRead++;
                  //  cout << "nbRead = "<<nbRead <<" rest = "<<nbDataRestToRead<<endl;
                }
                if(ret == 0){
                    ioctl(m_fd,GET_CONF_DATA_READ,&confData);
                    nbDataRestToTransfer =nbDataRestToTransfer-nbDataAll;
                    partOfTransfer ++;
                    if(nbDataRestToTransfer ==0){
                        cout<<"read read"<<endl;
                        emit dataAvaible(channelReceive,nbDataAll,partOfTransfer,true,confData,newEvent);
                    }else{
                        emit dataAvaible(channelReceive,nbDataAll,partOfTransfer,false,confData,newEvent);
                    }
                }else{
                    cout << "erreur read = "<<ret<<endl;
                }
                free(channelReceive);
                cout << "next  partOfTransfer = "<<partOfTransfer<<endl;

            }
            delete(newEvent);
        }
    }while(m_running);
}
