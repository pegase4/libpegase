#include "IPps.h"

IPps::IPps()
{

}

int IPps::init()
{
    _fd = open("/dev/pegase-sync-drv", O_RDWR);
    if(_fd < 0){
        qDebug() << "fail to open /dev/pegase-sync-drv";
        return -1;
    }else{
        m_running = true;
        return 0;
    }
}
IPps::~IPps()
{
    m_running = false;
}
void IPps::run()
{
    int returnRead,ret;
    IEvent *newEvent;
    while(m_running){
            ret = read (_fd, &returnRead, sizeof(returnRead));
            //qDebug() << "read " <<ret<< " : " << returnRead ;
            if(ret >= 0){
                newEvent = new IEvent();
                if(returnRead == 1){
                    emit ppsRising(newEvent);
                }else if(returnRead == 0){
                    emit ppsFalse(newEvent);
                }
                delete(newEvent);
            }
    };
}
