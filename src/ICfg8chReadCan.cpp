#include "ICfg8chReadCan.h"

ICfg8chReadCan::ICfg8chReadCan(int fd, int count, int nbChannel)
{
    m_fd = fd;
    m_running = true;
    m_count = count;
    m_nbChannel = nbChannel;
}

ICfg8chReadCan::~ICfg8chReadCan()
{
    m_running = false;
}

void ICfg8chReadCan::run()
{
    int ret;
    struct pollfd PollFd;
    PollFd.fd = m_fd;
    PollFd.events = POLLIN | POLLPRI;
    quint32 nbOverwrite,compteur;
    quint64 seconde;
    IEvent *newEvent;
    uint32_t * channelReceive;
    char *bufferReceive = NULL;
    bufferReceive = (char *) malloc(m_nbChannel* m_count * sizeof(uint32_t));
    if(bufferReceive == NULL){
    }
    channelReceive = (uint32_t *) malloc(m_nbChannel *m_count * sizeof(uint32_t));
    if(channelReceive == NULL){
    }
    do {
        ret = poll (&PollFd, 1, 500);
        if (ret > 0) {
            ret = read (m_fd, bufferReceive, m_count);
            newEvent = new IEvent();
            channelReceive = (uint32_t *) memcpy(static_cast<void *>(channelReceive), static_cast<void *>(bufferReceive), m_nbChannel * sizeof(uint32_t) * m_count);
            ret = ioctl(m_fd, CF8CH_GET_NB_ERR_OF, &nbOverwrite);
            if (ret < 0) {
            }else if(nbOverwrite > 0 ){
                emit Error_overwrite_dma(nbOverwrite,newEvent);
            }
            ret = ioctl(m_fd, CF8CH_GET_TIME_ACQUISITION_TS,  &seconde);
            ret = ioctl(m_fd, CF8CH_GET_TIME_ACQUISITION_MICRO,  &compteur);
            emit dataAvaible(channelReceive,m_count,m_nbChannel,seconde,compteur,newEvent);
            delete(newEvent);
        }
    }while(m_running);
    free(bufferReceive);
    free(channelReceive);
}
