#ifndef ICF8CH_H
#define ICF8CH_H

#include <QObject>
#include <IStdout.h>
#include <unistd.h>
#include <fcntl.h>
#include <ifsttar_g8ch.h>
#include <sys/ioctl.h>
#include <qtconcurrentrun.h>
#include <poll.h>
#include <QBitArray>
#include <QQueue>
#include <IAnalogData.h>
#include <IGnumdata.h>
#include <IEvent.h>
#include <bitset>
#include <sys/time.h>
#include <iostream>
#include "ICfg8chReadCan.h"
#include "ICfg8chReadTrig.h"
#include "ICfg8chReadPps.h"
#include <QThread>
#include <stdint.h>
#include <QDir>
#include <QProcess>


#define ICfg8ch_NAME                            "ICfg8ch "
#define CAN_FREQ_MAX_IT                         20
#define CAN_FREQ_MIN                            99
#define CAN_FREQ_MAX                            34000
#define CAN_COUNT_MAX                           150000
#define TRIG_TIMEOUT_MAX                        2000
#define TRIG_NB_TRIG_SECURITY                   5000
#define NB_SECONDE_PPS_INFO                     180
#define PLL_THEORIQUE                           240000000

// Etat du process de gestion du CAN
#define STATUS_NOT_DEFINE       				0x0000
#define STATUS_TEST_MODE        				0x4001
#define STATUS_IDLE             				0x4002
#define STATUS_INIT             				0x4004
#define STATUS_RUNNING          				0x4008
#define STATUS_SYNCHRONISING   					0x4010
#define STATUS_ERROR           					0x8000
#define STATUS_ERROR_TEST_MODE  				0x8002

// Mode d'acquisition du CAN ADS1278
#define CAN_MODE_HIGH_SPEED						0		// HIGH SPEED CAN mode selected
#define CAN_MODE_HIGH_RESOLUTION				1		// HIGH RESOLUTION CAN mode selected
#define CAN_MODE_LOW_POWER						2		// LOW POWER CAN mode selected
#define CAN_MODE_LOW_SPEED						3		// LOW_SPEED CAN mode selected

#define MULT_FE_CAN_HIGH_SPEED                  256.0	// Fclk/Fdata for CAN ADS1278 in HIGH SPEED mode (CLKDIV = 1)
#define MULT_FE_CAN_HIGH_RESOLUTION             512.0	// Fclk/Fdata for CAN ADS1278 in HIGH RESOLUTION mode (CLKDIV = 1)
#define MULT_FE_CAN_LOW_POWER_1		            512.0	// Fclk/Fdata for CAN ADS1278 in LOW_POWER mode (CLKDIV = 1)
#define MULT_FE_CAN_LOW_POWER_0		            256.0	// Fclk/Fdata for CAN ADS1278 in LOW_POWER mode (CLKDIV = 0), non utilisé : CLKDIV = 1 au niveau du FPGA pour piloter le CAN ADS1278 
#define MULT_FE_CAN_LOW_SPEED_1		            2560.0	// Fclk/Fdata for CAN ADS1278 in LOW_SPEED mode (CLKDIV = 1)
#define MULT_FE_CAN_LOW_SPEED_0		            512.0	// Fclk/Fdata for CAN ADS1278 in LOW_SPEED mode (CLKDIV = 0), non utilisé : CLKDIV = 1 au niveau du FPGA pour piloter le CAN ADS1278 

#define MULT_CAN_HIGH_SPEED						32		// Multiplicateur utiliser dans le FPGA pour générer "can_clk" à partir de "tp2_clk" en mode HIGH_SPEED
#define MULT_CAN_HIGH_RESOLUTION				32		// Multiplicateur utiliser dans le FPGA pour générer "can_clk" à partir de "tp2_clk" en mode HIGH_RESOLUTION
#define MULT_CAN_LOW_POWER						32		// Multiplicateur utiliser dans le FPGA pour générer "can_clk" à partir de "tp2_clk" en mode LOW_POWER (CLKDIV = 0 ou 1)
#define MULT_CAN_LOW_SPEED						32		// Multiplicateur utiliser dans le FPGA pour générer "can_clk" à partir de "tp2_clk" en mode LOW_SPEED (CLKDIV = 0 ou 1)

#define DUTY_CYCLE_CAN_CLK						50		// Rapport cyclique de l'horloge "can_clk"

// GPS - Timepulses
#define TIMEPULSE1                              1		// Définition du numéro d'identification du Timepulse 1
#define TIMEPULSE2                              2		// Définition du numéro d'identification du Timepulse 2
#define DELAYTIMEPULSE2                         -100	// Définition d'un delai de retard de 100ns pour le Timepulse 2 par rapport au pps
#define TOL_ERROR_TIMEPULSE2_GPS_NON_VALID      0.001	// Lorsque la réception GPS n'est pas valide, le retard du TIMEPULSE2 demandé par le script (Ex : "pegase-confpps.py 2 -f 1024000 -d -100") pour le timepulse2 n'est pas pris en compte par la puce NEO-M8T. Une petite tolérance sur la fréquence du TIMEPULSE2 est acceptée en %
#define NB_TRY_MAX_FCAN_CLK                     5		// Nombre de tentative de vérification de la fréquence de référence pour l'acquisition analogique (512 x Fech : pour le mode High Definition du CAN ADS1278)




using namespace std;


    class ICfg8ch : public QThread
    {
        Q_OBJECT

public:
    enum stateCan{CAN_NOT_OPENED,CAN_CONFIGURATED,CAN_OPENED,CAN_STARTED,CAN_STOPED};
    enum stateTrig{TRIG_NOT_OPENED,TRIG_CONFIGURATED,TRIG_OPENED,TRIG_STARTED,TRIG_STOPED};
    enum statePps{PPS_NOT_OPENED,PPS_OPENED,PPS_STARTED,PPS_STOPED};

    struct confCan{
        quint16 mode = 0;
        quint32 frequency = 0;
        quint32 count = 0;
        quint8 channel = 0;
        bool withGPS = true;
    };
    struct confTrig{
        quint8 channel = 0;
        quint16 timeout = 0;
        quint16 nbTrigSecurity = 300;
    };
    struct dateData{
        quint64 seconde = 0;
        quint32 nanoSeconde = 0;
        bool valeurReel = false;
        ppsInfos pps;
    };
    //explicit ICf8ch(QObject *parent = 0);

    explicit ICfg8ch();
    ~ICfg8ch();

    int Init();

    int SetCanConfig(confCan newConfCan);
    int SetTrigConfig(confTrig newConfTrig);
    int OpenCan();
    int OpenTrig();

    int CloseCan();
    int CloseTrig();

    int StartCan();
    int StartTrig();

    int StopCan();
    int StopTrig();

    QString showConfig();
    quint8 getStatusTrigOff();
    int resetStatusTrigOff();

    int GetRealFE(float &fFe);

    float GetFpgaVersion();

    QQueue<ppsInfos> ppsInfosQueue() const;

    private :
    int modprobeDriver();

    //CAN
    int SetFeCanToFPGA();
    int SetCountCanToFPGA();
    int SetChannelCanToFPGA();
    int SetModeCanToFPGA();

    //TRIG
    int SetTimeOutTrigToFPGA();
    int SetChannelTrigToFPGA();
    int SetNbTrigOffToFPGA();


    //PPS
    int OpenPps();
    int ClosePps();
    int StartPps();

    dateData calcDateData(quint64 seconde, quint32 cpt);

private slots:
    void ReadPps(ppsInfos newPpsInfo, IEvent *event);
    void ReadCan(uint32_t *dataList,int count, int nbChannel,quint64 seconde, quint32 compteur,IEvent *event);
    void ReadTrig(ReadTrigChanel_t* data,int nbdata, IEvent *event);
    void Error_trig_off_slot(int statusTrigOff, IEvent *event);
    void Error_overwrite_dma_slot(int nbOverwrite, IEvent *event);
signals:
    void Error_buff_overflow(quint32 nbOverflow, IEvent *event);
    void Data_Can(QVector<IAnalogData> listData, IEvent *event);
    void Error_trig_off(int statusTrigOff, IEvent *event);
    void Error_overwrite(int nbOverwrite, IEvent *event);
    void Data_Trig(QVector<IGnumData>, IEvent *event);
public slots:

private :
    //CLASS
    bool m_running = false;
    //CAN
    stateCan m_stateCan = CAN_NOT_OPENED;
    ICfg8chReadCan *readCanThread;
    confCan  m_confCan;
    int      m_canFd = -1;
    int m_nbCanChannel;


    //TRIG
    stateTrig m_stateTrig = TRIG_NOT_OPENED;
    ICfg8chReadTrig *readTrigThread;
    confTrig m_confTrig;
    int      m_trigFd = -1;
    int      m_statusTrigOff = 0xffff;


    //PPS
    int      m_ppsFd = -1;
    statePps m_statePps = PPS_NOT_OPENED;
    ICfg8chReadPps *readPpsThread;
    QQueue<ppsInfos> m_ppsInfosQueue;
    QMutex m_ppsInfosMutex;

};



#endif // ICF8CH_H
