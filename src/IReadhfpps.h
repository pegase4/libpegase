#ifndef IREADHFPPS_H
#define IREADHFPPS_H
#include <unistd.h>
#include "IEvent.h"
#include <ifsttar_hf8ch.h>


class IReadhfpps: public QThread
{
    Q_OBJECT
public:
    IReadhfpps(int fd);
    ~IReadhfpps();
private :
    int m_fd;
    bool m_running;
signals:
    void dataAvaible(ppsInfos_t newPpsInfo, IEvent *event);
private:
    void run ();
};
#endif // IREADHFPPS_H
