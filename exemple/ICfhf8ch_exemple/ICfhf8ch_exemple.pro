#
#   Qt modules
#
QT += core
QT += network
QT -= gui
QT += concurrent


#
#   Project configuration and compiler options
#
CONFIG += c++14
CONFIG += c++11
CONFIG += warn_on

#
#   Output config
#
TARGET = hf8ch_exemple
target.path = $$[QT_INSTALL_BINS]
INSTALLS += target
#
#   libPegase config
#
DEPENDPATH += . ../../src
INCLUDEPATH += ../../src
LIBS += -L../../src -lpegase

#
#   Sources
#
SOURCES += \
    cfhf8ch_exemple.cpp\
    cfhf8ch_exemple_main.cpp

HEADERS += \
        cfhf8ch_exemple.h \
        conf.h
   


