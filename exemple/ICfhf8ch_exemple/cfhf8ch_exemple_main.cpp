#include <QCoreApplication>
#include <QObject>
#include <QFile>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include "conf.h"
#include <unistd.h>
#include "cfhf8ch_exemple.h"

int main(int argc, char *argv[])
{
    /*
     *  Init Qt console app
     */
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("ICfhf8chExemple");
    QCoreApplication::setApplicationVersion("1.1.0");

    bool ok;
    ConfApp_t conf;
    QCommandLineParser parser;
    parser.setApplicationDescription("Test helper");
    parser.addHelpOption();
    parser.addVersionOption();
    // nbrx
    QCommandLineOption nbRx(QStringList()      << "nbRx", "nbRx <u> default 50000.", "nbRx");
    nbRx.setDefaultValue("50000");
    parser.addOption(nbRx);
    // nbrx
    QCommandLineOption seuil(QStringList()      << "seuil", "seuil <u> default 16384.", "seuil");
    seuil.setDefaultValue("16384");
    parser.addOption(seuil);

    QCommandLineOption moyenne(QStringList()      << "moyenne", "moyenne <u> default 0.", "moyenne");
    moyenne.setDefaultValue("0");
    parser.addOption(moyenne);

    QCommandLineOption mode(QStringList()      << "mode", "mode  ( soft=0 / seui = 1 / trig in = 2 / date = 3 ) default 0", "mode");
    parser.addOption(mode);
    mode.setDefaultValue("0");

    QCommandLineOption dateToTrig(QStringList()      << "dateToTrig", "date epoch de tir needeed by mode Date", "dateToTrig");
    dateToTrig.setDefaultValue(QString(VALUE_DEFAUT_DATE));
    parser.addOption(dateToTrig);

    QCommandLineOption signal(QStringList()      << "signal", "path du signal (binaire)", "signal");
    signal.setDefaultValue(VALUE_DEFAUT_SIGNAL);
    parser.addOption(signal);

    QList<QCommandLineOption> listGain;


    for(int i = 0; i < NB_VOIE;i++){
        QCommandLineOption gain(QStringList()      << "gain"+QString::number(i+1), "gain reception default 13", "gain"+QString::number(i));
        gain.setDefaultValue("13");
        parser.addOption(gain);
        listGain.push_back(gain);
    }
    QCommandLineOption gain(QStringList()      << "gain", "gain reception default 21", "gain");
    gain.setDefaultValue("21");
    parser.addOption(gain);

    QCommandLineOption prf(QStringList()      << "prf", "nombre de tir par seconde default 5", "prf");
    prf.setDefaultValue("5");
    parser.addOption(prf);

    QCommandLineOption channelTx(QStringList()      << "channelTx", "channel d'émission default 1", "channelTx");
    channelTx.setDefaultValue("1");
    parser.addOption(channelTx);

    QCommandLineOption freqRx(QStringList()      << "freqRx", "frequense de reception defaut 2Mhz", "freqRx");
    freqRx.setDefaultValue("2000000");
    parser.addOption(freqRx);

    QCommandLineOption channelRx(QStringList()      << "channelRx", "mask channel rx enable defaut 255", "channelRx");
    channelRx.setDefaultValue("255");
    parser.addOption(channelRx);

    QCommandLineOption seuilChannelDetection(QStringList()      << "seuilChannelDetection", "mask channel seuil detection enable defaut 255", "seuilChannelDetection");
    seuilChannelDetection.setDefaultValue("255");
    parser.addOption(seuilChannelDetection);

    QCommandLineOption file(QStringList()      << "file", "path du file generated (binaire) defaut : /lcpc/sd_stockage/", "file");
    file.setDefaultValue(VALUE_DEFAUT_FILE);
    parser.addOption(file);

    QCommandLineOption prefix(QStringList()      << "prefix", "prefix of the generated file  defaut : pegase", "prefix");
    prefix.setDefaultValue(VALUE_DEFAUT_PREFIX);
    parser.addOption(prefix);

    parser.process(app);

    conf.nbRx =  parser.value(nbRx).toUInt(&ok, 10);
    conf.seuil = parser.value(seuil).toInt(&ok, 10);
    conf.mode = parser.value(mode).toInt(&ok, 10);
    conf.moyenne = parser.value(moyenne).toInt(&ok, 10);
    for(int i = 0; i < NB_VOIE;i++){
        conf.gain[i] = parser.value(listGain.at(i)).toInt(&ok, 16);
    }
    conf.prf = parser.value(prf).toInt(&ok, 10);
    conf.channelTx = parser.value(channelTx).toInt(&ok, 10);
    conf.dateToTtrig = parser.value(dateToTrig).toUInt(&ok, 10);
    conf.freqRx = parser.value(freqRx).toInt(&ok, 10);
    conf.channelRx = parser.value(channelRx).toInt(&ok, 10);
    conf.seuilChannelDetection = parser.value(seuilChannelDetection).toInt(&ok, 10);
    conf.pathSignal = QString::fromLocal8Bit(parser.value(signal).toStdString().c_str());
    conf.pathFile = QString::fromLocal8Bit(parser.value(file).toStdString().c_str());
    conf.prefix = QString::fromLocal8Bit(parser.value(prefix).toStdString().c_str());
    Cfhf8chExemple* can  =new Cfhf8chExemple(conf);
    QObject::connect(can,SIGNAL(endPrint()),&app,SLOT(quit()));

    can->start();

    app.exec();
    return 0;
}
