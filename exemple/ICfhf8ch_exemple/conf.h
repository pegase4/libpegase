#ifndef CONF_H
#define CONF_H
#include <iostream>
#include <QStringList>

#define NB_VOIE  8
#define VALUE_DEFAUT_DATE  123456
#define VALUE_DEFAUT_SIGNAL  "none"
#define VALUE_DEFAUT_FILE  "/lcpc/sd_stockage/"
#define VALUE_DEFAUT_PREFIX  "pegase"

enum mode_tir {SOFT=0,SEUIL,TRIG_IN,DATE};
typedef struct _ConfApp ConfApp_t;
struct _ConfApp {
    int nbRx = 0;
    bool isCsv=0;
    int seuil=16384;
    int seuilChannelDetection=255;
    int mode = SEUIL;
    int moyenne = 0;
    QString pathSignal;
    QString pathFile;
    QString prefix;
    quint64 dateToTtrig;
    int gain[NB_VOIE];
    int prf;
    int channelTx;
    int freqRx;
    int channelRx;
    QStringList modeList= { "soft" , "seuil" , "trig in" , "date"};
    void afficher(std::ostream &flux) const
    {
        flux << "conf de l'acquisition : "<<std::endl;
        flux <<"\t nbRx = " <<nbRx<<std::endl;
        flux <<"\t mode = " << modeList.at(mode).toStdString()<<std::endl;
        flux <<"\t moyenne = " << moyenne<<std::endl;
        for(int i = 0; i < NB_VOIE;i++){
            flux <<"\t gain"<<i+1<<" = " <<std::hex << int(gain[i])<<std::endl;
        }
        flux <<std::dec;
        flux <<"\t prf = " << prf<<std::endl;
        flux <<"\t channelTx = " << channelTx<<std::endl;
        flux <<"\t freqRx = " << freqRx<<std::endl;
        flux <<"\t channelRx = " << channelRx<<std::endl;
        flux <<"\t path to pathFile  = " <<pathFile.toLocal8Bit().data()<<std::endl;
        flux <<"\t prefix  = " <<prefix.toLocal8Bit().data()<<std::endl;

        if(mode==DATE){
            flux <<"conf specifique mode date"<<std::endl;
            flux <<"\t date to trig = " <<dateToTtrig<<std::endl;
        }
        if(mode==SEUIL){
            flux <<"conf specifique mode seuil"<<std::endl;
            flux <<"\t seuil = " <<seuil<<std::endl;
            flux <<"\t seuilChannelDetection = " <<seuilChannelDetection<<std::endl;
        }
        if(pathSignal != VALUE_DEFAUT_SIGNAL){
            flux <<"\t path to signal  = " <<pathSignal.toLocal8Bit().data()<<std::endl;
        }
    }
    friend std::ostream& operator<<( std::ostream &flux, ConfApp_t const& conf )
    {
        conf.afficher(flux);
        return flux;
    }

};

#endif // CONF_H
