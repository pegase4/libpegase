#include <QMetaEnum>
#include <QString>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <sys/stat.h>
#include <fcntl.h>
#include <QThread>
#include <unistd.h>
#include "cfhf8ch_exemple.h"
#include <QDataStream>
#include <iostream>
#include "tdmswriter/tdmswriter.h"

using namespace std;

Cfhf8chExemple::Cfhf8chExemple(ConfApp_t conf)
{
    m_cfhf8ch = new ICfbeton();
    _conf = conf;
    cout << conf <<endl;

}

Cfhf8chExemple::~Cfhf8chExemple()
{
    delete(m_cfhf8ch);

}

int Cfhf8chExemple::start()
{
    int ret;
    ret = m_cfhf8ch->init(true);
    if(ret<0){
        cout <<"erreur m_cfhf8ch init"<<endl;
        return -1;
    }
    ret = m_cfhf8ch->start();
    if(ret<0){
        cout <<"erreur m_cfhf8ch start"<<endl;
        return -1;
    }
    //conecte le signal de lecture des données au slot d'écriture
    connect(m_cfhf8ch,SIGNAL(dataAvaible(Signal_t,IEvent*)),this,SLOT(readData(Signal_t,IEvent*)), Qt::DirectConnection);
    //permet de paramétrer le nombre de tire par acquisition (0 = un seul tir)
    m_cfhf8ch->setNbAvg(_conf.moyenne);
    //nombre de tir par secondes
    m_cfhf8ch->setPrf_hz(_conf.prf);
   // m_cfhf8ch->setSyncPps(200);
    // nombre d'échantillion acquis
    m_cfhf8ch->setNbRxSample(_conf.nbRx);
    //retard du tire (en mode date)
  //  m_cfhf8ch->setDigitDelay_dns(0);
    //quelle voie va émetre (mask)
    m_cfhf8ch->setChannelTx(_conf.channelTx);
    //retard de l'acquisition par rapport au tir
   // m_cfhf8ch->setAcqDelay_dns(0);
    _reelfrq = m_cfhf8ch->setfreqRx(_conf.freqRx);
    cout << "frequece reel = " << _reelfrq <<endl;
    m_cfhf8ch->setChannelRx(_conf.channelRx);
    //gain au niveau des voies d'acquissition
   // m_cfhf8ch->setGainRx(_conf.gain);
    for(int i = 0; i < NB_VOIE;i++){
        m_cfhf8ch->setGainRx(i+1,_conf.gain[i]);
    }
    //à mettre à 0, fonctionne dans un autre mode
 //   m_cfhf8ch->setSyncPps(0);
    //sauve la config avant une aquisition
    m_cfhf8ch->saveConfig();
    if(_conf.pathSignal != VALUE_DEFAUT_SIGNAL && _conf.pathSignal !=""){
        //set un signal d'émission
        setWaveform(_conf.pathSignal);
    }
    if(_conf.mode == SEUIL){
        //paramètre le seuil pour déclancher k'acquisition
        m_cfhf8ch->setSeuil(_conf.seuil,-_conf.seuil);
        //nombre d'échantillion acqui avant le dépassement du seuil
        m_cfhf8ch->setpreTrig(8000);
        //active l'acquisition et sur quelles voies on regarde le dépassement de seuil (mask)
        m_cfhf8ch->startTrigSeuilSoft(_conf.seuilChannelDetection);
    }else if (_conf.mode == TRIG_IN){
       // m_cfhf8ch->startTrigExterne(0,0);
    }else if(_conf.mode == DATE){
        if(_conf.dateToTtrig==VALUE_DEFAUT_DATE){
            std::cout<< "you need to get a date on this mode"<<std::endl;
            emit endPrint();
            return -1;
        }
        quint64 dateFPGA = m_cfhf8ch->getDateFPGA();
        if(dateFPGA >= _conf.dateToTtrig){
            cout <<"Date anterieur : date FPGA =  "<<dateFPGA<< " date trig = "<<_conf.dateToTtrig<<endl;
        }else{
            cout <<"Trig dans "<<_conf.dateToTtrig<<"-"<<dateFPGA<<" = "<<_conf.dateToTtrig-dateFPGA<<endl;
            m_cfhf8ch->startTrigDate(_conf.dateToTtrig);
        }
    }else if(_conf.mode==SOFT){
        //lance une acquisition
        m_cfhf8ch->startTrigSoft();
    }
    return ret;
}

void Cfhf8chExemple::setWaveform(QString path)
{
    QFile fileRead(path);
    if (!fileRead.open(QIODevice::ReadOnly)){
        cout << "impossible d'ouvrire le fichier " << path.toStdString();
        return;
    }
    QDataStream in(&fileRead);    // read the data serialized from the file
    QList<quint16> signalTx;
    quint16 a;
    int i = 0;
    while(!in.atEnd()){
       in >> a;//signal[i];
       signalTx.push_back(a);
        i++;
    }
    m_cfhf8ch->setSignalTx(signalTx,0);
}

void Cfhf8chExemple::readData(Signal_t signal, IEvent *event)
{
    quint64 seconde = event->GetTimeInSecondsEpoch();
    QString path = _conf.pathFile+_conf.prefix+"-hf8ch_exemple_"+QString::number(seconde)+"_"+QString::number(signal.partOfTransfer)+".bin";
     QFile file(path);
     if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    std::cout <<"binaire : "<<std::endl;
    QDataStream out(&file);   // we will serialize the data into the file
    int nbVoie = 0;
    int maskChannel_temp = signal.activateChannelRx;
    cout <<" freq signal printed : " << signal.freqencyRx<<endl;
    while (maskChannel_temp) {
                nbVoie += maskChannel_temp & 1;
                maskChannel_temp >>= 1;
                }
    out<<quint8(_conf.mode)<<int(signal.nbDataRx)<<int(signal.activateChannelRx)<<int(nbVoie)<<int(_conf.moyenne);
    for(int i = 0; i < NB_VOIE;i++){
        out <<quint8(_conf.gain[i]);
    }
    for(int data = 0; data < signal.nbDataRx/signal.nbChanneRx; data++){
        for(int channel = 0; channel < signal.nbChanneRx;channel++){
            out<<quint16(signal.dataListbrut[data*signal.nbChanneRx+channel]);
        }
    }
    file.close();
    std::cout <<"file : "<<signal.partOfTransfer<<std::endl;
    if(signal.endRead){
        emit endPrint();
    }
}

void Cfhf8chExemple::readDataTdms(Signal_t signal, IEvent *event)
{
    struct timespec tt;
    //_mTdms = new TDMS::TdmsWriter();
    static TDMS::TdmsWriter test;
    tt.tv_nsec = 0;
    tt.tv_sec = event->GetTimeInSecondsEpoch();
    QString path = _conf.pathFile+_conf.prefix+"-hf8ch_exemple_"+QString::number(tt.tv_sec)+"_"+QString::number(signal.partOfTransfer)+".tdms";
    cout <<"partOfTransfer = "<<signal.partOfTransfer<<" endRead = "<< signal.endRead<<endl;
    if(signal.partOfTransfer == 1){
        cout <<"create file" <<endl;                

        test.createfile(path.toStdString(),_conf.prefix.toStdString(),tt,(TDMS::mode_tir)_conf.mode,_conf.moyenne);
        test.write_waveforms((char *)signal.dataListbrut,signal.nbDataRx*sizeof(quint16),_reelfrq,_conf.channelRx,_conf.gain[0],_conf.gain[1],_conf.gain[2],_conf.gain[3],_conf.gain[4],_conf.gain[5],_conf.gain[6],_conf.gain[7]);
    }else{
        test.append_waveforms((char *)signal.dataListbrut,signal.nbDataRx*sizeof(quint16));
        cout <<"append file : "<<test.get_path()<< " "<<test.is_ready() <<endl;
    }
    if(signal.endRead){
        cout <<"close file" <<endl;
        test.close();
        emit endPrint();
    }

}
