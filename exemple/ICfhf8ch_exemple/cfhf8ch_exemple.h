#ifndef CFHF8CH_EXEMPLE_H
#define CFHF8CH_EXEMPLE_H

#include <sys/ioctl.h>
#include <QString>
#include <QObject>
#include "ICfbeton.h"
#include "conf.h"

class Cfhf8chExemple : public QObject
{
    Q_OBJECT

public:
    Cfhf8chExemple(ConfApp_t conf);
    ~Cfhf8chExemple();

    int start();
    ICfbeton* m_cfhf8ch;
    void setWaveform(QString path);
public slots:
    void readData(Signal_t signal,IEvent* event);
    void readDataTdms(Signal_t signal,IEvent* event);

signals :
    void endPrint();
private:
    ConfApp_t _conf;
    int _reelfrq;
    //TDMS::TdmsWriter *_mTdms;
};

#endif
