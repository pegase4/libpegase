#include <QMetaEnum>
#include <QString>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <sys/stat.h>
#include <fcntl.h>
#include <QThread>
#include <unistd.h>
#include "cfg8ch_exemple.h"
#include <iostream>
#include <iomanip>
#include <iostream>
#define UNUSED(x) (void)(x)

using namespace std;

Cfg8chExemple::Cfg8chExemple()
{

}

Cfg8chExemple::~Cfg8chExemple()
{

}

void Cfg8chExemple::start(quint32 freq, quint32 count, quint8 channel, quint16 mode, bool canOk, bool trigOk)
{

    ICfg8ch::confCan myConfCan;
    ICfg8ch::confTrig myConfTrig;
    int retOpenCan = 0, retOpenTrig = 0, retConfCan = 0, retConfTrig = 0;


    m_icfg8ch = new ICfg8ch();
    if(m_icfg8ch->Init() < 0){
        //IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Error PPS"<< Qt::endl;
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ERROR init ICfg8ch"<< Qt::endl;
        return;
    }

    IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : FPGA version : "<< QString::number(m_icfg8ch->GetFpgaVersion(), 'f', 2) << Qt::endl;
    

    if(canOk){
        retOpenCan = m_icfg8ch->OpenCan();
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : OpenCan done" << Qt::endl;
    }else{
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : OpenCan failed => not activated" << Qt::endl;
    }

    if(trigOk){
        retOpenTrig = m_icfg8ch->OpenTrig();
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : OpenTrig done" << Qt::endl;
    }else{
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : OpenTrig failed => not activated" << Qt::endl;
    }


    myConfCan.frequency = freq;
    myConfCan.count = count;
    myConfCan.channel = channel;
    myConfCan.mode = mode;
    IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Can configuration : freq = "<< myConfCan.frequency <<", count = "<< myConfCan.count <<", channel = "<< myConfCan.channel <<", mode = "<< myConfCan.mode << Qt::endl;

    myConfTrig.channel = 255;
    myConfTrig.nbTrigSecurity = 200;
    myConfTrig.timeout = 5;
    IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Trig configuration : channel = "<< myConfTrig.channel <<", nbTrigSecurity = "<< myConfTrig.nbTrigSecurity <<", timeout = "<< myConfTrig.timeout << Qt::endl;


    if(canOk && (retOpenCan == 0)){
        retConfCan = m_icfg8ch->SetCanConfig(myConfCan);
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ConfCan done" << Qt::endl;
    }else{
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ERROR ConfCan failed" << Qt::endl;
    }

    if(trigOk && (retOpenTrig == 0)){
        retConfTrig = m_icfg8ch->SetTrigConfig(myConfTrig);
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ConfTrig done" << Qt::endl;
    }else{
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ERROR ConfTrig failed" << Qt::endl;
    }


    if(canOk && (retOpenCan == 0) && (retConfCan == 0)){
        connect(m_icfg8ch,SIGNAL(Data_Can(QVector<IAnalogData>,IEvent*)),this,SLOT(slotDataCan(QVector<IAnalogData>,IEvent*)));
        connect(m_icfg8ch,SIGNAL(Error_overwrite(int,IEvent*)),this,SLOT(slotOverwriteDma(int,IEvent*)));
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Can connect done" << Qt::endl;
    }else{
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ERROR Can connect failed" << Qt::endl;
    }

    if(trigOk && (retOpenTrig == 0) && (retConfTrig == 0)){
        connect(m_icfg8ch,SIGNAL(Data_Trig(QVector<IGnumData>,IEvent*)),this,SLOT(slotDataTrig(QVector<IGnumData>,IEvent*)));
        connect(m_icfg8ch,SIGNAL(Error_trig_off(int,IEvent*)),this,SLOT(slotDataTrigOff(int,IEvent*)));
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : Trig connect done" << Qt::endl;
    }else{
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ERROR Trig connect failed" << Qt::endl;
    }


    if(canOk && (retOpenCan == 0) && (retConfCan == 0)){
        m_icfg8ch->StartCan();
  //      IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : StartCan ret = " << QString::number(retStartCan) << Qt::endl;
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : StartCan done" << Qt::endl;
    }else{
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ERROR StartCan failed" << Qt::endl;
    }    

    if(trigOk && (retOpenTrig == 0) && (retConfTrig == 0)){
        m_icfg8ch->StartTrig();
//        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : StartTrig ret = " << QString::number(retStartTrig) << Qt::endl;
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : StartTrig done" << Qt::endl;
    }else{
        IStdout() << ICfg8ch_NAME << __FUNCTION__ <<" : ERROR StartTrig failed" << Qt::endl;
    }
}

void Cfg8chExemple::slotDataTrig(QVector<IGnumData> listData, IEvent *event)
{
    (void)(event);
    if(!listData.empty()){

        for(int i = 0;i<listData.size();i++){
            IStdout()<< i<< " "<< listData.at(i).m_s<<"."<<listData.at(i).m_ns<<"."<<listData.at(i).m_data<<";";
            IStdout() <<Qt::endl;
        }
    }
    IStdout() << "end nb  = "<< listData.size()<<Qt::endl;
}

void Cfg8chExemple::slotDataTrigOff(int channelTrigOf, IEvent *event)
{
    (void)(event);

    IStdout()<<"trig off!! : "<< channelTrigOf<<Qt::endl;
    m_icfg8ch->resetStatusTrigOff();
    IStdout() << "trig off!! : "<< m_icfg8ch->getStatusTrigOff()<<Qt::endl;
}

void Cfg8chExemple::slotOverwriteDma(int nbOverwrite, IEvent *event)
{
    (void)(event);

    IStdout()<<"overwrite : "<< nbOverwrite<<Qt::endl;

}

void Cfg8chExemple::slotDataCan(QVector<IAnalogData> listData, IEvent *event)
{
    UNUSED(event);
    if(!listData.empty()){
        float_t freq;
        m_icfg8ch->GetRealFE(freq);
        IStdout() << "date;";
        for(int i = 0;i<listData.at(0).m_data.size();i++){
            cout <<i<<";";
            //
        }
         cout <<endl;
        for(int i = 0;i<listData.size();i++){
             if(i < 20 || i >listData.size() -20 ){
            // if((qAbs(listData.at(i).dataToUv().at(7)) > 1000000) && (qAbs(listData.at(i-1).dataToUv().at(7)) < 1000000)){

            cout << std::setfill(' ') << std::setw(5)<< i <<" "<<std::setfill(' ') << std::setw(10)<<listData.at(i).m_s<<"."<<std::setfill(' ') << std::setw(9)<<listData.at(i).m_ns<<";";

            for(int j = 0; j<listData.at(i).dataToUv().size();j++){

                cout <<std::setfill(' ') << std::setw(7)<<listData.at(i).dataToUv().at(j)<<";";
            }
            cout <<endl;


            }
        }
        // }
        // IStdout() <<Qt::endl;
    }

    //  IStdout()<<ret<< " : freq  = "<< freq<<Q::Qt::endl;

}
