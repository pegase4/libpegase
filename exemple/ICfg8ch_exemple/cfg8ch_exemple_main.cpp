#include <QCoreApplication>
#include "ICfg8ch.h"
#include <QDebug>

#include <QObject>
#include <QFile>
#include <QCommandLineOption>
#include <QCommandLineParser>

#include <unistd.h>
#include "cfg8ch_exemple.h"


struct confExemple{
    ICfg8ch::confCan myConfigCan;
    ICfg8ch::confTrig myConfigTrig;

};

int main(int argc, char *argv[])
{
    /*
     *  Init Qt console app
     */
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("DEZER");
    QCoreApplication::setApplicationVersion("1.0.0");

    bool ok;

    QCommandLineParser parser;
    parser.setApplicationDescription("Test helper");
    parser.addHelpOption();
    parser.addVersionOption();

    // freq
    QCommandLineOption freqOption(QStringList()      << "freq", "freq <hz>.", "freq");
    freqOption.setDefaultValue("2000");
    parser.addOption(freqOption);
    
    // count
    QCommandLineOption countOption(QStringList()     << "count", "count <nb>.", "count");
    countOption.setDefaultValue("2000");
    parser.addOption(countOption);
    
    // channel
    QCommandLineOption channelOption(QStringList()     << "channel", "channel <nb>.", "channel");
    channelOption.setDefaultValue("255");
    parser.addOption(channelOption);
    
    // mode
    QCommandLineOption modeOption(QStringList()     << "mode", "mode <nb>.", "mode");
    modeOption.setDefaultValue("1");     // CAN_MODE_HIGH_RESOLUTION
    parser.addOption(modeOption);

    // can
    QCommandLineOption canOption(QStringList()     << "can", "can <bool>.", "can");
    canOption.setDefaultValue("1");
    parser.addOption(canOption);
    
    // trig
    QCommandLineOption trigOption(QStringList()     << "trig", "trig <bool>.", "trig");
    trigOption.setDefaultValue("1");
    parser.addOption(trigOption);
    parser.process(app);

    quint32 freq = parser.value(freqOption).toUInt(&ok, 10);
    quint32 count = parser.value(countOption).toUInt(&ok, 10);
    quint8 channel = parser.value(channelOption).toUInt(&ok, 10);
    quint16 mode = parser.value(modeOption).toUInt(&ok, 10);
    bool canop = parser.value(canOption).toInt(&ok, 10);
    bool trig = parser.value(trigOption).toInt(&ok, 10);

    Cfg8chExemple* can  =new Cfg8chExemple();
    can->start(freq,count,channel,mode,canop,trig);

    app.exec();

    return 0;
}
