#ifndef GPIOEXEMPLE_H
#define GPIOEXEMPLE_H

#include <sys/ioctl.h>
#include <QString>
#include <QObject>
#include "ICfg8ch.h"
#include "IStdout.h"


class Cfg8chExemple : public QObject
{
    Q_OBJECT

public:
    Cfg8chExemple();
    ~Cfg8chExemple();

    void start(quint32  freq, quint32 count, quint8 channel, quint16 mode, bool canOk, bool trigOk);
    void test();
    ICfg8ch* m_icfg8ch;
public slots:
    void slotDataCan(QVector<IAnalogData> listData, IEvent *event);
    void slotDataTrig(QVector<IGnumData> listData, IEvent *event);
    void slotDataTrigOff(int channelTrigOf,IEvent*event);
    void slotOverwriteDma(int nbOverwrite,IEvent*event);
};

#endif
