#
#   Qt modules
#
QT += core
QT += network
QT -= gui
QT += concurrent


#
#   Project configuration and compiler options
#
CONFIG += c++14
CONFIG += c++11
CONFIG += warn_on

#
#   Output config
#
TARGET = ICfg8ch_example
target.path = $$[QT_INSTALL_BINS]
INSTALLS += target

#
#   libzeus config
#
DEPENDPATH += . ../../src
INCLUDEPATH += ../../src
LIBS += -L../../src -lpegase

#
#   Sources
#
SOURCES += \
    cfg8ch_exemple.cpp \
    cfg8ch_exemple_main.cpp

HEADERS += \
        cfg8ch_exemple.h
   

