#include <QMetaEnum>
#include <QString>
#include <QTextStream>
#include <QFile>
#include <QDebug>
#include <sys/stat.h>
#include <fcntl.h>
#include <QThread>
#include <unistd.h>
#include "event_exemple.h"

EventExemple::EventExemple()
{
    _ipps = new IPps();
}

int EventExemple::start()
{
    connect(_ipps,SIGNAL(ppsRising(IEvent*)),this,SLOT(truePPS(IEvent*)), Qt::QueuedConnection);
    connect(_ipps,SIGNAL(ppsFalse(IEvent*)),this,SLOT(falsePPS(IEvent*)), Qt::QueuedConnection);
    int ret = _ipps->init();
    if(ret >= 0){
        _ipps->start();
    }
    return ret;
}

EventExemple::~EventExemple()
{
    delete(_ipps);
}



void EventExemple::truePPS(IEvent*event )
{
    GpsData gpsdata = event->getGpsData();
    fprintf(stdout, "GPS Current Status :\n"
                                "\tDate : %04uY %02uM %02uD %02uH %02uM %02uS\n"
                                "\tPosition (Lat/Lon/Alt) : %f %f %f\n"
                                "\tQuality : %u\n"
                                "\tSat : %u\n"
                                "\tdHdop : %f\n"
                                "\tdVdop : %f\n"
                                "\tisSurveyinMode : %s\n"
                                "\tsurveyinObservationTime : %u sec\n"
                                "\tECEFMeanX : %d cm\n"
                                "\tECEFMeanY : %d cm\n"
                                "\tECEFMeanZ : %d cm\n"
                                "\tECEFMeanV : %u mm^2\n"
                                "\tisFixedMode : %s\n"
                                "\tqErr : %d ps\n",
                                gpsdata.uiYear, gpsdata.uiMonth, gpsdata.uiDay, gpsdata.uiHour, gpsdata.uiMinute, gpsdata.uiSeconde,
                                gpsdata.dLatitude, gpsdata.dLongitude, gpsdata.dAltitude,
                                gpsdata.iQuality,
                                gpsdata.uiSatelliteUsed,
                                gpsdata.dHdop,
                                gpsdata.dVdop,
                                gpsdata.isSurveyinMode ? "yes" : "no",
                                gpsdata.surveyinObservationTime,
                                gpsdata.ECEFMeanX,
                                gpsdata.ECEFMeanY,
                                gpsdata.ECEFMeanZ,
                                gpsdata.ECEFMeanV,
                                gpsdata.isFixedMode ? "yes" : "no",
                                gpsdata.qErr);

}

void EventExemple::falsePPS(IEvent*event )
{
    (void)(event);
    fprintf(stdout, "Faux pps\n");
}
