#ifndef EVENT_EXEMPLE_H
#define EVENT_EXEMPLE_H

#include <sys/ioctl.h>
#include <QString>
#include <QObject>
#include "IPps.h"
#include "IEvent.h"
#include <ifsttar_sync.h>

class EventExemple : public QObject
{
    Q_OBJECT

public:
    EventExemple();
    ~EventExemple();
    int start();
    IPps *_ipps;
public slots:
    void truePPS(IEvent*event );
    void falsePPS(IEvent*event);
};

#endif
