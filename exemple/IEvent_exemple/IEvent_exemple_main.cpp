#include <QCoreApplication>
#include <QDebug>

#include <QObject>
#include <QFile>

#include <unistd.h>
#include "event_exemple.h"

void OnGPIO_IT()
{
    printf("coucou\n");
}

int main(int argc, char *argv[])
{
    /*
     *  Init Qt console app
     */
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("DEZER");
    QCoreApplication::setApplicationVersion("1.0.0");

    EventExemple* event =new EventExemple();
    event->start();
    app.exec();
    return 0;
}
