#
#   Qt modules
#
QT += core
QT -= gui

#
#   Project configuration and compiler options
#
CONFIG += c++14
CONFIG += warn_on

#
#   Output config
#
TEMPLATE = app
TARGET = IEvent_example
target.path = $$[QT_INSTALL_BINS]
INSTALLS += target

#
#   libPegase config
#
DEPENDPATH += . ../../src
INCLUDEPATH += ../../src
LIBS += -L../../src -lpegase

#
#   Sources
#
SOURCES += \
    IEvent_exemple_main.cpp\
    event_exemple.cpp

HEADERS += \
	event_exemple.h
   

