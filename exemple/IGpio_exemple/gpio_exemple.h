#ifndef GPIO_EXEMPLE_H
#define GPIO_EXEMPLE_H

#include <sys/ioctl.h>
#include <QString>
#include <QObject>
#include <QTimer>
#include <QDebug>
#include "IGpio.h"

class GpioExemple : public QObject
{
    Q_OBJECT

public:
    GpioExemple();
    ~GpioExemple();
    int start();
    void cleanExit();

private slots:
    void toggleGpio();
    void rising(int numChip, int offset, IGpio::Event event, IEvent *ievent);
    void falling(int numChip, int offset, IGpio::Event event, IEvent *ievent);

private:
    IGpio *m_gpio_output;
    IGpio *m_gpio_interupt;
};

#endif
