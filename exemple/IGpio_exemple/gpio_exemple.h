#ifndef GPIO_EXEMPLE_H
#define GPIO_EXEMPLE_H

#include <sys/ioctl.h>
#include <QString>
#include <QObject>
#include "IGpio.h"
#include <QDebug>

class GpioExemple : public QObject
{
    Q_OBJECT

public:
    GpioExemple();
    IGpio *m_gpio_output;
    IGpio *m_gpio_interupt;
    int start();
public slots:
    void rising(int,int,IGpio::Event,IEvent *);
};

#endif
