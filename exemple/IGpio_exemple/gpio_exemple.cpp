#include <unistd.h>
#include <csignal>
#include <QCoreApplication>
#include <QThread>
#include "gpio_exemple.h"

void signalHandler(int signum)
{
    qDebug() << "Signal (" << signum << ") reçu. Fermeture propre...";
    QMetaObject::invokeMethod(QCoreApplication::instance(), "quit", Qt::QueuedConnection);
}

GpioExemple::GpioExemple()
{
    // Capturer Ctrl+C et appeler signalHandler()
    std::signal(SIGINT, signalHandler);

    // Initialisation des GPIO
    m_gpio_output = new IGpio(IGpio::PE, 11);
    if (m_gpio_output->open(IGpio::Output) < 0) return;

    m_gpio_interupt = new IGpio(IGpio::PG, 10);
    if (m_gpio_interupt->open(IGpio::event) < 0) return;

    m_gpio_interupt->waitEvent(IGpio::RISING_EDGE);

    bool success = connect(m_gpio_interupt, SIGNAL(GpioRising(int, int, IGpio::Event, IEvent*)),
                           this, SLOT(rising(int, int, IGpio::Event, IEvent*)));

    if (success)
        qDebug() << "✅ Connexion du signal GpioRising réussie !";
    else
        qDebug() << "❌ Échec de connexion du signal GpioRising !";

    success = connect(m_gpio_interupt, SIGNAL(GpioFalling(int, int, IGpio::Event, IEvent*)),
                           this, SLOT(falling(int, int, IGpio::Event, IEvent*)));

    if (success)
        qDebug() << "✅ Connexion du signal GpioFalling réussie !";
    else
        qDebug() << "❌ Échec de connexion du signal GpioFalling !";

    qDebug() << "✅ IGpio est prêt !";

    // Associer la fermeture propre des GPIO à la fermeture de Qt
    QObject::connect(QCoreApplication::instance(), &QCoreApplication::aboutToQuit,
                     this, &GpioExemple::cleanExit);
}

GpioExemple::~GpioExemple()
{
    cleanExit();
}

int GpioExemple::start()
{
    // Utiliser QTimer au lieu d'une boucle infinie
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &GpioExemple::toggleGpio);
    timer->start(1000);
    return 0;
}

void GpioExemple::toggleGpio()
{
    static bool toggle = false;
    toggle = !toggle;
    m_gpio_output->SetValue(toggle);
}

void GpioExemple::rising(int numChip, int offset, IGpio::Event event, IEvent *ievent)
{
    (void)(ievent);
    qDebug() << "Rising [" << event << "] Le gpiochip" << numChip << " ligne " << offset;
}

void GpioExemple::falling(int numChip, int offset, IGpio::Event event, IEvent *ievent)
{
    (void)(ievent);
    qDebug() << "falling [" << event << "] Le gpiochip" << numChip << " ligne " << offset;
}

void GpioExemple::cleanExit()
{
    qDebug() << "Fermeture propre des GPIO et de l'application...";

    if (m_gpio_output) {
        m_gpio_output->close();
        delete m_gpio_output;
        m_gpio_output = nullptr;
    }

    if (m_gpio_interupt) {
        m_gpio_interupt->close();
        m_gpio_interupt->wait();
        disconnect(m_gpio_interupt, nullptr, this, nullptr);
        delete m_gpio_interupt;
        m_gpio_interupt = nullptr;
    }

    qDebug() << "✅ Tous les GPIO fermés. Bye!";
}
