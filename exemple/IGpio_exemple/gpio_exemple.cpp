#include <unistd.h>
#include "gpio_exemple.h"

GpioExemple::GpioExemple()
{
    m_gpio_output = new IGpio(IGpio::PF,13);
    
    m_gpio_interupt = new IGpio(IGpio::PB,1);
    m_gpio_interupt->waitEvent(IGpio::RISING_EDGE);
    connect(m_gpio_interupt,SIGNAL(GpioRising(int,int,IGpio::Event)),this,SLOT(rising(int,int,CGpio::Event)));   
}

int GpioExemple::start()
{
    bool toogle = false;
    m_gpio_interupt->start();
    while (1)
    {
        m_gpio_output->SetValue(!toogle);
        sleep(1);
    }
    return 0; 
}


void GpioExemple::rising(int numChip,int offset ,IGpio::Event event,IEvent *ievent){
    (void)(ievent);
    qDebug()<< "Rising ["<<event<<"] Le  gpiochip" << numChip << " ligne "<<offset <<"\n";
}
