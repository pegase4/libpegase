#
#   Qt modules
#
QT += core
QT -= gui

#
#   Project configuration and compiler options
#
CONFIG += c++14
CONFIG += warn_on

#
#   Output config
#
TARGET = IGpio_example
target.path = $$[QT_INSTALL_BINS]
INSTALLS += target

#
#   libPegase config
#
DEPENDPATH += . ../../src
INCLUDEPATH += ../../src
LIBS += -L../../src -lpegase

#
#   Sources
#
SOURCES += \
    gpio_exemple.cpp\
    IGpio_exemple_main.cpp


HEADERS += \
	gpio_exemple.h
   

