#include <QCoreApplication>
#include <unistd.h>
#include "gpio_exemple.h"


int main(int argc, char *argv[])
{
    /*
     *  Init Qt console app
     */
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("IGpio_exemple");
    QCoreApplication::setApplicationVersion("1.0.0");

    GpioExemple* gpio =new GpioExemple();
    gpio->start();
    app.exec();
    return 0;
}
