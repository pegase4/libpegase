#include <QCoreApplication>
#include "gpio_exemple.h"

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("IGpio_exemple");
    QCoreApplication::setApplicationVersion("1.0.0");

    GpioExemple* gpio = new GpioExemple();
    gpio->start();

    return app.exec();
}
